package ProgChad.PomoziMi.mailer;

import ProgChad.PomoziMi.Controller.PomoziMiController;
import ProgChad.PomoziMi.DTO.ResetPasswordDTO;
import ProgChad.PomoziMi.domain.RegistrationToken;
import ProgChad.PomoziMi.service.validation.impl.ResetFormValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriBuilder;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Ovaj servis šalje mailove.
 */
@Service
public class EmailSenderService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private SpringTemplateEngine emailTemplateEngine;

    /**
     * Metoda asinkrono šalje emailove koje joj se preda kao RegisterToken tip argumenta.
     * @param token registracijski token
     */
    @Async
    public void sendRegistrationVerificationEmail(RegistrationToken token, String URI) throws MessagingException, IOException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_RELATED, StandardCharsets.UTF_8.name());

        Context context = new Context();
        context.setVariable("username", token.getKorisnik().getUsername());
        context.setVariable("token", URI + token.getToken());

        String html = emailTemplateEngine.process("complete_registration_mail", context);

        helper.setTo(token.getKorisnik().getEmail());
        helper.setText(html,true);
        helper.setSubject("Dovršite registraciju!");
        helper.setFrom("PomoziMi");

        //File img = Path.of("/src/main/resources/static/img/Handshake.png").toFile();

        //helper.addAttachment("Handshake.png", img);

        javaMailSender.send(message);
    }

    /**
     * Metoda asinrkono šalje mailove koji služe za slanje emaila.
     * @param resetPasswordDTO
     * @param newPassword
     */
    @Async
    public void sendNewPasswordMail(ResetPasswordDTO resetPasswordDTO, String newPassword) throws MessagingException {

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_RELATED, StandardCharsets.UTF_8.name());

        Context context = new Context();
        context.setVariable("username", resetPasswordDTO.getUsername());
        context.setVariable("password", newPassword);

        String html = emailTemplateEngine.process("forgotten_password_mail", context);

        helper.setTo(resetPasswordDTO.getEmail());
        helper.setText(html,true);
        helper.setSubject("Nova lozinka!");
        helper.setFrom("PomoziMi");

        javaMailSender.send(message);
    }
}
