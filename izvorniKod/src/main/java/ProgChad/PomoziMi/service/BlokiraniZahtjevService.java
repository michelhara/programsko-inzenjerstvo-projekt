package ProgChad.PomoziMi.service;

import java.util.List;

import ProgChad.PomoziMi.domain.Zahtjev;

public interface BlokiraniZahtjevService {
	
	/**
	 * Vraca listu svih blokiranih zahtjeva
	 * @return
	 */
	List<Zahtjev> listAll();
	
	/**
	 * Vraca sve blokirane zahtjeve od korisnika s ID-om korisnikId
	 * @param korisnikId
	 * @return
	 */
	List<Zahtjev> listAllByKorisnikId(Integer korisnikId);
	
	/**
	 * @param zahtjevId
	 * @return
	 */
	Boolean blockZahtjev(Integer zahtjevId);
	
	/**
	 * @param zahtjevId
	 * @return
	 */
	Boolean unblockZahtjev(Integer zahtjevId);

}
