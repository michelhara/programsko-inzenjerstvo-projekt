package ProgChad.PomoziMi.service.impl;

import ProgChad.PomoziMi.domain.RegistrationToken;
import ProgChad.PomoziMi.repository.RegistrationTokenRepository;
import ProgChad.PomoziMi.service.RegistrationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementacija servica registracijskog tokena.
 */
@Service
public class RegistrationTokenServiceJpa implements RegistrationTokenService {

    @Autowired
    private RegistrationTokenRepository registrationTokenRepository;

    @Override
    public RegistrationToken findByToken(String token) {
        RegistrationToken regToken = registrationTokenRepository.findByToken(token);
        if(token == null) {
            throw new IllegalArgumentException("Token ne postoji!");
        }
        return regToken;
    }

    @Override
    public void removeToken(RegistrationToken token) {
        registrationTokenRepository.delete(token);
    }

    @Override
    public RegistrationToken createToken(RegistrationToken registrationToken) {
        if(registrationToken == null) {
            throw new IllegalArgumentException("Registracijski token je neispravan!");
        }
        return registrationTokenRepository.save(registrationToken);
    }
}
