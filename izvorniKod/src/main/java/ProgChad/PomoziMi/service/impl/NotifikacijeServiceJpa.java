package ProgChad.PomoziMi.service.impl;

import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Notifikacije;
import ProgChad.PomoziMi.domain.Zahtjev;
import org.springframework.beans.factory.annotation.Autowired;

import ProgChad.PomoziMi.repository.KorisnikRepository;
import ProgChad.PomoziMi.repository.NotifikacijeRepository;
import ProgChad.PomoziMi.service.NotifikacijeService;
import org.springframework.stereotype.Service;

@Service
public class NotifikacijeServiceJpa implements NotifikacijeService {
	
	@Autowired
	NotifikacijeRepository notifikacijeRepository;
	
	@Autowired
	KorisnikRepository korisnikRepository;

	@Override
	public void deleteNotification(Integer id) {
		Notifikacije notifikacija = notifikacijeRepository.findByNotifikacijaId(id);
		if(notifikacija == null)
			throw new IllegalArgumentException("Notifikacija s ovim id-om ne postoji!");
		notifikacijeRepository.delete(notifikacija);
	}


	@Override
	public void createNotification(Korisnik upucenaKorisniku, Korisnik kliknuKorinsik, Zahtjev zahtjevZaPomoci, boolean obavijestOPomoci, String customMessage) {

		if(zahtjevZaPomoci == null || upucenaKorisniku == null ||kliknuKorinsik == null) {
			throw new IllegalArgumentException("Nužne informacije nisu predane! (Zahtjev, kliknuo korisnik i vlasnik zahtjeva!)");
		}

		Notifikacije notifikacija = new Notifikacije(upucenaKorisniku);
		notifikacija.setKorisnikKojiJeNapravioNesto(kliknuKorinsik);
		notifikacija.setZahtjevZaPomoc(zahtjevZaPomoci);
		notifikacija.setObavijestOPomoci(obavijestOPomoci);
		if(!obavijestOPomoci && customMessage == null) {
			notifikacija.setSadrzaj(" je odobrio vašu pomoć! Korisnikov email je: "
					+ zahtjevZaPomoci.getKorisnikId().getEmail() + " te je broj koji je naveo u zahtjevu: " + zahtjevZaPomoci.getBrojTelefona());
		} else if(obavijestOPomoci && customMessage == null){
			notifikacija
					.setSadrzaj(" Vam želi pomoći na zahtjev: "
							+ (zahtjevZaPomoci.getOpisZahtjeva().length() < 30 ?
							zahtjevZaPomoci.getOpisZahtjeva()
							: zahtjevZaPomoci.getOpisZahtjeva().substring(0, 20) + "..."));
		} else {
			notifikacija.setSadrzaj(customMessage);
		}

		notifikacijeRepository.save(notifikacija);
	}
}
