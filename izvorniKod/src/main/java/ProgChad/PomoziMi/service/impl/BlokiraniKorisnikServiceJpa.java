package ProgChad.PomoziMi.service.impl;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ProgChad.PomoziMi.domain.BlokiraniKorisnik;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.repository.BlokiraniKorisnikRepository;
import ProgChad.PomoziMi.service.BlokiraniKorisnikService;
import ProgChad.PomoziMi.service.KorisnikService;

@Service
public class BlokiraniKorisnikServiceJpa implements BlokiraniKorisnikService {
	
	@Autowired
	private BlokiraniKorisnikRepository blokiraniKorisnikRepo;
	
	@Autowired
	private KorisnikService korisnikService;

	@Override
	public List<Korisnik> listAll() {
		return blokiraniKorisnikRepo.findAllBlokiraniKorisnici();
	}

	@Override
	public Boolean blockKorisnik(Integer korisnikId) {
		BlokiraniKorisnik blokiraniKorisnik = null;
		
		//admin blokira
		Korisnik korisnik = korisnikService.findByKorisnikID(korisnikId);

		if (korisnik != null) {
			//onemoguci korisnika
			korisnik.setEnabled(0);
			blokiraniKorisnik = new BlokiraniKorisnik(korisnik, Date.valueOf("2100-10-10") , 1); 
		}
		
		if (blokiraniKorisnik != null) {
			blokiraniKorisnikRepo.save(blokiraniKorisnik); 
			return true;
		} 
		
		return false;
	}

	@Override
	public Boolean unblockKorisnik(Integer korisnikId) {
		//admin deblokira
		BlokiraniKorisnik blokiraniKorisnik = blokiraniKorisnikRepo.findByKorisnikId(korisnikId);
		
		Korisnik korisnik = korisnikService.findByKorisnikID(korisnikId);
		
		if (blokiraniKorisnik != null && korisnik != null) {
			//onemoguci korisnika
			korisnik.setEnabled(1);
			blokiraniKorisnikRepo.deleteBlokiraniKorisnik(blokiraniKorisnik.getBlokiraniKorisnikId());
			return true;
		} 
		
		return false;
	}

}
