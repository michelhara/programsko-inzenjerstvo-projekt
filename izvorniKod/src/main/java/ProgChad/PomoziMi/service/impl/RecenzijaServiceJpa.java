package ProgChad.PomoziMi.service.impl;

import java.util.List;

import ProgChad.PomoziMi.domain.Korisnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ProgChad.PomoziMi.domain.Recenzija;
import ProgChad.PomoziMi.repository.RecenzijeRepository;
import ProgChad.PomoziMi.service.RecenzijeService;

@Service
public class RecenzijaServiceJpa implements RecenzijeService {

	@Autowired
	private RecenzijeRepository recenzijeRepo;
	
	@Override
	public List<Recenzija> findByKorisnikId(Integer korisnikId) {
		return recenzijeRepo.findByKorisnikId(korisnikId);
	}
	
	@Override
	public List<Recenzija> findByRecenziraniKorisnikId(Integer korisnikId) {
		return recenzijeRepo.findByRecenziraniKorisnikId(korisnikId);
	}

	@Override
	public Recenzija findByOcijenjenIOcijenjujeId(Integer ocijenjenId, Integer ocijenjujeId) {
		Recenzija recenzija = recenzijeRepo.korisnikOcijenioKorisnika(ocijenjujeId, ocijenjenId);
		return recenzija;
	}

	@Override
	public Boolean deleteRecenzija(Recenzija recenzija) {

		if(recenzija != null)
			recenzijeRepo.delete(recenzija);

		return true;
	}

	@Override
	public Boolean ocijenioKorisnik(Korisnik logiraniKorisnik, Korisnik gledaniKorisnik) {
		Recenzija recenzija = recenzijeRepo.korisnikOcijenioKorisnika(logiraniKorisnik.getKorisnikId(), gledaniKorisnik.getKorisnikId());
		return recenzija != null;
	}

	@Override
	public Recenzija createRecenzija(Recenzija recenzija) {
		try {
			return recenzijeRepo.save(recenzija);
		} catch (Exception e) {
			throw new IllegalArgumentException("Neuspješna izrada recenzije!");
		}	
	}

}
