package ProgChad.PomoziMi.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import ProgChad.PomoziMi.DTO.RecenzijaDTO;
import ProgChad.PomoziMi.DTO.ResetPasswordDTO;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Recenzija;
import ProgChad.PomoziMi.domain.RegistrationToken;
import ProgChad.PomoziMi.repository.KorisnikRepository;
import ProgChad.PomoziMi.repository.RecenzijeRepository;
import ProgChad.PomoziMi.service.KorisnikService;
import ProgChad.PomoziMi.service.RegistrationTokenService;

@Service
public class KorisnikServiceJpa implements KorisnikService {

	@Autowired
	private KorisnikRepository korisnikRepo;

	@Autowired
	private RegistrationTokenService registrationTokenService;

	@Autowired
	private RecenzijeRepository recenzijeRepository;

	@Override
	public List<Korisnik> listAll() {
		return korisnikRepo.findAll();
	}

	@Override
	public List<Korisnik> listAllBlocked() {
		return korisnikRepo.listAllBlocked();
	}

	@Override
	public Boolean isBlocked(Integer korisnikId) {
		List<Korisnik> blokiraniKorisnici = korisnikRepo.listAllBlocked();
		for (Korisnik k : blokiraniKorisnici) {
			if (k.getKorisnikId() == korisnikId) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Stvara novog korisnika ako već ne postoji.
	 * 
	 * @param korisnik
	 * @return stvoreni korisnik
	 */
	@Override
	public Korisnik createKorisnik(Korisnik korisnik) {
		if (korisnikRepo.findByEmail(korisnik.getEmail()) != null)
			throw new IllegalArgumentException("Korisnik s ovim e-mailom već postoji!");

		if (korisnikRepo.findByUsername(korisnik.getUsername()) != null)
			throw new IllegalArgumentException("Korisnik s ovim imenom već postoji!");

		// Ekripcija lozinke.
		String salt = BCrypt.gensalt(12);
		String hashed = BCrypt.hashpw(korisnik.getPassword(), salt);
		korisnik.setPassword(hashed);

		// korisnik e-mail verifikacija
		korisnik.setEnabled(0);
		return korisnikRepo.save(korisnik);
	}

	@Override
	public Korisnik findByKorisnikID(Integer korisnikID) {
		Korisnik korisnikById = korisnikRepo.findByKorisnikId(korisnikID);
		if (korisnikById == null)
			throw new IllegalArgumentException("Ne postoji korisnik s tim identifikatorom.");
		return korisnikById;
	}

	@Override
	public Korisnik findByUsername(String username) {
		Korisnik korisnik = korisnikRepo.findByUsername(username);
		if (korisnik == null)
			throw new IllegalArgumentException("Ne postoji korisnik s tim korisničkim imenom.");
		return korisnik;
	}

	@Override
	public Korisnik findByEmail(String email) {
		Korisnik korisnik = korisnikRepo.findByEmail(email);
		if (korisnik == null)
			throw new IllegalArgumentException("Ne postoji korisnik s tim emailom.");
		return korisnik;
	}

	@Override
	public boolean confirmKorisnik(String token) {
		RegistrationToken registrationToken = registrationTokenService.findByToken(token);
		if (registrationToken == null)
			return false;
		if (registrationToken.getUntilDate().before(new Date())) {
			registrationTokenService.removeToken(registrationToken);
			return false;
		}

		Korisnik korisnik = registrationToken.getKorisnik();
		korisnik.setEnabled(1);
		korisnikRepo.save(korisnik);
		registrationTokenService.removeToken(registrationToken);
		return true;
	}

	@Override
	public String changePassword(ResetPasswordDTO resetPasswordDTO) {
		Korisnik korisnik = korisnikRepo.findByUsername(resetPasswordDTO.getUsername());
		if (korisnik == null)
			return null;

		if (!korisnik.getEmail().equals(resetPasswordDTO.getEmail()))
			return null;

		String newPassword = randomPassword(16);
		changePassword(korisnik.getUsername(), newPassword);
		return newPassword;
	}

	/**
	 * Metoda generira nasumično string određene duljine.
	 * 
	 * @return
	 */
	private String randomPassword(int length) {
		int leftLimit = 48; // numeral '0'
		int rightLimit = 122; // letter 'z'
		Random random = new Random();

		String newPassword = random.ints(leftLimit, rightLimit + 1)
				.filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97)).limit(length)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

		return newPassword;
	}

	@Override
	public void changePassword(String username, String novaLozinka) {
		Korisnik korisnik = korisnikRepo.findByUsername(username);
		if (korisnik == null)
			throw new IllegalArgumentException("Nema korisnika s tim korisničkim imenom!");

		// Ekripcija lozinke.
		String salt = BCrypt.gensalt(12);
		String hashed = BCrypt.hashpw(novaLozinka, salt);
		korisnik.setPassword(hashed);

		korisnikRepo.save(korisnik);
	}

	@Override
	public Korisnik updateKorisnik(Integer korisnikId, Korisnik korisnik) {

		Korisnik updatedKorisnik = korisnikRepo.findByKorisnikId(korisnikId);

		if (updatedKorisnik != null) {

			updatedKorisnik.setEmail(korisnik.getEmail());
			updatedKorisnik.setEnabled(korisnik.getEnabled());
			updatedKorisnik.setIme(korisnik.getIme());
			updatedKorisnik.setPassword(korisnik.getPassword());
			updatedKorisnik.setPrezime(korisnik.getPrezime());
			updatedKorisnik.setSakriveniZahtjevi(korisnik.getSakriveniZahtjevi());
			updatedKorisnik.setUloga(korisnik.getUloga());
			updatedKorisnik.setUsername(korisnik.getUsername());

			try {
				return korisnikRepo.save(updatedKorisnik);
			} catch (Exception e) {
				throw new IllegalArgumentException("Neuspješna izmjena korisnika s ID-om " + korisnikId + "!");
			}
		}

		return null;
	}

	@Override
	public Boolean makeKorisnikAdmin(Integer korisnikId) {
		Korisnik updatedKorisnik = korisnikRepo.findByKorisnikId(korisnikId);

		if (updatedKorisnik != null) {

			// ako se pokusa vec postojecem adminu dati ovlasti
			if (updatedKorisnik.getUloga().equals("ROLE_ADMIN")) {
				return true;
			}

			updatedKorisnik.setUloga("ROLE_ADMIN");

			try {
				korisnikRepo.save(updatedKorisnik);
				return true;
			} catch (Exception e) {
				throw new IllegalArgumentException(
						"Neuspješna dodjela administratorskih prava korisniku s ID-om " + korisnikId + "!");
			}
		}
		return false;
	}
}
