package ProgChad.PomoziMi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ProgChad.PomoziMi.domain.Kontakti;
import ProgChad.PomoziMi.repository.KontaktiRepository;
import ProgChad.PomoziMi.service.KontaktiService;

@Service
public class KontaktiServiceJpa implements KontaktiService {
	
	@Autowired
	private KontaktiRepository kontaktiRepo;

	@Override
	public List<Kontakti> listAll() {
		return kontaktiRepo.findAll();
	}

	@Override
	public Kontakti findByKorisnikID(Integer korisnikId) {
		//uzmi prvu korisnikovu adresu i prvi kontakt
		List<Kontakti> kontakti = kontaktiRepo.findByKorisnikId(korisnikId);
		
		if (kontakti.size() > 0) {
			return kontakti.get(0);
		}
		
		return null;
	}

	@Override
	public Kontakti findByKontaktId(Integer kontaktId) {
		Kontakti k = kontaktiRepo.findByKontaktId(kontaktId);
		if(k == null)
			throw new IllegalArgumentException("Ne postoji kontakt s tim identifikatorom!");
		return k;
	}

	@Override
	public Kontakti saveKontakt(Kontakti kontakt) {
		if(kontakt == null) throw new IllegalArgumentException("Kontakt ne može biti null!");
		if(!kontakt.getBrojTelefona().matches("[0-9]+")) throw new IllegalArgumentException("Broj telefona mora sadržavati samo brojeve!");
		return kontaktiRepo.save(kontakt);
	}

	@Override
	public void removeKontakt(Kontakti kontakt) {
		kontaktiRepo.delete(kontakt);
	}

}
