package ProgChad.PomoziMi.service.impl;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ProgChad.PomoziMi.domain.BlokiraniZahtjev;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Zahtjev;
import ProgChad.PomoziMi.repository.BlokiraniZahtjevRepository;
import ProgChad.PomoziMi.service.BlokiraniZahtjevService;
import ProgChad.PomoziMi.service.KorisnikService;
import ProgChad.PomoziMi.service.ZahtjevService;

@Service
public class BlokiraniZahtjevServiceJpa implements BlokiraniZahtjevService {
	
	@Autowired
	private BlokiraniZahtjevRepository blokiraniZahtjevRepo;
	
	@Autowired
	private ZahtjevService zahtjevService;
	
	@Autowired
	private KorisnikService korisnikService;

	@Override
	public List<Zahtjev> listAll() {
		return blokiraniZahtjevRepo.findAllBlokiraniZahtjevi();
	}

	@Override
	public List<Zahtjev> listAllByKorisnikId(Integer korisnikId) {
		Korisnik korisnik = korisnikService.findByKorisnikID(korisnikId);
		
		if (korisnik != null) {
			List<Zahtjev> blokiraniZahtjevi = blokiraniZahtjevRepo.findAllByKorisnikId(korisnikId);
			
			return blokiraniZahtjevi;
		} else {
			return null;
		}
		
	}

	@Override
	public Boolean blockZahtjev(Integer zahtjevId) {
		BlokiraniZahtjev blokiraniZahtjev = null;
		
		//admin blokira
		Zahtjev zahtjev = zahtjevService.findByZahtjevId(zahtjevId);

		if (zahtjev != null) {
			blokiraniZahtjev = new BlokiraniZahtjev(zahtjev, Date.valueOf("2100-10-10") , 1);
		}
		
		if (blokiraniZahtjev != null) {
			blokiraniZahtjevRepo.save(blokiraniZahtjev); 
			return true;
		} 
		
		return false;
	}

	@Override
	public Boolean unblockZahtjev(Integer zahtjevId) {				
		//admin deblokira
		BlokiraniZahtjev blokiraniZahtjev = blokiraniZahtjevRepo.findByZahtjevId(zahtjevId);
		
		if (blokiraniZahtjev != null) {
			blokiraniZahtjevRepo.deleteBlokiraniZahtjev(blokiraniZahtjev.getZahtjevId().getZahtjevId());
			return true;
		} 
		
		return false;
	}

}
