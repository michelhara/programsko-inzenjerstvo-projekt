package ProgChad.PomoziMi.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ProgChad.PomoziMi.domain.Kontakti;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Zahtjev;
import ProgChad.PomoziMi.repository.ZahtjevRepository;
import ProgChad.PomoziMi.service.KontaktiService;
import ProgChad.PomoziMi.service.KorisnikService;
import ProgChad.PomoziMi.service.ZahtjevService;

@Service
public class ZahtjevServiceJpa implements ZahtjevService {
	
	@Autowired
	private ZahtjevRepository zahtjevRepo;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	private KontaktiService kontaktiService;

	@Override
	public List<Zahtjev> listAll() {
		return zahtjevRepo.findAll();
	}
	
	@Override
	public List<Zahtjev> listAllFilteredByKorisnikId(Integer korisnikId, Integer loggedId) {
		return zahtjevRepo.findAllFilteredByKorisnikId(korisnikId, loggedId);
	}

	@Override
	public List<Zahtjev> listAllFinishedFilteredByKorisnikId(Integer korisnikId) {
		return zahtjevRepo.findAllFinishedFilteredByKorisnikId(korisnikId);
	}
	
	@Override
	public List<Zahtjev> listAllBlocked() {
		return zahtjevRepo.listAllBlocked();
	}

	@Override
	public List<Zahtjev> listAllFilteredNoLocation(Integer korisnik) {
		return zahtjevRepo.listZahtjevNoLocation(korisnik);
	}

	@Override
	public List<Zahtjev> listAllFilteredLocation(Integer korisnik, Integer udaljenost, Double korisnikovLatitude, Double korisnikovLongitude) {
		return zahtjevRepo.listZahtjevLocation(korisnik).stream().filter(z -> closeEnough(z, udaljenost, korisnikovLatitude, korisnikovLongitude)).collect(Collectors.toList());
	}
	
	@Override
	public List<Zahtjev> listAllFilteredNoLocationByCategory(Integer korisnik, String kategorija) {
		return zahtjevRepo.listZahtjevNoLocationByCategory(korisnik, kategorija);
	}

	@Override
	public List<Zahtjev> listAllFilteredLocationByCategory(Integer korisnik, String kategorija, Integer udaljenost, 
			Double korisnikovLatitude, Double korisnikovLongitude) {
		return zahtjevRepo.listZahtjevLocationByCategory(korisnik, kategorija).stream().filter(z -> closeEnough(z, udaljenost, korisnikovLatitude, korisnikovLongitude)).collect(Collectors.toList());
	}

	/**
	 * Metoda izračunava udaljenost koordinata zahtjeva do koordinata korisnika i provjerava jel je uključeno u danu udaljenost.
	 * @param z
	 * @param udaljenost
	 * @param korisnikovLatitude
	 * @param korisnikovLongitude
	 * @return
	 */
	protected boolean closeEnough(Zahtjev z, Integer udaljenost, Double korisnikovLatitude, Double korisnikovLongitude) {
		
		if(udaljenost <= 0) {
			throw new IllegalArgumentException("Udaljenost mora biti veća od 0!");
		}
		
		double AVERAGE_RADIUS_OF_EARTH_KM = 6371;

			double latDistance = Math.toRadians(korisnikovLatitude - z.getLatitude());
			double lngDistance = Math.toRadians(korisnikovLongitude - z.getLongitude());

			double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
					+ Math.cos(Math.toRadians(korisnikovLatitude)) * Math.cos(Math.toRadians(z.getLatitude()))
					* Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

			double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

			return (Math.round(AVERAGE_RADIUS_OF_EARTH_KM * c)) < udaljenost;
	}

	@Override
	public List<Zahtjev> listAllByKorisnikId(Integer korisnikId) {
		return zahtjevRepo.findAllByKorisnikId(korisnikId);
	}
	
	@Override
	public Zahtjev findByKorisnikIdAndZahtjevId(Integer korisnikId, Integer zahtjevId) {
		return zahtjevRepo.findByKorisnikIdAndZahtjevId(korisnikId, zahtjevId);
	}
	
	@Override
	public Zahtjev findByZahtjevId(Integer zahtjevId) {
		return zahtjevRepo.findByZahtjevId(zahtjevId);
	}

	@Override
	public Zahtjev createZahtjev(Integer korisnikId, Zahtjev zahtjev) {
		
		Korisnik korisnik = korisnikService.findByKorisnikID(korisnikId);
		
		zahtjev.setKorisnikId(korisnik);
		
		//ako korisnik zeli promijeniti adresu ili kontakt koji je unio medu osobne podatke
		//Kontakti kontakti = kontaktiService.findByKorisnikID(korisnikId);

		try {
			return zahtjevRepo.save(zahtjev);
		} catch (Exception e) {
			throw new IllegalArgumentException("Neuspješna izrada zahtjeva!");
		}
					
	}

	@Override
	public Zahtjev updateZahtjev(Integer korisnikId, Integer zahtjevId, Zahtjev zahtjev) {
		
		Korisnik korisnik = korisnikService.findByKorisnikID(korisnikId);
		
		zahtjev.setKorisnikId(korisnik);
		
		Zahtjev updatedZahtjev = zahtjevRepo.findByKorisnikIdAndZahtjevId(korisnikId, zahtjevId);
		
		if (updatedZahtjev != null) {

			updatedZahtjev.setOpisZahtjeva(zahtjev.getOpisZahtjeva());
			updatedZahtjev.setRok(zahtjev.getRok());
			updatedZahtjev.setIzvrsen(zahtjev.getIzvrsen());
			updatedZahtjev.setLongitude(zahtjev.getLongitude());
			updatedZahtjev.setLatitude(zahtjev.getLatitude());
			updatedZahtjev.setBrojTelefona(zahtjev.getBrojTelefona());
			updatedZahtjev.setKategorija(zahtjev.getKategorija());
			updatedZahtjev.setSakriven(zahtjev.getSakriven());
			updatedZahtjev.setPrikaziPodatke(zahtjev.getPrikaziPodatke());
			updatedZahtjev.setPrihvacaKorisnikId(zahtjev.getPrihvacaKorisnikId());
			updatedZahtjev.setZeliPomoci(zahtjev.getZeliPomoci());
			updatedZahtjev.setSakriveniZahtjev(zahtjev.getSakriveniZahtjev());
			
			try {
				return zahtjevRepo.save(updatedZahtjev);
			} catch (Exception e) {
				throw new IllegalArgumentException("Neuspješna izmjena zahtjeva s ID-om " + zahtjevId + "!");
			}
			
		} 
			
		return null;
	}

	@Override
	public Boolean deleteZahtjev(Integer korisnikId, Integer zahtjevId) {
		Korisnik korisnik = korisnikService.findByKorisnikID(korisnikId);
		
		Zahtjev deletedZahtjev = null;
		
		if (korisnik.getUloga().equals("ROLE_ADMIN")) {
			deletedZahtjev = zahtjevRepo.findByZahtjevId(zahtjevId);
		} else {
			deletedZahtjev = zahtjevRepo.findByKorisnikIdAndZahtjevId(korisnikId, zahtjevId);
		}
		
		if (deletedZahtjev != null) {
			
			try {
				zahtjevRepo.delete(deletedZahtjev);
			} catch (Exception e) {
				throw new IllegalArgumentException("Neuspješno brisanje zahtjeva s ID-om " + zahtjevId + "!");
			}
			
			return true;
		}
		
		return false;
		
	}

	@Override
	public Boolean hideZahtjev(Integer korisnikId, Integer zahtjevId) {

		Zahtjev hiddenZahtjev = zahtjevRepo.findByZahtjevId(zahtjevId);

		if (hiddenZahtjev != null) {
			if (korisnikId.equals(hiddenZahtjev.getKorisnikId().getKorisnikId())) {
				hiddenZahtjev.setSakriven(true);
			} else {
				hiddenZahtjev.addSakriveniZahtjev(korisnikService.findByKorisnikID(korisnikId));
			}
			zahtjevRepo.save(hiddenZahtjev);
			return true;
		}

		return false;
	}

	@Override
	public Boolean unhideZahtjev(Integer korisnikId, Integer zahtjevId) {

		Zahtjev hiddenZahtjev = zahtjevRepo.findByZahtjevId(zahtjevId);

		if (hiddenZahtjev != null) {
			if (korisnikId.equals(hiddenZahtjev.getKorisnikId().getKorisnikId())) {
				if(!hiddenZahtjev.getSakriven())
					throw new IllegalArgumentException("Ovaj zahtjev nije sakriven!");
				hiddenZahtjev.setSakriven(false);
			} else {
				Korisnik korisnik = korisnikService.findByKorisnikID(korisnikId);
				if(!hiddenZahtjev.getSakriveniZahtjev().contains(korisnik))
					throw new IllegalArgumentException("Ovaj korisnik nije sakrio ovaj zahtjev!");
				hiddenZahtjev.removeSakriveniZahtjev(korisnik);
			}
			zahtjevRepo.save(hiddenZahtjev);
			return true;
		}

		return false;
	}

}
