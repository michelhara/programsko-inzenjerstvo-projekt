package ProgChad.PomoziMi.service;

import java.util.List;

import ProgChad.PomoziMi.domain.Kontakti;

public interface KontaktiService {
	
	List<Kontakti> listAll();
	
	Kontakti findByKorisnikID(Integer korisnikID);

	Kontakti findByKontaktId(Integer kontaktId);

	Kontakti saveKontakt(Kontakti kontakt);

	void removeKontakt(Kontakti kontakt);

}
