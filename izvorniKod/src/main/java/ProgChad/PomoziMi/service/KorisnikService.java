package ProgChad.PomoziMi.service;

import ProgChad.PomoziMi.DTO.RecenzijaDTO;
import ProgChad.PomoziMi.DTO.ResetPasswordDTO;
import ProgChad.PomoziMi.DTO.JSON.RecenzijaDTOJson;
import ProgChad.PomoziMi.domain.Korisnik;

import java.util.List;

public interface KorisnikService {

    List<Korisnik> listAll();

    /**
     * Metoda kreira novog korisnika. Prima objekt korisnika kojeg treba spremiti u bazu podataka.
     * @param korisnik
     * @return napravljenog korisnika
     */
    Korisnik createKorisnik(Korisnik korisnik);

    /**
     * Pronalazi korisnika po unikantnom ID-u.
     * @param korisnikID
     * @return korisnik
     */
    Korisnik findByKorisnikID(Integer korisnikID);

    /**
     * Pronalazi korisnika po unikantnom username-u.
     * @param username
     * @return korisnik
     */
    Korisnik findByUsername(String username);

    /**
     * Pronalazi korisnika po unikantno e-mailu.
     * @param email
     * @return
     */
	Korisnik findByEmail(String email);

    /**
     * Pomoću predanog tokena dovršava registraciju korisnika.
     * @param token
     * @return
     */
	boolean confirmKorisnik(String token);

    /**
     * Metoda mijenja korisničku lozinku ovisno o tome postoji li uopće korisnik te mijenja postojeću lozinku na novu
     * nasumično generiranu lozinku od 16 znakova. Korišteno kada korisnik zaboravi lozinku i ne može se logirati.
     * @param resetPasswordDTO
     * @return
     */
	String changePassword(ResetPasswordDTO resetPasswordDTO);

    /**
     * Metoda mijenja korisničku lozinku na novu lozinku koju je napisao.
     * @param username
     * @param novaLozinka
     */
	void changePassword(String username, String novaLozinka);
	
	Korisnik updateKorisnik(Integer korisnikId, Korisnik korisnik);
	
	/**
	 * Danom korisniku daje administratorske ovlasti
	 * @param korisnikId
	 * @return
	 */
	Boolean makeKorisnikAdmin(Integer korisnikId);

	/**
	 * Vraca listu blokiranih korisnika
	 * @return
	 */
	List<Korisnik> listAllBlocked();

	/**
	 * Funkcija koja provjera je li korisnik blokiran
	 * @param korisnikId
	 * @return
	 */
	Boolean isBlocked(Integer korisnikId);

}
