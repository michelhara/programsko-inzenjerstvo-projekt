package ProgChad.PomoziMi.service;

import ProgChad.PomoziMi.domain.RegistrationToken;

public interface RegistrationTokenService {

    /**
     * Pronalazi nalazi li se taj određeni token unutar te baze i vraća ga.
     * @param token
     * @return
     */
    RegistrationToken findByToken(String token);

    /**
     * Metoda će izbrisati token iz tablice tokena.
     * @param token
     * @return
     */
    void removeToken(RegistrationToken token);

    /**
     * Stvara novi token.
     * @param registrationToken
     * @return
     */
    RegistrationToken createToken(RegistrationToken registrationToken);

}
