package ProgChad.PomoziMi.service;

import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Recenzija;

import java.util.List;

public interface RecenzijeService {

    List<Recenzija> findByKorisnikId (Integer korisnikId);
    
    List<Recenzija> findByRecenziraniKorisnikId (Integer korisnikId);

    Recenzija findByOcijenjenIOcijenjujeId(Integer ocijenjenId, Integer ocijenjujeId);

    Recenzija createRecenzija(Recenzija recenzija);
    
    Boolean deleteRecenzija(Recenzija recenzija);

     Boolean ocijenioKorisnik(Korisnik logiraniKorisnik, Korisnik gledaniKorisnik);

}
