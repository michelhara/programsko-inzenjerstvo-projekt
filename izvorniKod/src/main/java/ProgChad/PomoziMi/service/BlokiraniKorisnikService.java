package ProgChad.PomoziMi.service;

import java.util.List;

import ProgChad.PomoziMi.domain.Korisnik;

public interface BlokiraniKorisnikService {
	
	/**
	 * Vraca listu svih blokiranih korisnika
	 * @return
	 */
	List<Korisnik> listAll();
	
	/**
	 * Vraća true ako je korisnik uspješno blokiran, false ako nije
	 * @param korisnikId
	 * @return
	 */
	Boolean blockKorisnik(Integer korisnikId);
	
	/**
	 * Vraća true ako je korisnik uspješno deblokiran, false ako nije
	 * @param korisnikId
	 * @return
	 */
	Boolean unblockKorisnik(Integer korisnikId);

}
