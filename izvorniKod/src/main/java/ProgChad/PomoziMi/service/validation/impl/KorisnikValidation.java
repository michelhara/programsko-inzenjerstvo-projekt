package ProgChad.PomoziMi.service.validation.impl;

import ProgChad.PomoziMi.DTO.DTO;
import ProgChad.PomoziMi.service.KorisnikService;
import ProgChad.PomoziMi.service.validation.Validacija;
import org.springframework.beans.factory.annotation.Autowired;

public class KorisnikValidation extends Validacija{

    /**
     * Validira ako je giveni DTO dopušteno prazan na bilo kojem od danih mjesta u konkretnoj implementaciji.
     * @param korisnikDTO
     * @return
     */
    protected boolean validnaForma(DTO korisnikDTO) {
        if(!korisnikDTO.emptyOrNull()) throw new IllegalArgumentException("Sva polja forme moraju biti ispunjena!");
        return true;
    }

    /**
     * Validira ako je giveni DTO ima dopuštene vrijednosti u konkretnoj implementaciji.
     * @param korisnikDTO
     * @return
     */
    protected boolean objektValidacija(DTO korisnikDTO) {
        if(!korisnikDTO.equalUnique()) throw new IllegalArgumentException("Lozinke nisu jednake!");
        return true;
    }

}
