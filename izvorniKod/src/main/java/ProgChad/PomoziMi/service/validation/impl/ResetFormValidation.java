package ProgChad.PomoziMi.service.validation.impl;

import ProgChad.PomoziMi.DTO.DTO;
import ProgChad.PomoziMi.service.validation.Validacija;
import org.springframework.stereotype.Service;

@Service
public class ResetFormValidation extends Validacija {
    @Override
    protected boolean validnaForma(DTO ResetFormDTO) {
        return ResetFormDTO.emptyOrNull();
    }

    @Override
    protected boolean objektValidacija(DTO ResetFormDTO) {
        return ResetFormDTO.equalUnique();
    }
}
