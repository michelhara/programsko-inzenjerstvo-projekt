package ProgChad.PomoziMi.service.validation;

import ProgChad.PomoziMi.DTO.DTO;

public abstract class Validacija {

    public final boolean validiraj(DTO dto) {
        return  validnaForma(dto) && objektValidacija(dto);
    }

    protected abstract boolean validnaForma(DTO dto);

    protected abstract boolean objektValidacija(DTO dto);

}
