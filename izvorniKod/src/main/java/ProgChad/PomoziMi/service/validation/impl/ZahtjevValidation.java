package ProgChad.PomoziMi.service.validation.impl;

import ProgChad.PomoziMi.DTO.DTO;
import ProgChad.PomoziMi.service.validation.Validacija;

public class ZahtjevValidation extends Validacija {

	@Override
	protected boolean validnaForma(DTO zahtjevDTO) {
		if (!zahtjevDTO.emptyOrNull()) {
			throw new IllegalArgumentException("Polja opisa zahtjeva, kategorije i kontakt broj moraju biti popunjena!");
		}
		return true;
	}

	@Override
	protected boolean objektValidacija(DTO zahtjevDTO) {
		return zahtjevDTO.equalUnique();
	}

}
