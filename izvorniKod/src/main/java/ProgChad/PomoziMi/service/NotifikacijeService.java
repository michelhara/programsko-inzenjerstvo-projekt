package ProgChad.PomoziMi.service;

import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Notifikacije;
import ProgChad.PomoziMi.domain.Zahtjev;

public interface NotifikacijeService {

	void deleteNotification(Integer id);

	void createNotification(Korisnik upucenaKorisniku, Korisnik kliknuKorinsik, Zahtjev zahtjevZaPomoci, boolean obavijestOPomoci, String customMessage);
}
