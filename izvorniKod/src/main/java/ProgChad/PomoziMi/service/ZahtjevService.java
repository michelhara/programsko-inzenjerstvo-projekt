package ProgChad.PomoziMi.service;

import java.util.List;

import ProgChad.PomoziMi.domain.Zahtjev;

public interface ZahtjevService {
	
	List<Zahtjev> listAll();

	/**
	 * Vraca zahtjeve filtrirane tako da se ne pokazuju blokirani ilis kriveni zahtjevi (TODO udaljenost.)
	 * @param korisnik
	 * @return
	 */
	List<Zahtjev> listAllFilteredNoLocation(Integer korisnik);

	/**
	 * Vraca sve zahtjeve filtrirane po udaljenosti. Zahtjevi će biti vraćeni ako nisu blokirani, ili ako korisnik nije sakrio taj zahtjev te ako je u ispravnoj udaljenosti.
	 * @param korisnik
	 * @param udaljenost
	 * @param korisnikovLatitude
	 * @param korisnikovLongitude
	 * @return
	 */
	List<Zahtjev> listAllFilteredLocation(Integer korisnik, Integer udaljenost, Double korisnikovLatitude, Double korisnikovLongitude);

	/**
	 * Vraca sve zahtjeve filtrirane po udaljenosti. Zahtjevi će biti vraćeni ako nisu blokirani, ili ako korisnik nije sakrio taj zahtjev te ako je u ispravnoj udaljenosti.
	 * Uvažava i danu kategoriju.
	 * @param korisnik
	 * @param udaljenost
	 * @param korisnikovLatitude
	 * @param korisnikovLongitude
	 * @return
	 */
	List<Zahtjev> listAllFilteredLocationByCategory(Integer korisnik, String kategorija, Integer udaljenost, Double korisnikovLatitude, Double korisnikovLongitude);

	/**
	 * Vraca sve zahtjeve koji su filtirrani po kategoriji, ali ne i po udaljenosti.
	 * @param korisnik
	 * @param kategorija
	 * @return
	 */
	List<Zahtjev> listAllFilteredNoLocationByCategory(Integer korisnik, String kategorija);

	/**
	 * Vraća sve zahtjeve koje je korisnik napravio, a da nisu blokirani.
	 * @param korisnikId
	 * @return
	 */
	List<Zahtjev> listAllByKorisnikId(Integer korisnikId);

	/**
	 * Vraca sve zahtjeve koje korisnik vidi kad gleda profil drugog korisnika. Nece biti prikazani zahtjevi koji su sakriveni ili blokirani.
	 * @param korisnikId
	 * @param loggedIn
	 * @return
	 */
	List<Zahtjev> listAllFilteredByKorisnikId(Integer korisnikId, Integer loggedIn);

	/**
	 * Vraca sve zahtjeve koje je korisnik izvrsio, a da nisu blokirani.
	 * @param korisnikId
	 * @return
	 */
	List<Zahtjev> listAllFinishedFilteredByKorisnikId(Integer korisnikId);
	
	/**
	 * Vraca sve blokirane zahtjeve
	 * @return
	 */
	List<Zahtjev> listAllBlocked();

	/**
	 * Pronalazi zahtjev s tim korisnickim id-om i id-om tog zahtjeva.
	 * @param korisnikId
	 * @param zahtjevId
	 * @return
	 */
	Zahtjev findByKorisnikIdAndZahtjevId(Integer korisnikId, Integer zahtjevId);
	
	Zahtjev findByZahtjevId(Integer zahtjevId);
	
	Zahtjev createZahtjev(Integer korisnikId, Zahtjev zahtjev);
	
	Zahtjev updateZahtjev(Integer korisnikId, Integer zahtjevId, Zahtjev zahtjev);
	
	Boolean deleteZahtjev(Integer korisnikId, Integer zahtjevId);
	
	Boolean hideZahtjev(Integer korisnikId, Integer zahtjevId);

    Boolean unhideZahtjev(Integer korisnikId, Integer zahtjevId);
    
}
