package ProgChad.PomoziMi.commandRunner;


import java.sql.Timestamp;

import ProgChad.PomoziMi.domain.Recenzija;
import ProgChad.PomoziMi.repository.RecenzijeRepository;
import ProgChad.PomoziMi.service.RecenzijeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import ProgChad.PomoziMi.Controller.RestController.RestAdminController;
import ProgChad.PomoziMi.Controller.RestController.RestKorisnikController;
import ProgChad.PomoziMi.DTO.RecenzijaDTO;
import ProgChad.PomoziMi.domain.Kontakti;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Zahtjev;
import ProgChad.PomoziMi.repository.KontaktiRepository;
import ProgChad.PomoziMi.repository.KorisnikRepository;
import ProgChad.PomoziMi.repository.ZahtjevRepository;

@Component
public class KorisnikRunner implements CommandLineRunner {

    @Autowired
    private KorisnikRepository korisnikRrepository;
    @Autowired
    private ZahtjevRepository zahtjevRepository;
    @Autowired
	RestAdminController controller;
    @Autowired
	private KontaktiRepository kontaktiRepository;
    @Autowired
	private RestKorisnikController rkc;
    @Autowired
	private RecenzijeRepository recenzijeRepository;
    

    @Override
    public void run(String... args) throws Exception {
    	

    	
    	korisnikRrepository.save(new Korisnik(
                "Marko",
                "Josipović",
                "ROLE_USER",
                "wegiyo5652@hafutv.com",
                "Markec22",
                "$2y$12$qw8zEJf86L3sV/I8p2wcO.h6oqzI1msrzeb03.zrlymNtkHNqe7aC",
                1
        ));
    	korisnikRrepository.save(new Korisnik(
                "Nikola",
                "Keš",
                "ROLE_ADMIN",
                "nikola@email.com",
                "NikolaMoni",
                "$2y$12$2V995RQprxP1EtgalxcqpOJcMiU8GeQsT9HEPBeSXL9hwHq1shAc.",
                1
        ));
    	korisnikRrepository.save(new Korisnik(
                "Lovro",
                "Sreb",
                "ROLE_ADMIN",
                "lovro@email.com",
                "propaloDijete",
                "$2y$12$8a4rl8iOQYNCvZ2Aopmf3eyySUy26fydP/4dVNvfjhG1N0W7o9Lq.",
                1
        ));
    	korisnikRrepository.save(new Korisnik(
                "Pjer",
                "Gjer",
                "ROLE_USER",
                "pjer@email.com",
                "pjero",
                "$2y$12$oInNWfrF2V6dW2taNnwHmuO4y6EOnhCTJX66L8rHHpFmKc7v4EyW6",
                1
        ));
    	korisnikRrepository.save(new Korisnik(
                "Test",
                "User",
                "ROLE_USER",
                "test@email.com",
                "testni",
                "$2y$12$UwRtDL9a80YGasRwsil9Puq57CsyOKWGaztLKk4U6MrMvpzjT0V5y",
                1
        ));
    	korisnikRrepository.save(new Korisnik(
                "Milivoj",
                "Mirković",
                "ROLE_USER",
                "milivoj@email.com",
                "milko",
                "$2y$12$cidSttHYn3MpYbFjMGY6ke.Tm9l8549PzbpBIuBbqrKie7SrgjuHG",
                1
        ));
    	korisnikRrepository.save(new Korisnik(
                "USER",
                "TEST",
                "ROLE_USER",
                "novi@email.com",
                "register",
                "$2y$12$QHPWgXs1FaR0iz9bnTbMO.17ldVdsYCy/vP9hFkcD0M2GFd1xTJd6",
                0
        ));
    	//DODAVANJE ZAHTJEVA
    	//testni
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("testni"),
				"Kako obračunati kamate?",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				44.116940,
				15.235673,
				"0922334455",
				"financije",
				true,
				false,
				null
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("testni"),
				"Treabm pomoć s učenjem crtanja stripova.",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				44.116940,
				15.235673,
				"0922334455",
				"ostalo",
				true,
				false,
				null
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("testni"),
				"Kako naštimati gitaru?",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				44.116940,
				15.235673,
				"0922334455",
				"tehnika",
				true,
				false,
				null
		));
		//milko
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("milko"),
				"Potrebna pomoć popravljanja rupe u zidu.",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				44.116940,
				15.235673,
				"0925550968",
				"ostalo",
				true,
				false,
				null
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("milko"),
				"Mačak izgubljen! Potrebna pomoć kod traženja...",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				44.116940,
				15.235673,
				"0925550968",
				"ostalo",
				false,
				false,
				null
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("milko"),
				"Koju srednju školu odabrati?",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				null,
				null,
				"0925550968",
				"savjeti",
				false,
				false,
				null
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("milko"),
				"Potrebna pomoć oko kuhanja brancina.",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				null,
				null,
				"0925550968",
				"hrana",
				false,
				false,
				null
		));
		//pjero
    	zahtjevRepository.save(new Zahtjev(
    			this.korisnikRrepository.findByUsername("pjero"),
    			"Pomoć oko bicikla, guma mi je probušena!",
    			new Timestamp(System.currentTimeMillis() + 86_400_000),
    			false,
				45.837724,
				16.071932,
				"0955551545",
				"savjeti",
    			false,
    			false,
    			null
    	));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("pjero"),
				"Kako prići curi, a da ne budem čudan? Fizička demonstracija nužna!",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				45.837724,
				16.071932,
				"0955551545",
				"ostalo",
				false,
				false,
				null
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("pjero"),
				"Potrebna pomoć oko odabira boje za zidove!",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				true,
				null,
				null,
				"0955551545",
				"ostalo",
				false,
				false,
				korisnikRrepository.findByUsername("NikolaMoni")
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("pjero"),
				"Kako pravilno uložiti novce?",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				null,
				null,
				"0955551545",
				"financije",
				false,
				false,
				null
		));
    	//propaloDijete
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("propaloDijete"),
				"Pomoć oko ispita, pišem ga sutra! ",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				45.8187795,
				15.9988782,
				"0995556357",
				"ostalo",
				false,
				false,
				null
		));
    	zahtjevRepository.save(new Zahtjev(
    			this.korisnikRrepository.findByUsername("propaloDijete"),
    			"Instalacija windowsa mi je zaštekala, upomoć!",
    			new Timestamp(System.currentTimeMillis() + 86_400_000),
    			false,
				45.8501177,
				15.9340317,
				"0925556320",
				"tehnika",
    			false,
    			false,
    			null
    	));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("propaloDijete"),
				"Postavljanje modernih led lampi. Problem je tehničke prirode!",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				45.8501177,
				15.9340317,
				"0925556320",
				"tehnika",
				false,
				false,
				null
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("propaloDijete"),
				"Kako najbolje uložiti u dionice?",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				null,
				null,
				"0925556320",
				"financije",
				false,
				false,
				null
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("propaloDijete"),
				"Kako vratiti novce iz lošeg uloga u dionice?",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				null,
				null,
				"0925556320",
				"financije",
				false,
				false,
				null
		));
    	//NIKOLA MONI
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("NikolaMoni"),
				"Montiranje lustera",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				45.840126,
				16.058728,
				"0925551840",
				"ostalo",
				false,
				false,
				null
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("NikolaMoni"),
				"Mala količina soli potrebna!",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				true,
				45.840126,
				16.058728,
				"0925551840",
				"hrana",
				false,
				false,
				this.korisnikRrepository.findByUsername("pjero")
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("NikolaMoni"),
				"Savjeti oko češkanja mačaka.",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				true,
				45.840126,
				16.058728,
				"0925551840",
				"ostalo",
				false,
				false,
				this.korisnikRrepository.findByUsername("propaloDijete")
		));
		zahtjevRepository.save(new Zahtjev(
				this.korisnikRrepository.findByUsername("NikolaMoni"),
				"Pomoć oko kuhanja, pećnica mi je umrla :(",
				new Timestamp(System.currentTimeMillis() + 86_400_000),
				false,
				45.8401746,
				15.8942922,
				"092555437",
				"hrana",
				false,
				false,
				null
		));

		//KONTAKTI
        kontaktiRepository.save(new Kontakti(this.korisnikRrepository.findByUsername("pjero"), "091915234"));
		kontaktiRepository.save(new Kontakti(this.korisnikRrepository.findByUsername("propaloDijete"), "0955555950"));
		kontaktiRepository.save(new Kontakti(this.korisnikRrepository.findByUsername("propaloDijete"), "0915557423"));
		kontaktiRepository.save(new Kontakti(this.korisnikRrepository.findByUsername("NikolaMoni"), "0915555673"));

		//RECENZIJE
		recenzijeRepository.save(new Recenzija(this.korisnikRrepository.findByUsername("pjero"), this.korisnikRrepository.findByUsername("propaloDijete"), 4, "Dobar, ali ne odličan!"));
		recenzijeRepository.save(new Recenzija(this.korisnikRrepository.findByUsername("pjero"), this.korisnikRrepository.findByUsername("NikolaMoni"), 2, "NIje mi pomogao..."));
		recenzijeRepository.save(new Recenzija(this.korisnikRrepository.findByUsername("propaloDijete"), this.korisnikRrepository.findByUsername("pjero"), 5, "Ne mogu ga dovoljno nahvaliti!"));

	}
}
