package ProgChad.PomoziMi.mapper;


import ProgChad.PomoziMi.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ProgChad.PomoziMi.DTO.ZahtjevDTO;
import ProgChad.PomoziMi.domain.Zahtjev;

import java.sql.Timestamp;

@Service
public class ZahtjevMapper {
	
	public static Zahtjev modelToDTO(ZahtjevDTO zahtjevDTO, KorisnikService korisnikService) {
		
		Zahtjev zahtjev = new Zahtjev();
		
		//ID korisnik koji radi zahtjev se dobiva iz trenutno ulogiranog korisnika
		zahtjev.setKorisnikId(null);
		zahtjev.setIzvrsen(zahtjevDTO.getIzvrsen() != null && zahtjevDTO.getIzvrsen());
		zahtjev.setPrikaziPodatke(false);
		zahtjev.setOpisZahtjeva(zahtjevDTO.getOpisZahtjeva());
		zahtjev.setRok(zahtjevDTO.getRok());
		zahtjev.setLatitude(zahtjevDTO.getLatitude());
		zahtjev.setLongitude(zahtjevDTO.getLongitude());
		zahtjev.setBrojTelefona(zahtjevDTO.getBrojTelefona());
		zahtjev.setKategorija(zahtjevDTO.getKategorija());
		zahtjev.setSakriven(zahtjevDTO.getSakriven());
		zahtjev.setPrihvacaKorisnikId(zahtjevDTO.getPrihvacaKorisnikId() == null ? null : korisnikService.findByKorisnikID(zahtjevDTO.getPrihvacaKorisnikId()));

		if(zahtjev.getDatumNastanka() == null)
			zahtjev.setDatumNastanka(new Timestamp(System.currentTimeMillis()));
	
		return zahtjev;
		
	}

}
