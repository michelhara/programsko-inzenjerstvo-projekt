package ProgChad.PomoziMi.mapper;

import org.springframework.stereotype.Service;

import ProgChad.PomoziMi.DTO.KorisnikDTO;
import ProgChad.PomoziMi.domain.Korisnik;

@Service
public class KorisnikMapper {
	
	public static Korisnik modelToDTO(KorisnikDTO korisnikDTO) {
		Korisnik korisnik = new Korisnik();
		korisnik.setIme(korisnikDTO.getIme());
		korisnik.setPrezime(korisnikDTO.getPrezime());
		korisnik.setUsername(korisnikDTO.getUsername());
		korisnik.setEmail(korisnikDTO.getEmail());
		korisnik.setPassword(korisnikDTO.getPassword());
		
		korisnik.setEnabled(1);
		korisnik.setUloga("ROLE_USER");
		return korisnik;		
	}

}
