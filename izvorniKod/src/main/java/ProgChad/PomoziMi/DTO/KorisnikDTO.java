package ProgChad.PomoziMi.DTO;

public class KorisnikDTO implements DTO{

	private String ime;
	private String prezime;
	private String email;
	private String username;
	private String password;
	private String passwordPonovljeni;

	public String getPasswordPonovljeni() {
		return passwordPonovljeni;
	}

	public void setPasswordPonovljeni(String passwordPonovljeni) {
		this.passwordPonovljeni = passwordPonovljeni;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public boolean emptyOrNull() {
		return  (getEmail() != null && !getEmail().isEmpty()) &&
				(getIme() != null && !getIme().isEmpty()) &&
				(getPassword() != null && !getPassword().isEmpty() ) &&
				(getPasswordPonovljeni() != null && !getPasswordPonovljeni().isEmpty() ) &&
				(getPrezime() != null && !getPrezime().isEmpty()) &&
				(getUsername() != null && !getUsername().isEmpty());
	}

	@Override
	public boolean equalUnique() {
		return getPassword().equals(getPasswordPonovljeni());
	}


}
