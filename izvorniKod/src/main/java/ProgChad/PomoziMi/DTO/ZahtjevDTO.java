package ProgChad.PomoziMi.DTO;

import java.sql.Timestamp;

public class ZahtjevDTO implements DTO {

	private String opisZahtjeva;
	private Timestamp rok;
	private Boolean adresa;
	private Boolean izvrsen;
	private Double latitude;
	private Double longitude;
	private String brojTelefona;
	private String kategorija;
	private Boolean sakriven;
	private Boolean spremiKontakt;
	private Integer prihvacaKorisnikId;

	public ZahtjevDTO() {
	}

	public ZahtjevDTO(String opisZahtjeva, Timestamp rok, Double latitude, Double longitude, String brojTelefona, String kategorija, 
			Boolean sakriven, Boolean spremiKontakt) {
		this.opisZahtjeva = opisZahtjeva;
		this.rok = rok;
		this.latitude = latitude;
		this.longitude = longitude;
		this.brojTelefona = brojTelefona;
		this.kategorija = kategorija;
		this.sakriven = sakriven;
		this.spremiKontakt = spremiKontakt;
	}

	public Integer getPrihvacaKorisnikId() {
		return prihvacaKorisnikId;
	}

	public void setPrihvacaKorisnikId(Integer prihvacaKorisnikId) {
		this.prihvacaKorisnikId = prihvacaKorisnikId;
	}

	public Boolean getIzvrsen() {
		return izvrsen;
	}

	public void setIzvrsen(Boolean izvrsen) {
		this.izvrsen = izvrsen;
	}

	public Boolean getAdresa() {
		return adresa;
	}

	public void setAdresa(Boolean adresa) {
		this.adresa = adresa;
	}

	public Boolean getSpremiKontakt() {
		return spremiKontakt;
	}

	public void setSpremiKontakt(Boolean spremiKontakt) {
		this.spremiKontakt = spremiKontakt;
	}

	public String getOpisZahtjeva() {
		return opisZahtjeva;
	}
	public void setOpisZahtjeva(String opisZahtjeva) {
		this.opisZahtjeva = opisZahtjeva;
	}
	
	public Timestamp getRok() {
		return rok;
	}
	public void setRok(Timestamp rok) {
		this.rok = rok;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}
	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
	
	public String getKategorija() {
		return kategorija;
	}

	public void setKategorija(String kategorija) {
		this.kategorija = kategorija;
	}
	
	public Boolean getSakriven() {
		return sakriven;
	}
	public void setSakriven(Boolean sakriven) {
		this.sakriven = sakriven;
	}

	@Override
	public boolean emptyOrNull() {
		return (getOpisZahtjeva() != null && !getOpisZahtjeva().isEmpty()) &&
				(getKategorija() != null && !getKategorija().isEmpty()) &&
				getSakriven() != null && getBrojTelefona() != null;
	}
	@Override
	public boolean equalUnique() {
		return true;
	}
	
}
