package ProgChad.PomoziMi.DTO.JSON;

import ProgChad.PomoziMi.DTO.DTO;

import java.util.Date;

public class ZahtjevDTOJson {

	private Integer zahtjevId;
	private String opisZahtjeva;
	private Date rok;
	private Boolean izvrsen;
	private Integer kontaktId;
	private String brojTelefona;
	private String kategorija;
	private Boolean sakriven;
	private Boolean prikaziPodatke;
	private Integer prihvacaKorisnikId;
	private String prihvacaKorisnikUsername;
	private boolean trenutniKorisnikZainteresinan;
	private Integer vlasnikZahtjevaId;
	private String vlasnikZahtjevaUsername;

	public ZahtjevDTOJson() {
	}

	public ZahtjevDTOJson(Integer zahtjevId, String opisZahtjeva, Date rok, Boolean izvrsen, Double latitude, Double longitude, 
			String brojTelefona, String kategorija, Boolean sakriven, Boolean prikaziPodatke, Integer prihvacaKorisnikId, String prihvacaKorisnikUsername, 
			boolean trenutniKorisnikZainteresinan, Integer vlasnikZahtjevaId, String vlasnikZahtjevaUsername) {
		this.zahtjevId = zahtjevId;
		this.opisZahtjeva = opisZahtjeva;
		this.rok = rok;
		this.izvrsen = izvrsen;
		this.brojTelefona = brojTelefona;
		this.kategorija = kategorija;
		this.sakriven = sakriven;
		this.prikaziPodatke = prikaziPodatke;
		this.prihvacaKorisnikId = prihvacaKorisnikId;
		this.prihvacaKorisnikUsername = prihvacaKorisnikUsername;
		this.trenutniKorisnikZainteresinan = trenutniKorisnikZainteresinan;
		this.vlasnikZahtjevaId = vlasnikZahtjevaId;
		this.vlasnikZahtjevaUsername = vlasnikZahtjevaUsername;
	}



	public String getOpisZahtjeva() {
		return opisZahtjeva;
	}
	public void setOpisZahtjeva(String opisZahtjeva) {
		this.opisZahtjeva = opisZahtjeva;
	}
	
	public Date getRok() {
		return rok;
	}
	public void setRok(Date rok) {
		this.rok = rok;
	}
	
	public Boolean getIzvrsen() {
		return izvrsen;
	}
	public void setIzvrsen(Boolean izvrsen) {
		this.izvrsen = izvrsen;
	}

	public Integer getZahtjevId() {
		return zahtjevId;
	}

	public void setZahtjevId(Integer zahtjevId) {
		this.zahtjevId = zahtjevId;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}
	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
	
	public String getKategorija() {
		return kategorija;
	}

	public void setKategorija(String kategorija) {
		this.kategorija = kategorija;
	}
	
	public Boolean getSakriven() {
		return sakriven;
	}
	public void setSakriven(Boolean sakriven) {
		this.sakriven = sakriven;
	}
	public Boolean getPrikaziPodatke() {
		return prikaziPodatke;
	}
	public void setPrikaziPodatke(Boolean prikaziPodatke) {
		this.prikaziPodatke = prikaziPodatke;
	}
	public Integer getPrihvacaKorisnikId() {
		return prihvacaKorisnikId;
	}
	public void setPrihvacaKorisnikId(Integer prihvacaKorisnikId) {
		this.prihvacaKorisnikId = prihvacaKorisnikId;
	}

	public String getPrihvacaKorisnikUsername() {
		return prihvacaKorisnikUsername;
	}

	public void setPrihvacaKorisnikUsername(String prihvacaKorisnikUsername) {
		this.prihvacaKorisnikUsername = prihvacaKorisnikUsername;
	}

	public Integer getVlasnikZahtjevaId() {
		return vlasnikZahtjevaId;
	}

	public void setVlasnikZahtjevaId(Integer vlasnikZahtjevaId) {
		this.vlasnikZahtjevaId = vlasnikZahtjevaId;
	}

	public String getVlasnikZahtjevaUsername() {
		return vlasnikZahtjevaUsername;
	}

	public void setVlasnikZahtjevaUsername(String vlasnikZahtjevaUsername) {
		this.vlasnikZahtjevaUsername = vlasnikZahtjevaUsername;
	}

	public boolean isTrenutniKorisnikZainteresinan() {
		return trenutniKorisnikZainteresinan;
	}

	public void setTrenutniKorisnikZainteresinan(boolean trenutniKorisnikZainteresinan) {
		this.trenutniKorisnikZainteresinan = trenutniKorisnikZainteresinan;
	}

	public void setKontaktId(Integer kontaktId) {
		this.kontaktId = kontaktId;
	}

	public Integer getKontaktId() {
		return kontaktId;
	}

}
