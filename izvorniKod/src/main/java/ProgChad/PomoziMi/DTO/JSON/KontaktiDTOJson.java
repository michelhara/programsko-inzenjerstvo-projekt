package ProgChad.PomoziMi.DTO.JSON;

public class KontaktiDTOJson {
	
	private Integer kontaktId;
	private String brojTelefona;
	
	public KontaktiDTOJson() {
		
	}

	public KontaktiDTOJson(Integer kontaktId, String brojTelefona) {
		this.kontaktId = kontaktId;
		this.brojTelefona = brojTelefona;
	}

	public Integer getKontaktId() {
		return kontaktId;
	}

	public void setKontaktId(Integer kontaktId) {
		this.kontaktId = kontaktId;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

}
