package ProgChad.PomoziMi.DTO.JSON;

import ProgChad.PomoziMi.domain.Notifikacije;
import ProgChad.PomoziMi.domain.Recenzija;

import java.util.Set;

public class KorisnikDTOJson {

    private Integer korisnikId;
    private String ime;
    private String prezime;
    private String email;
    private String username;
    private String uloga;
    private Set<RecenzijaDTOJson> recenzije;
    private Set<KontaktiDTOJson> kontakti;
    private Set<NotifikacijaDTOJson> notifikacije;

    public KorisnikDTOJson(Integer korisnikId, String ime, String prezime, String email, String username, String uloga, Set<RecenzijaDTOJson> recenzije, Set<KontaktiDTOJson> kontakti, Set<NotifikacijaDTOJson> notifikacije) {
        this.korisnikId = korisnikId;
        this.ime = ime;
        this.prezime = prezime;
        this.email = email;
        this.username = username;
        this.uloga = uloga;
        this.recenzije = recenzije;
        this.kontakti = kontakti;
        this.notifikacije = notifikacije;
    }

    public Integer getKorisnikId() {
        return korisnikId;
    }

    public void setKorisnikId(Integer korisnikId) {
        this.korisnikId = korisnikId;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUloga() {
        return uloga;
    }

    public void setUloga(String uloga) {
        this.uloga = uloga;
    }

    public Set<RecenzijaDTOJson> getRecenzije() {
        return recenzije;
    }

    public void setRecenzije(Set<RecenzijaDTOJson> recenzije) {
        this.recenzije = recenzije;
    }

    public Set<KontaktiDTOJson> getKontakti() {
        return kontakti;
    }

    public void setKontakti(Set<KontaktiDTOJson> kontakti) {
        this.kontakti = kontakti;
    }

    public Set<NotifikacijaDTOJson> getNotifikacije() {
        return notifikacije;
    }

    public void setNotifikacije(Set<NotifikacijaDTOJson> notifikacije) {
        this.notifikacije = notifikacije;
    }
}
