package ProgChad.PomoziMi.DTO.JSON;

public class NotifikacijaDTOJson {

    private Integer notifikacijaId;

    private Integer korisnikId; // korisnik ciji je zahtjev

    private String korisnikUsername; // korisnik ciji je zahtjev

    private String sadrzaj;

    private Integer korisnikKojiJeNapravioNestoId; // korisnik koji ce prihvatit zahtjev, tj korisnik kojem ce biti odobreno prihvaćanje

    private String korisnikKojiJeNapravioNestoUsername; // korisnik koji ce prihvatit zahtjev, tj korisnik kojem ce biti odobreno prihvaćanje

    private Integer zahtjevZaPomocId;

    private boolean obavijestOPomoci;

    public NotifikacijaDTOJson(Integer notifikacijaId, Integer korisnikId, String korisnikUsername, String sadrzaj, Integer korisnikKojiJeNapravioNestoId, String korisnikKojiJeNapravioNestoUsername, Integer zahtjevZaPomocId, boolean obavijestOPomoci) {
        this.notifikacijaId = notifikacijaId;
        this.korisnikId = korisnikId;
        this.korisnikUsername = korisnikUsername;
        this.sadrzaj = sadrzaj;
        this.korisnikKojiJeNapravioNestoId = korisnikKojiJeNapravioNestoId;
        this.korisnikKojiJeNapravioNestoUsername = korisnikKojiJeNapravioNestoUsername;
        this.zahtjevZaPomocId = zahtjevZaPomocId;
        this.obavijestOPomoci = obavijestOPomoci;
    }

    public Integer getNotifikacijaId() {
        return notifikacijaId;
    }

    public void setNotifikacijaId(Integer notifikacijaId) {
        this.notifikacijaId = notifikacijaId;
    }

    public Integer getKorisnikId() {
        return korisnikId;
    }

    public void setKorisnikId(Integer korisnikId) {
        this.korisnikId = korisnikId;
    }

    public String getKorisnikUsername() {
        return korisnikUsername;
    }

    public void setKorisnikUsername(String korisnikUsername) {
        this.korisnikUsername = korisnikUsername;
    }

    public String getSadrzaj() {
        return sadrzaj;
    }

    public void setSadrzaj(String sadrzaj) {
        this.sadrzaj = sadrzaj;
    }

    public Integer getKorisnikKojiJeNapravioNestoId() {
        return korisnikKojiJeNapravioNestoId;
    }

    public void setKorisnikKojiJeNapravioNestoId(Integer korisnikKojiJeNapravioNestoId) {
        this.korisnikKojiJeNapravioNestoId = korisnikKojiJeNapravioNestoId;
    }

    public String getKorisnikKojiJeNapravioNestoUsername() {
        return korisnikKojiJeNapravioNestoUsername;
    }

    public void setKorisnikKojiJeNapravioNestoUsername(String korisnikKojiJeNapravioNestoUsername) {
        this.korisnikKojiJeNapravioNestoUsername = korisnikKojiJeNapravioNestoUsername;
    }

    public Integer getZahtjevZaPomocId() {
        return zahtjevZaPomocId;
    }

    public void setZahtjevZaPomocId(Integer zahtjevZaPomocId) {
        this.zahtjevZaPomocId = zahtjevZaPomocId;
    }

    public boolean isObavijestOPomoci() {
        return obavijestOPomoci;
    }

    public void setObavijestOPomoci(boolean obavijestOPomoci) {
        this.obavijestOPomoci = obavijestOPomoci;
    }
}
