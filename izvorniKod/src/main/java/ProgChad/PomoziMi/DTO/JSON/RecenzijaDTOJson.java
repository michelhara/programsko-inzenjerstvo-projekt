package ProgChad.PomoziMi.DTO.JSON;

public class RecenzijaDTOJson {

    private Integer recenziraniKorisnikId;
    private String recenziraniKorisnikUsername;
    private Integer recenziraKorisnikId;
    private String recenziraKorisnikUsername;
    private Integer ocjena;
    private String komentar;

    public RecenzijaDTOJson(Integer recenziraniKorisnikId, String recenziraniKorisnikUsername, Integer recenziraKorisnik, String recenziraKorisnikUsername, Integer ocjena, String komentar) {
        this.recenziraniKorisnikId = recenziraniKorisnikId;
        this.recenziraniKorisnikUsername = recenziraniKorisnikUsername;
        this.recenziraKorisnikId = recenziraKorisnik;
        this.recenziraKorisnikUsername = recenziraKorisnikUsername;
        this.ocjena = ocjena;
        this.komentar = komentar;
    }

    public Integer getRecenziraniKorisnikId() {
        return recenziraniKorisnikId;
    }

    public void setRecenziraniKorisnikId(Integer recenziraniKorisnikId) {
        this.recenziraniKorisnikId = recenziraniKorisnikId;
    }

    public String getRecenziraniKorisnikUsername() {
        return recenziraniKorisnikUsername;
    }

    public void setRecenziraniKorisnikUsername(String recenziraniKorisnikUsername) {
        this.recenziraniKorisnikUsername = recenziraniKorisnikUsername;
    }

    public Integer getRecenziraKorisnikId() {
        return recenziraKorisnikId;
    }

    public void setRecenziraKorisnikId(Integer recenziraKorisnikId) {
        this.recenziraKorisnikId = recenziraKorisnikId;
    }

    public String getRecenziraKorisnikUsername() {
        return recenziraKorisnikUsername;
    }

    public void setRecenziraKorisnikUsername(String recenziraKorisnikUsername) {
        this.recenziraKorisnikUsername = recenziraKorisnikUsername;
    }

    public Integer getOcjena() {
        return ocjena;
    }

    public void setOcjena(Integer ocjena) {
        this.ocjena = ocjena;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }
}
