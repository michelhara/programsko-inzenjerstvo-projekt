package ProgChad.PomoziMi.DTO;

public interface DTO {

    boolean emptyOrNull();

    /**
     * Provjerava ako su neka polja koja trebaju biti jednaka nisu jednaka unutar DTO objekta.
     * @return
     */
    boolean equalUnique();

}
