package ProgChad.PomoziMi.DTO;

public class RecenzijaDTO implements DTO {
	
    private Integer ocjena;
    private String komentar;
	private String recenziraniKorisnikUsername;
    
    public RecenzijaDTO() {
    	
    }

    public RecenzijaDTO(Integer ocjena, String komentar, String recenziraniKorisnikUsername) {
        this.ocjena = ocjena;
        this.komentar = komentar;
        this.recenziraniKorisnikUsername = recenziraniKorisnikUsername;
    }

    public Integer getOcjena() {
        return ocjena;
    }

    public void setOcjena(Integer ocjena) {
        this.ocjena = ocjena;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }
    
    public String getRecenziraniKorisnikUsername() {
		return recenziraniKorisnikUsername;
	}

	public void setRecenziraniKorisnikUsername(String recenziraniKorisnikUsername) {
		this.recenziraniKorisnikUsername = recenziraniKorisnikUsername;
	}

	@Override
	public boolean emptyOrNull() {
		return getOcjena() != null && (getKomentar() != null && !getKomentar().isEmpty());
	}

	@Override
	public boolean equalUnique() {
		return true;
	}

}
