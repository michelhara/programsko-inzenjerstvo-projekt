package ProgChad.PomoziMi.DTO;

/**
 * Korišten kada korisnik želi promijeniti svoju lozinku.
 */
public class ResetPasswordDTO implements DTO{

    /**
     * Korisnikov username
     */
    private String username;

    /**
     * Korisnikov email
     */
    private String email;

    public ResetPasswordDTO(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean emptyOrNull() {
        return username != null && !username.isEmpty() && email != null && !email.isEmpty();
    }

    @Override
    public boolean equalUnique() {
        return !email.equals(username);
    }
}
