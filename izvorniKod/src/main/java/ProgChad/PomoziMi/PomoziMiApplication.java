package ProgChad.PomoziMi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PomoziMiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PomoziMiApplication.class, args);
	}

}
