package ProgChad.PomoziMi.profile;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties("spring.datasource")
public class DBprofile {

    @Profile("dev")
    @Bean
    public void h2Connection() {
        System.out.println("This configuration connects to the h2 database!");
    }

    @Profile("prod")
    @Bean
    public void herokuConnection() {
        System.out.println("This configuration connects to the heroku database!");
    }

}
