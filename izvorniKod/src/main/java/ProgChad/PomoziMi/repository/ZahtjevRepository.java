package ProgChad.PomoziMi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ProgChad.PomoziMi.domain.Zahtjev;
import ProgChad.PomoziMi.domain.ZahtjevId;

public interface ZahtjevRepository extends JpaRepository<Zahtjev, ZahtjevId> {
	
	@Query("SELECT z FROM Zahtjev z WHERE z.korisnikId.korisnikId = :kid AND z.zahtjevId = :zid") 
	Zahtjev findByKorisnikIdAndZahtjevId(@Param("kid") Integer korisnikId, @Param("zid") Integer zahtjevId);
	
	@Query("SELECT z FROM Zahtjev z WHERE z.zahtjevId = :zid") 
	Zahtjev findByZahtjevId(@Param("zid") Integer zahtjevId);
	
	@Query("SELECT z FROM Zahtjev z WHERE z.korisnikId.korisnikId = :kid " +
			"AND z.zahtjevId NOT IN (SELECT b.zahtjevId.zahtjevId FROM BlokiraniZahtjev b) " +
			"ORDER BY z.datumNastanka")
	List<Zahtjev> findAllByKorisnikId(@Param("kid") Integer korisnikId);
	
	@Query("SELECT z FROM Zahtjev z WHERE z.korisnikId.korisnikId = :kid AND z.sakriven != true AND z.izvrsen != true " +
			"AND z.zahtjevId NOT IN (SELECT b.zahtjevId.zahtjevId FROM BlokiraniZahtjev b) " +
			"AND :loggedKid NOT IN (SELECT k.korisnikId FROM z.sakriveni_zahtjev k) " +
			"AND (z.rok >= CURRENT_TIMESTAMP OR z.rok IS NULL) " +
			"ORDER BY z.datumNastanka")
	List<Zahtjev> findAllFilteredByKorisnikId(@Param("kid") Integer korisnikId, @Param("loggedKid") Integer loggedInUser);

	@Query("SELECT z FROM Zahtjev z WHERE z.prihvacaKorisnikId.korisnikId = :kid AND z.izvrsen = true " +
			"AND z.zahtjevId NOT IN (SELECT b.zahtjevId.zahtjevId FROM BlokiraniZahtjev b) " +
			"ORDER BY z.datumNastanka")
	List<Zahtjev> findAllFinishedFilteredByKorisnikId(@Param("kid") Integer korisnikId);

	@Query("SELECT z FROM Zahtjev z WHERE z.sakriven != true AND z.izvrsen != true " +
			"AND z.zahtjevId NOT IN (SELECT b.zahtjevId.zahtjevId FROM BlokiraniZahtjev b) " +
			"AND z.korisnikId.korisnikId NOT IN (SELECT b.korisnikId.korisnikId FROM BlokiraniKorisnik b)" +
			"AND :kid NOT IN (SELECT k.korisnikId FROM z.sakriveni_zahtjev k) " +
			"AND z.longitude IS NULL AND z.latitude IS NULL " +
			"AND (z.rok >= CURRENT_TIMESTAMP OR z.rok IS NULL) " +
			"ORDER BY z.datumNastanka")
	List<Zahtjev> listZahtjevNoLocation(@Param("kid") Integer korisnikId);

	@Query("SELECT z FROM Zahtjev z WHERE z.sakriven != true AND z.izvrsen != true " +
			"AND z.zahtjevId NOT IN (SELECT b.zahtjevId.zahtjevId FROM BlokiraniZahtjev b) " +
			"AND z.korisnikId.korisnikId NOT IN (SELECT b.korisnikId.korisnikId FROM BlokiraniKorisnik b)" +
			"AND :kid NOT IN (SELECT k.korisnikId FROM z.sakriveni_zahtjev k)" +
			"AND z.longitude IS NOT NULL AND z.latitude IS NOT NULL " +
			"AND (z.rok >= CURRENT_TIMESTAMP OR z.rok IS NULL) " +
			"ORDER BY z.datumNastanka")
	List<Zahtjev> listZahtjevLocation(@Param("kid") Integer korisnikId);
	
	@Query("SELECT z FROM Zahtjev z WHERE z.sakriven != true AND z.izvrsen != true " +
			"AND z.kategorija = :c " +
			"AND z.zahtjevId NOT IN (SELECT b.zahtjevId.zahtjevId FROM BlokiraniZahtjev b) " +
			"AND z.korisnikId.korisnikId NOT IN (SELECT b.korisnikId.korisnikId FROM BlokiraniKorisnik b)" +
			"AND :kid NOT IN (SELECT k.korisnikId FROM z.sakriveni_zahtjev k) " +
			"AND z.longitude IS NULL AND z.latitude IS NULL " +
			"AND (z.rok >= CURRENT_TIMESTAMP OR z.rok IS NULL) " +
			"ORDER BY z.datumNastanka")
	List<Zahtjev> listZahtjevNoLocationByCategory(@Param("kid") Integer korisnikId, @Param("c") String kategorija);

	@Query("SELECT z FROM Zahtjev z WHERE z.sakriven != true AND z.izvrsen != true " +
			"AND z.kategorija = :c " +
			"AND z.zahtjevId NOT IN (SELECT b.zahtjevId.zahtjevId FROM BlokiraniZahtjev b) " +
			"AND z.korisnikId.korisnikId NOT IN (SELECT b.korisnikId.korisnikId FROM BlokiraniKorisnik b)" +
			"AND :kid NOT IN (SELECT k.korisnikId FROM z.sakriveni_zahtjev k)" +
			"AND z.longitude IS NOT NULL AND z.latitude IS NOT NULL " +
			"AND (z.rok >= CURRENT_TIMESTAMP OR z.rok IS NULL) " +
			"ORDER BY z.datumNastanka")
	List<Zahtjev> listZahtjevLocationByCategory(@Param("kid") Integer korisnikId, @Param("c") String kategorija);

	@Query("SELECT z FROM Zahtjev z WHERE z.izvrsen != true " +
			"AND z.zahtjevId IN (SELECT b.zahtjevId.zahtjevId FROM BlokiraniZahtjev b)")
	List<Zahtjev> listAllBlocked();
}
