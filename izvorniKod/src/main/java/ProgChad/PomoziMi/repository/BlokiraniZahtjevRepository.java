package ProgChad.PomoziMi.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ProgChad.PomoziMi.domain.BlokiraniZahtjev;
import ProgChad.PomoziMi.domain.BlokiraniZahtjevId;
import ProgChad.PomoziMi.domain.Zahtjev;

public interface BlokiraniZahtjevRepository extends JpaRepository<BlokiraniZahtjev, BlokiraniZahtjevId> { 
	
	@Query("SELECT z FROM Zahtjev z WHERE z.zahtjevId IN (SELECT b.zahtjevId.zahtjevId FROM BlokiraniZahtjev b)") 
	List<Zahtjev> findAllBlokiraniZahtjevi();
	
	@Query("SELECT z FROM Zahtjev z WHERE z.zahtjevId IN (SELECT b.zahtjevId.zahtjevId FROM BlokiraniZahtjev b) " +
			"AND z.korisnikId.korisnikId = :kid") 
	List<Zahtjev> findAllByKorisnikId(@Param("kid") Integer korisnikId);
	
	@Query("SELECT b FROM BlokiraniZahtjev b WHERE b.zahtjevId.korisnikId = :kid AND b.zahtjevId.zahtjevId = :zid")
	BlokiraniZahtjev findByKorisnikIdAndZahtjevId(@Param("kid") Integer korisnikId, @Param("zid") Integer zahtjevId);
	
	@Query("SELECT b FROM BlokiraniZahtjev b WHERE b.zahtjevId.zahtjevId = :zid")
	BlokiraniZahtjev findByZahtjevId(@Param("zid") Integer zahtjevId);

	@Transactional
	@Modifying
	@Query("DELETE FROM BlokiraniZahtjev b WHERE b.zahtjevId.zahtjevId = :bz")
	void deleteBlokiraniZahtjev(@Param("bz") Integer blokiraniZahtjevId);

}
