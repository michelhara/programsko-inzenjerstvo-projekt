package ProgChad.PomoziMi.repository;

import ProgChad.PomoziMi.domain.RegistrationToken;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repozitorij koji služi za upotpunjavanje registracije.
 */
public interface RegistrationTokenRepository extends JpaRepository<RegistrationToken, Long> {

    RegistrationToken findByToken(String token);

}
