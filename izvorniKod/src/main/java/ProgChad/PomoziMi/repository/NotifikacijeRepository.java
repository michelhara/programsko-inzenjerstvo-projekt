package ProgChad.PomoziMi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ProgChad.PomoziMi.domain.Notifikacije;

public interface NotifikacijeRepository extends JpaRepository<Notifikacije, Integer> {
	
	Notifikacije findByNotifikacijaId(Integer notifikacijaId);

}
