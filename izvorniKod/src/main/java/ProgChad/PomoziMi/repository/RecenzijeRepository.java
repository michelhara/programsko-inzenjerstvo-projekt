package ProgChad.PomoziMi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ProgChad.PomoziMi.domain.Recenzija;
import ProgChad.PomoziMi.domain.RecenzijaId;

public interface RecenzijeRepository extends JpaRepository<Recenzija, RecenzijaId>  {

	@Query("SELECT r FROM Recenzija r WHERE r.recenziraKorisnik.korisnikId = :kid")
	List<Recenzija> findByKorisnikId(@Param("kid") Integer korisnikId);

	@Query("SELECT r FROM Recenzija r WHERE r.recenziraniKorisnik.korisnikId = :kid")
	List<Recenzija> findByRecenziraniKorisnikId(@Param("kid") Integer korisnikId);

	@Query("SELECT r FROM Recenzija r WHERE r.recenziraniKorisnik.korisnikId = :recenKor AND r.recenziraKorisnik.korisnikId = :recKor")
	Recenzija korisnikOcijenioKorisnika(@Param("recKor") Integer recenziraKorisnik, @Param("recenKor") Integer recenziraniKorisnik);
}
