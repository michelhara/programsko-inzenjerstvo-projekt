package ProgChad.PomoziMi.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ProgChad.PomoziMi.domain.BlokiraniKorisnik;
import ProgChad.PomoziMi.domain.Korisnik;

public interface BlokiraniKorisnikRepository extends JpaRepository<BlokiraniKorisnik, Integer> {

	@Query("SELECT k FROM Korisnik k WHERE k.korisnikId IN (SELECT b.korisnikId.korisnikId FROM BlokiraniKorisnik b)")
	List<Korisnik> findAllBlokiraniKorisnici();

	@Query("SELECT b FROM BlokiraniKorisnik b WHERE b.korisnikId.korisnikId = :bkid")
	BlokiraniKorisnik findByKorisnikId(@Param("bkid") Integer korisnikId);

	@Transactional
	@Modifying
	@Query("DELETE FROM BlokiraniKorisnik b WHERE b.korisnikId.korisnikId = :bkid")
	void deleteBlokiraniKorisnik(@Param("bkid") Integer korisnikID);

}
