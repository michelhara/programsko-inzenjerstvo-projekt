package ProgChad.PomoziMi.repository;

import ProgChad.PomoziMi.domain.Korisnik;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface KorisnikRepository extends JpaRepository<Korisnik, Integer> {

    /**
     * Metoda vraća korisnika s istim e-mailom.
     * @param email
     * @return
     */
    Korisnik findByEmail(String email);

    /**
     * Metoda vraća korisnika s istim usernameom
     * @param username
     * @return
     */
    Korisnik findByUsername(String username);
    
    /**
     * Metoda vraća korisnika s istim ID-om
     * @param korisnikId
     * @return
     */
    Korisnik findByKorisnikId(Integer korisnikId);

    @Query("SELECT k FROM Korisnik k WHERE k.korisnikId IN (SELECT b.korisnikId.korisnikId FROM BlokiraniKorisnik b)")
	List<Korisnik> listAllBlocked();

}
