package ProgChad.PomoziMi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ProgChad.PomoziMi.domain.Kontakti;
import ProgChad.PomoziMi.domain.KontaktiId;

public interface KontaktiRepository extends JpaRepository<Kontakti, KontaktiId> {
	
	@Query("SELECT k FROM Kontakti k WHERE k.korisnikId.korisnikId = :kid") 
	List<Kontakti> findByKorisnikId(@Param("kid") Integer korisnikId);

	@Query("SELECT k FROM Kontakti k WHERE k.kontaktId = :kontaktId")
	Kontakti findByKontaktId(@Param("kontaktId") Integer kontaktId);

}
