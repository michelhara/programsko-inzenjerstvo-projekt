package ProgChad.PomoziMi.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.jdbcAuthentication().dataSource(dataSource)
        .usersByUsernameQuery("select username, password, enabled from korisnik where username = ?")
        .authoritiesByUsernameQuery("select username, uloga from korisnik where username = ?");

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic();

        http.authorizeRequests()
        	.antMatchers("/").permitAll()
            .antMatchers("/users/register").permitAll()
            .antMatchers("/resources/**").permitAll()
            .antMatchers("/h2-console/**").permitAll()
            .antMatchers("/home").hasAnyRole("USER, ADMIN")
                .antMatchers("/user/**").hasAnyRole("USER, ADMIN")
            .and()
                .formLogin()
                .loginPage("/")
                .defaultSuccessUrl("/home")
            .and()
                .logout()
                .logoutSuccessUrl("/?logout=true")
                .invalidateHttpSession(true)
                .permitAll();

 		http.headers().frameOptions().sameOrigin();        
        http.csrf().disable();  
    }

    @Bean
    public PasswordEncoder pswdEncoder() {
        return new BCryptPasswordEncoder();
    }

}
