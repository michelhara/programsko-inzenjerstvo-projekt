package ProgChad.PomoziMi.Controller;

import ProgChad.PomoziMi.DTO.ResetPasswordDTO;
import ProgChad.PomoziMi.domain.Recenzija;
import ProgChad.PomoziMi.mailer.EmailSenderService;
import ProgChad.PomoziMi.service.KorisnikService;
import ProgChad.PomoziMi.service.RecenzijeService;
import ProgChad.PomoziMi.service.ZahtjevService;
import ProgChad.PomoziMi.service.validation.impl.ResetFormValidation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Zahtjev;

import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import java.util.Collections;
import java.util.List;

@Controller
public class PomoziMiController {

	/**
	 * Servis zadužen za korisnika
	 */
	@Autowired
	private KorisnikService korisnikService;
	
	/**
	 * Servis zadužen za zahtjeve
	 */
	@Autowired
	private ZahtjevService zahtjevService;

	/**
	 * Servis zadužen za slanje emaila.
	 */
	@Autowired
	private EmailSenderService emailSenderService;

	/**
	 * Provjera validnosti forme
	 */
	@Autowired
	private ResetFormValidation resetFormValidation;
	
	/**
	 * Servis zadužen za recenzija
	 */
	@Autowired
	private RecenzijeService recenzijeService;
    
    @GetMapping("/home")
    public String User(ModelMap model){
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	if(!(authentication instanceof AnonymousAuthenticationToken)) {
    		String currentUserName = authentication.getName();
    		Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
    		model.addAttribute("username", loggedInUser.getUsername());
			model.addAttribute("korisnikId", loggedInUser.getKorisnikId());
			model.addAttribute("ime", loggedInUser.getIme());
    		model.addAttribute("prezime", loggedInUser.getPrezime());
    		model.addAttribute("uloga", loggedInUser.getUloga());
    		model.addAttribute("email", loggedInUser.getEmail());
    		model.addAttribute("kontakti", ControllerUtils.transformToKontaktiDTOJson(loggedInUser.getKontakti()));
    		model.addAttribute("recenzije", ControllerUtils.transformToRecenzijeDTOJson(loggedInUser.getRecenzije()));
    		model.addAttribute("notifikacije", ControllerUtils.transformToNotifikacijaDTOJson(loggedInUser.getNotifikacije()));
			model.addAttribute("brojNotifikacija", loggedInUser.getNotifikacije().size());

			if (loggedInUser.getUloga().equals("ROLE_ADMIN")) {
    			List<Korisnik> blokiraniKorisnici = korisnikService.listAllBlocked();
    			model.addAttribute("blokiraniKorisnici", ControllerUtils.transformToMnogiKorisniciDTOJson(blokiraniKorisnici));
    		}
    		
    		List<Zahtjev> zahtjevi = zahtjevService.listAllFilteredNoLocation(loggedInUser.getKorisnikId());
			Collections.reverse(zahtjevi);
			model.addAttribute("zahtjevi", ControllerUtils.transformToZahtjevDTOJson(zahtjevi, loggedInUser));
    		
    	}
        return "home";
    }

	@GetMapping("/user/{userName}")
	public String AnotherUser(@PathVariable("userName") String userName, ModelMap model){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(!(authentication instanceof AnonymousAuthenticationToken)) {
			Korisnik loggedInUser;
			try {
				loggedInUser = korisnikService.findByUsername(authentication.getName());
				if(loggedInUser.getUsername().equals(userName))
					return "redirect:/home";
			} catch (IllegalArgumentException ex) {
				return "redirect:/home";
			}

			Korisnik user = korisnikService.findByUsername(userName);
			model.addAttribute("username", user.getUsername());
			model.addAttribute("korisnikId", loggedInUser.getKorisnikId());
			model.addAttribute("anotherKorisnikId", user.getKorisnikId());
			model.addAttribute("ime", user.getIme());
			model.addAttribute("prezime", user.getPrezime());
			model.addAttribute("uloga", loggedInUser.getUloga());
			model.addAttribute("email", user.getEmail());
			model.addAttribute("ocijenio", recenzijeService.ocijenioKorisnik(loggedInUser, user));
			
			Boolean blokiran = korisnikService.isBlocked(user.getKorisnikId());
			
			model.addAttribute("blokiran", blokiran);

			model.addAttribute("recenzije", ControllerUtils.transformToRecenzijeDTOJson(user.getRecenzije()));
			
			List<Recenzija> recenzijePrijavljenogKorisnika = recenzijeService.findByKorisnikId(loggedInUser.getKorisnikId());
			
			model.addAttribute("recenzijePrijavljenogKorisnika", ControllerUtils.transformToRecenzijeDTOJson(recenzijePrijavljenogKorisnika));

			List<Zahtjev> zahtjevi = zahtjevService.listAllFilteredByKorisnikId(user.getKorisnikId(), loggedInUser.getKorisnikId());
			Collections.reverse(zahtjevi);
			model.addAttribute("zahtjevi", ControllerUtils.transformToZahtjevDTOJson(zahtjevi, loggedInUser));
		}
		return "anotherUser";
	}

    @GetMapping("/")
    public String login(@RequestParam(value="error", required= false) String error,
    					@RequestParam(value="logout", required=false) String logout,
    					@RequestParam(value="confirmed", required = false) String confirmed,
						@RequestParam(value="not_confirmed", required = false) String not_confirmed,
						@RequestParam(value="changed", required = false) String changed,
						@RequestParam(value="not_changed", required = false) String not_changed,
						Model model) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(!(authentication instanceof AnonymousAuthenticationToken))
			return "redirect:/home";

    	String errorMessage = null;
    	if(error != null) {
    		errorMessage = "Korisničko ime ili lozinka su netočni!";
    		model.addAttribute("forgotten", true);
    	}

    	if(logout != null){
    		errorMessage = "Uspješno ste odjavljeni!";
    	}

    	if(confirmed != null) {
			errorMessage = "Uspješno ste registrirani!";
		}

		if(not_confirmed != null) {
			errorMessage = "Registracija je bila neuspješna! Token je vrlo vjerojatno istekao.";
		}

		if(changed != null) {
			errorMessage = "Upravo Vam je poslana nova lozinka na vaš email sandučić!";
		}

		if(not_changed != null) {
			errorMessage = "Došlo je do pogreške kod mijenjanja vaše lozinke. Pokušajte ponovno!";
			model.addAttribute("forgotten", true);
		}

    	model.addAttribute("errorMessage", errorMessage);
		return "index";
    }

	/**
	 * Controller metoda koja je zadužena za pozivanje provjere tokena i preusmjeravanje na login stranicu ako je uspješno token potvrđen.
	 * @param confirmation_token
	 * @return
	 */
	@GetMapping("/register/{confirmation_token}")
	public ModelAndView  confirmRegister(@PathVariable("confirmation_token") String confirmation_token) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(!(authentication instanceof AnonymousAuthenticationToken))
			return new ModelAndView( "redirect:/home");
		if(korisnikService.confirmKorisnik(confirmation_token)) {
			return new ModelAndView("redirect:/?confirmed");
		}
		else {
			return new ModelAndView("redirect:/?not_confirmed");
		}
	}

	/**
	 * Metoda koja se koristi kada korisnik zaboravi svoju lozinku.
	 * @param model
	 * @return
	 */
	@GetMapping("/forgotten_password")
	public String forgotPassword(Model model) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(!(authentication instanceof AnonymousAuthenticationToken))
			return "redirect:/home";
		return "forgotten_password";
	}

	/**
	 * Metoda mijenja korisničku lozinku te istovremeno šalje novu korisničku lozinku korisniku na email.
	 * @param resetPasswordDTO
	 * @return
	 */
	@PostMapping("/forgotten_password")
	public ModelAndView forgotPassword( ResetPasswordDTO resetPasswordDTO) {
		ModelAndView modelAndView = new ModelAndView();
		if(!resetFormValidation.validiraj(resetPasswordDTO)){
			modelAndView.setViewName("forgotten_password");
			modelAndView.addObject("username", resetPasswordDTO.getUsername());
			modelAndView.addObject("email", resetPasswordDTO.getEmail());
			modelAndView.addObject("Error", "Molimo Vas ispravno ispunite sva polja!");
			return modelAndView;
		}
		String newPassword = korisnikService.changePassword(resetPasswordDTO);
		if(newPassword == null) {
			modelAndView.setViewName("forgotten_password");
			modelAndView.addObject("username", resetPasswordDTO.getUsername());
			modelAndView.addObject("email", resetPasswordDTO.getEmail());
			modelAndView.addObject("Error", "Korisničko ime ili email nisu ispravno uneseni!");
			return modelAndView;
		}
		else {
			try {
				emailSenderService.sendNewPasswordMail(resetPasswordDTO,  newPassword);
			} catch (MessagingException me) {
				modelAndView.setViewName("redirect:/?not_changed");
			}
			modelAndView.setViewName("redirect:/?changed");
		}
		return modelAndView;
	}
}
