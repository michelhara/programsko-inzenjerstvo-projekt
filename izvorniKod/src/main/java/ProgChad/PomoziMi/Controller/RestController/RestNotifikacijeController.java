package ProgChad.PomoziMi.Controller.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ProgChad.PomoziMi.service.NotifikacijeService;

@RestController
public class RestNotifikacijeController {
	
	@Autowired
	private NotifikacijeService notifikacijeService;
	
	@DeleteMapping("/notification/delete/{notificationId}")
	public ResponseEntity<Object> deleteNotification (@PathVariable("notificationId") Integer id) {
		notifikacijeService.deleteNotification(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

}
