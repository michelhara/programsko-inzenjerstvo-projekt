package ProgChad.PomoziMi.Controller.RestController;

import ProgChad.PomoziMi.Controller.ControllerUtils;
import ProgChad.PomoziMi.DTO.JSON.KorisnikDTOJson;
import ProgChad.PomoziMi.DTO.JSON.ZahtjevDTOJson;
import ProgChad.PomoziMi.DTO.ZahtjevDTO;
import ProgChad.PomoziMi.domain.Kontakti;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Notifikacije;
import ProgChad.PomoziMi.domain.Zahtjev;
import ProgChad.PomoziMi.mailer.EmailSenderService;
import ProgChad.PomoziMi.mapper.ZahtjevMapper;
import ProgChad.PomoziMi.service.*;
import ProgChad.PomoziMi.service.validation.Validacija;
import ProgChad.PomoziMi.service.validation.impl.ZahtjevValidation;
import org.openqa.selenium.InvalidArgumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@RestController
public class RestZahtjevController {

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private ZahtjevService zahtjevService;

    @Autowired
    private EmailSenderService emailSenderService;

    @Autowired
    private KontaktiService kontaktiService;

    @Autowired
    private BlokiraniZahtjevService blokiraniZahtjevService;

    @Autowired
    private NotifikacijeService notifikacijeService;


    @PostMapping("/createZahtjev")
    public ResponseEntity<ZahtjevDTOJson> createZahtjev(@RequestBody ZahtjevDTO zahtjevDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Validacija validacija = new ZahtjevValidation();

            if (validacija.validiraj(zahtjevDTO)) {
                String currentUserName = authentication.getName();
                Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

                //U slucaju da se stvara novi kontakt potrebno je vratiti id tog kontakta.
                Kontakti noviKontakt = null;

                if(zahtjevDTO.getSpremiKontakt() && zahtjevDTO.getBrojTelefona() != null) {
                    try {
                        noviKontakt = kontaktiService.saveKontakt(new Kontakti(loggedInUser, zahtjevDTO.getBrojTelefona()));
                    } catch (Exception ignored) {}
                }

                Zahtjev zahtjev = zahtjevService.createZahtjev(loggedInUser.getKorisnikId(),
                        ZahtjevMapper.modelToDTO(zahtjevDTO, korisnikService));


                if (zahtjev != null) {
                    ZahtjevDTOJson zahtjevDTOJson = ControllerUtils.transformSingleZahtjevToDTOJson(loggedInUser, zahtjev);
                    if(noviKontakt != null)
                        zahtjevDTOJson.setKontaktId(noviKontakt.getKontaktId());
                    return ResponseEntity.status(HttpStatus.CREATED).body(zahtjevDTOJson);
                } else {
                    throw new IllegalArgumentException("Neispravan zahtjev!");
                }

            } else {
                throw new IllegalArgumentException("Neispravna forma izrade zahtjeva!");
            }

        } else {
            throw new IllegalArgumentException("Potrebna prijava korisnika za izradu zahtjeva!");
        }

    }

    @PutMapping("/updateZahtjev/{zahtjevId}")
    public ResponseEntity<?> updateZahtjev(@PathVariable("zahtjevId") Integer zahtjevId,
                                           @RequestBody ZahtjevDTO zahtjevDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Validacija validacija = new ZahtjevValidation();

            if (validacija.validiraj(zahtjevDTO)) {
                String currentUserName = authentication.getName();
                Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

                Zahtjev zahtjev = zahtjevService.updateZahtjev(loggedInUser.getKorisnikId(), zahtjevId,
                        ZahtjevMapper.modelToDTO(zahtjevDTO, korisnikService));

                if (zahtjev != null) {
                    return ResponseEntity.status(HttpStatus.OK).body(ControllerUtils.transformSingleZahtjevToDTOJson(loggedInUser, zahtjev));
                } else {
                    throw new IllegalArgumentException("Vaš zahtjev s ID-om " + zahtjevId + " ne postoji!");
                }

            } else {
                throw new IllegalArgumentException("Neispravna forma izmjene zahtjeva!");
            }

        } else {
            throw new IllegalArgumentException("Potrebna prijava korisnika za izmjenu zahtjeva!");
        }

    }

    @DeleteMapping("/deleteZahtjev/{zahtjevId}")
    public ResponseEntity<?> deleteZahtjev(@PathVariable("zahtjevId") Integer zahtjevId) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

            Boolean zahtjevObrisan = zahtjevService.deleteZahtjev(loggedInUser.getKorisnikId(), zahtjevId);

            if (zahtjevObrisan) {
                return ResponseEntity.status(HttpStatus.OK).build();
            } else {
                throw new IllegalArgumentException("Nemate dovoljne razine ovlasti za ovu akciju!");
            }
        } else {
            throw new IllegalArgumentException("Potrebna prijava korisnika za brisanje zahtjeva!");
        }

    }

    @PutMapping("/hideZahtjev/{zahtjevId}")
    public ResponseEntity<?> hideZahtjev(@PathVariable("zahtjevId") Integer zahtjevId) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

            Boolean zahtjevSakriven = zahtjevService.hideZahtjev(loggedInUser.getKorisnikId(), zahtjevId);

            if (zahtjevSakriven) {
                Zahtjev currentZahtjev = zahtjevService.findByZahtjevId(zahtjevId);
                Zahtjev updatedZahtjev = zahtjevService.updateZahtjev(currentZahtjev.getKorisnikId().getKorisnikId(),
                        zahtjevId, currentZahtjev);
                Korisnik updatedKorisnik = korisnikService.updateKorisnik(loggedInUser.getKorisnikId(), loggedInUser);
                return ResponseEntity.status(HttpStatus.OK).build();
            } else {
                throw new IllegalArgumentException("Vaš zahtjev s ID-om " + zahtjevId + " ne postoji!");
            }
        } else {
            throw new IllegalArgumentException("Potrebna prijava korisnika za sakrivanje zahtjeva!");
        }
    }

    @PutMapping("/unhideZahtjev/{zahtjevId}")
    public ResponseEntity<?> unhideZahtjev(@PathVariable("zahtjevId") Integer zahtjevId) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

            Boolean zahtjevOtkriven = zahtjevService.unhideZahtjev(loggedInUser.getKorisnikId(), zahtjevId);

            if (zahtjevOtkriven) {
                Zahtjev currentZahtjev = zahtjevService.findByZahtjevId(zahtjevId);
                Zahtjev updatedZahtjev = zahtjevService.updateZahtjev(currentZahtjev.getKorisnikId().getKorisnikId(),
                        zahtjevId, currentZahtjev);
                Korisnik updatedKorisnik = korisnikService.updateKorisnik(loggedInUser.getKorisnikId(), loggedInUser);
                return ResponseEntity.status(HttpStatus.OK).build();
            } else {
                throw new IllegalArgumentException("Vaš zahtjev s ID-om " + zahtjevId + " ne postoji!");
            }
        } else {
            throw new IllegalArgumentException("Potrebna prijava korisnika za razotkrivanje zahtjeva!");
        }
    }

    @GetMapping("/hideZahtjev/{zahtjevId}")
    public Set<KorisnikDTOJson> getHiddenZahtjev(@PathVariable("zahtjevId") Integer zahtjevId) {
        Set<Korisnik> korisniciSakrili = zahtjevService.findByZahtjevId(zahtjevId).getSakriveniZahtjev();
        Set<KorisnikDTOJson> returnKorisniciSakrili = new LinkedHashSet<>();
        for(Korisnik k : korisniciSakrili) {
            returnKorisniciSakrili.add(ControllerUtils.transformToKorisnikDTOJson(k));
        }

        return returnKorisniciSakrili;
    }


    @PutMapping("/blockZahtjev/{zahtjevId}")
    public ResponseEntity<?> blockZahtjev(@PathVariable("zahtjevId") Integer zahtjevId) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

            if (!loggedInUser.getUloga().equals("ROLE_ADMIN")) {
                throw new IllegalArgumentException("Nemate dovoljne razine ovlasti za ovu akciju!");
            }

            Boolean zahtjevBlokiran = blokiraniZahtjevService.blockZahtjev(zahtjevId);

            if (zahtjevBlokiran) {
                return ResponseEntity.status(HttpStatus.OK).build();
            } else {
                throw new IllegalArgumentException("Greška pri blokiranju zahtjeva!");
            }
        } else {
            throw new IllegalArgumentException("Potrebna prijava korisnika za blokiranje zahtjeva!");
        }

    }

    @PutMapping("/unblockZahtjev/{zahtjevId}")
    public ResponseEntity<?> unblockZahtjev(@PathVariable("zahtjevId") Integer zahtjevId) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

            if (!loggedInUser.getUloga().equals("ROLE_ADMIN")) {
                throw new IllegalArgumentException("Nemate dovoljne razine ovlasti za ovu akciju!");
            }

            Boolean zahtjevDeblokiran = blokiraniZahtjevService.unblockZahtjev(zahtjevId);

            if (zahtjevDeblokiran) {
                return ResponseEntity.status(HttpStatus.OK).build();
            } else {
                throw new IllegalArgumentException("Greška pri deblokiranju zahtjeva!");
            }
        } else {
            throw new IllegalArgumentException("Potrebna prijava korisnika za deblokiranje zahtjeva!");
        }

    }

    /**
     * Razlika između ove slične funkcije je u tome što ova služi za dohvaćanje zahtjeva koji će se prikazati korisniku.
     * @param filter
     * @param category
     * @param distance
     * @param latitude
     * @param longitude
     * @return
     */
    @GetMapping("/zahtjevi")
    public ResponseEntity<List<ZahtjevDTOJson>> getZahtjevi(@RequestParam(value = "filter", required = true) String filter,
                                                           @RequestParam(value = "category", required = false) String category,
                                                           @RequestParam(value = "distance", required = false) Integer distance,
                                                           @RequestParam(value = "lat", required = false) Double latitude,
                                                           @RequestParam(value = "long", required = false) Double longitude) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken)
            throw new IllegalArgumentException("Potrebna prijava korisnika za dobivanje liste zahtjeva!");

        String currentUserName = authentication.getName();
        Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

        List<Zahtjev> zahtjevi;
        if(filter.equals("location")) {
            //dodati ako je po kategorijama filtriranje
            if(distance == null || latitude == null || longitude == null)
                throw new IllegalArgumentException("Ako se filtrira po lokaciji, distance, lat i long su ključni!");

            if (category.equals("sve")) {
                zahtjevi = zahtjevService.listAllFilteredLocation(loggedInUser.getKorisnikId(), distance, latitude, longitude);
            } else {
                zahtjevi = zahtjevService.listAllFilteredLocationByCategory(loggedInUser.getKorisnikId(), category, distance, latitude, longitude);

            }
        }
        else {
            if (category.equals("sve")) {
                zahtjevi = zahtjevService.listAllFilteredNoLocation(loggedInUser.getKorisnikId());
            } else {
                zahtjevi = zahtjevService.listAllFilteredNoLocationByCategory(loggedInUser.getKorisnikId(), category);
            }
        }
        return ResponseEntity.ok().body(ControllerUtils.transformToZahtjevDTOJson(zahtjevi, loggedInUser));
    }

    @GetMapping("/zahtjeviKorisnika")
    public ResponseEntity<List<ZahtjevDTOJson>> getZahtjeviKorisnika(@RequestParam(value = "userId", required = false) Integer userId,
                                                           @RequestParam(value = "finished", required = false) String finished) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken)
            throw new IllegalArgumentException("Potrebna prijava korisnika za dobivanje liste zahtjeva!");

        String currentUserName = authentication.getName();
        Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
        List<Zahtjev> zahtjevi;

        if(userId == null || loggedInUser.getKorisnikId().equals(userId))
            zahtjevi = zahtjevService.listAllByKorisnikId(loggedInUser.getKorisnikId());
        else if(loggedInUser.getUloga().equals("ROLE_ADMIN"))
            zahtjevi = zahtjevService.listAllFilteredByKorisnikId(userId, loggedInUser.getKorisnikId());
        else
            throw new IllegalArgumentException("Potrebne su adminske ovlasti za akciju!");

        return ResponseEntity.ok().body(ControllerUtils.transformToZahtjevDTOJson(zahtjevi, loggedInUser));
    }

    @GetMapping("/zahtjeviIzvrseni")
    public ResponseEntity<List<ZahtjevDTOJson>> getIzvrseniZahtjevi() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken)
            throw new IllegalArgumentException("Potrebna prijava korisnika za dobivanje izvrsene liste zahtjeva!");

        String currentUserName = authentication.getName();
        Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
        List<Zahtjev> zahtjevi;

        zahtjevi = zahtjevService.listAllFinishedFilteredByKorisnikId(loggedInUser.getKorisnikId());

        return ResponseEntity.ok().body(ControllerUtils.transformToZahtjevDTOJson(zahtjevi, loggedInUser));
    }
    
    @GetMapping("/zahtjeviBlokirani")
    public ResponseEntity<List<ZahtjevDTOJson>> getBlokiraniZahtjevi() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken)
            throw new IllegalArgumentException("Potrebna prijava korisnika za dobivanje liste blokiranih zahtjeva!");
        
        String currentUserName = authentication.getName();
        Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
        
        if (!loggedInUser.getUloga().equals("ROLE_ADMIN")) {
            throw new IllegalArgumentException("Nemate dovoljne razine ovlasti za ovu akciju!");
        }
        
        List<Zahtjev> zahtjevi = zahtjevService.listAllBlocked();

        return ResponseEntity.ok().body(ControllerUtils.transformToZahtjevDTOJson(zahtjevi, loggedInUser));
    }
    
    @PutMapping("/helpZahtjev/{zahtjevId}")
    public ResponseEntity<?> helpZahtjev(@PathVariable("zahtjevId") Integer zahtjevId){
    	Authentication authenication = SecurityContextHolder.getContext().getAuthentication();
    	if(authenication instanceof AnonymousAuthenticationToken)
    		throw new IllegalArgumentException("Potrebna prijava korisnika za pomaganje drugim korisnicima!");
    
    	String currentUserName = authenication.getName();
    	Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
    	
    	Zahtjev currentZahtjev = zahtjevService.findByZahtjevId(zahtjevId);

    	if(currentZahtjev == null)
    	    throw new IllegalArgumentException("Ne postoji takav zahtjev!");

    	currentZahtjev.addZeliPomoci(loggedInUser);

        notifikacijeService.createNotification(currentZahtjev.getKorisnikId(), loggedInUser, currentZahtjev, true, null);

    	Zahtjev updatedZahtjev = zahtjevService.updateZahtjev(currentZahtjev.getKorisnikId().getKorisnikId(), zahtjevId, currentZahtjev);
    	return ResponseEntity.status(HttpStatus.OK).build();
    }
    
    @GetMapping("/helpZahtjev/{zahtjevId}")
    public Set<KorisnikDTOJson> getHelpZahtjev(@PathVariable("zahtjevId") Integer zahtjevId){
    	Set<Korisnik> korisniciZelePomoci = zahtjevService.findByZahtjevId(zahtjevId).getZeliPomoci();
    	Set<KorisnikDTOJson> returnKorisniciZelePomoci = new LinkedHashSet<>();
    	for(Korisnik k : korisniciZelePomoci) {
    		returnKorisniciZelePomoci.add(ControllerUtils.transformToKorisnikDTOJson(k));
    	}
    	
    	return returnKorisniciZelePomoci;
    }
    
    @PutMapping("/noHelpZahtjev/{zahtjevId}")
    public ResponseEntity<?> noHelpZahtjev(@PathVariable("zahtjevId") Integer zahtjevId){
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	if(authentication instanceof AnonymousAuthenticationToken)
    		throw new IllegalArgumentException("Potrebna prijava korisnika za prekidom pri pomaganju drugim korisnicima!");
    	
    	String currentUserName = authentication.getName();
    	Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
    	
    	Zahtjev currentZahtjev = zahtjevService.findByZahtjevId(zahtjevId);
    	currentZahtjev.removeZeliPomoci(loggedInUser);
    	
    	Zahtjev updatedZahtjev = zahtjevService.updateZahtjev(currentZahtjev.getKorisnikId().getKorisnikId(), zahtjevId, currentZahtjev);
    	return ResponseEntity.status(HttpStatus.OK).build();
    }
    
    @PutMapping("/acceptHelpZahtjev/{zahtjevId}/{korisnikId}")
    public ResponseEntity<?> acceptHelpZahtjev(@PathVariable("zahtjevId") Integer zahtjevId,
    										   @PathVariable("korisnikId") Integer korisnikId){
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	if(authentication instanceof AnonymousAuthenticationToken)
    		throw new IllegalArgumentException("Potrebna prijava korisnika za prihvaćanje pomoći!");


    	Zahtjev currentZahtjev = zahtjevService.findByZahtjevId(zahtjevId);
    	Korisnik helpKorisnik = korisnikService.findByKorisnikID(korisnikId);

    	if(currentZahtjev.getZeliPomoci().contains(helpKorisnik)) {
    		currentZahtjev.setPrihvacaKorisnikId(helpKorisnik);

            notifikacijeService.createNotification(helpKorisnik, currentZahtjev.getKorisnikId(), currentZahtjev, false, null);

    		Zahtjev updatedZahtjev = zahtjevService.updateZahtjev(currentZahtjev.getKorisnikId().getKorisnikId(), zahtjevId, currentZahtjev);
    	}else {
    		throw new IllegalArgumentException("Korisnik " + helpKorisnik.getUsername() + " nije izrazio želju za pomoć pri ovom zahtjevu!");
    	}
    	
    	return ResponseEntity.status(HttpStatus.OK).build();
    }
    
    @GetMapping("/acceptHelpZahtjev/{zahtjevId}")
    public KorisnikDTOJson getAcceptHelpZahtjev(@PathVariable("zahtjevId") Integer zahtjevId) {
    	
    	Zahtjev currentZahtjev = zahtjevService.findByZahtjevId(zahtjevId);
    	if(currentZahtjev.getPrihvacaKorisnikId() == null) {
    		throw new IllegalArgumentException("Ne postoji korisnik koji je odabran za pomoć pri ovom zahtjevu!");
    	}
    	Korisnik helpKorisnik = currentZahtjev.getPrihvacaKorisnikId();
    	return ControllerUtils.transformToKorisnikDTOJson(helpKorisnik);
    }
    
    @PutMapping("/removeHelpZahtjev/{zahtjevId}")
    public ResponseEntity<?> removeHelpZahtjev(@PathVariable("zahtjevId") Integer zahtjevId){
    	
    	Zahtjev currentZahtjev = zahtjevService.findByZahtjevId(zahtjevId);
        Korisnik odustaniKorisnik = currentZahtjev.getPrihvacaKorisnikId();
        if(odustaniKorisnik == null)
            throw new IllegalArgumentException("Nitko ne pomaže ovom zahtjevu!");

        currentZahtjev.setPrihvacaKorisnikId(null);
    	Zahtjev updatedZahtjev = zahtjevService.updateZahtjev(currentZahtjev.getKorisnikId().getKorisnikId(), zahtjevId, currentZahtjev);

    	String customMessage = " odustao je od vaše pomoći.";

        notifikacijeService.createNotification(odustaniKorisnik, currentZahtjev.getKorisnikId(), currentZahtjev, false, customMessage);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/zahtjev/{zahtjevId}")
    public ResponseEntity<ZahtjevDTOJson> getZahtjev(@PathVariable("zahtjevId") Integer zahtjevId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken)
            throw new IllegalArgumentException("Potrebna prijava korisnika za dobivanje izvrsene liste zahtjeva!");

        String currentUserName = authentication.getName();
        Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
        Zahtjev zahtjev = zahtjevService.findByZahtjevId(zahtjevId);

        if(!zahtjev.getKorisnikId().getKorisnikId().equals(loggedInUser.getKorisnikId()))
            throw new InvalidArgumentException("Ne možete dohvaćati tuđe zahtjeve!");

        return ResponseEntity.ok().body(ControllerUtils.transformSingleZahtjevToDTOJson(loggedInUser, zahtjev));
    }

}
