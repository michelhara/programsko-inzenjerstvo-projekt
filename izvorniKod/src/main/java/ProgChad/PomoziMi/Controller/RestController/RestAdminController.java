package ProgChad.PomoziMi.Controller.RestController;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import ProgChad.PomoziMi.DTO.JSON.KontaktiDTOJson;
import ProgChad.PomoziMi.DTO.JSON.KorisnikDTOJson;
import ProgChad.PomoziMi.DTO.JSON.ZahtjevDTOJson;
import ProgChad.PomoziMi.domain.Kontakti;
import ProgChad.PomoziMi.domain.RegistrationToken;
import ProgChad.PomoziMi.domain.Zahtjev;
import ProgChad.PomoziMi.mailer.EmailSenderService;
import ProgChad.PomoziMi.service.BlokiraniKorisnikService;
import ProgChad.PomoziMi.service.BlokiraniZahtjevService;
import ProgChad.PomoziMi.service.KontaktiService;
import ProgChad.PomoziMi.service.RegistrationTokenService;
import ProgChad.PomoziMi.service.ZahtjevService;
import ProgChad.PomoziMi.service.validation.impl.KorisnikValidation;
import ProgChad.PomoziMi.service.validation.impl.ZahtjevValidation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import ProgChad.PomoziMi.DTO.KorisnikDTO;
import ProgChad.PomoziMi.DTO.ZahtjevDTO;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.mapper.KorisnikMapper;
import ProgChad.PomoziMi.mapper.ZahtjevMapper;
import ProgChad.PomoziMi.service.KorisnikService;
import ProgChad.PomoziMi.service.validation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.mail.MessagingException;

@RestController
public class RestAdminController {

	@Autowired
	private KorisnikService korisnikService;

	@Autowired
	private ZahtjevService zahtjevService;

	@Autowired
	private EmailSenderService emailSenderService;

	@Autowired
	private KontaktiService kontaktiService;
	
	@Autowired
	private BlokiraniZahtjevService blokiraniZahtjevService;
	
	
	@Autowired
	private BlokiraniKorisnikService blokiraniKorisnikService;

	@PutMapping("/makeAdmin/{korisnikID}")
	public ResponseEntity<?> makeAdmin(@PathVariable("korisnikID") Integer id) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String currentUserName = authentication.getName();
			Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
			
			if (!loggedInUser.getUloga().equals("ROLE_ADMIN")) {
				throw new IllegalArgumentException("Nemate dovoljne razine ovlasti za ovu akciju!");
			}
			
			Boolean makeAdminSuccess = korisnikService.makeKorisnikAdmin(id);
			
			if (makeAdminSuccess) {
				return ResponseEntity.status(HttpStatus.OK).build();
			} else {
				throw new IllegalArgumentException("Greška pri dodjelivanju administratorskih prava korisniku!");
			}
		} else {
			throw new IllegalArgumentException("Potrebna prijava u sustav!");
		}
	}

	@PutMapping("/blockUser/{userId}")
	public ResponseEntity<?> blockUser(@PathVariable("userId") Integer userId) {
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String currentUserName = authentication.getName();
			Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
			
			if (!loggedInUser.getUloga().equals("ROLE_ADMIN")) {
				throw new IllegalArgumentException("Nemate dovoljne razine ovlasti za ovu akciju!");
			}

			Boolean korisnikBlokiran = blokiraniKorisnikService.blockKorisnik(userId);

			if (korisnikBlokiran) {
				return ResponseEntity.status(HttpStatus.OK).build();
			} else {
				throw new IllegalArgumentException("Greška pri blokiranju korisnika!");
			}
		} else {
			throw new IllegalArgumentException("Potrebna prijava korisnika za blokiranje zahtjeva!");
		}
		
	}
	
	@PutMapping("/unblockUser/{userId}")
	public ResponseEntity<?> unblockKorisnik(@PathVariable("userId") Integer userId) {
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String currentUserName = authentication.getName();
			Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
			
			if (!loggedInUser.getUloga().equals("ROLE_ADMIN")) {
				throw new IllegalArgumentException("Nemate dovoljne razine ovlasti za ovu akciju!");
			}

			Boolean korisnikDeblokiran = blokiraniKorisnikService.unblockKorisnik(userId);

			if (korisnikDeblokiran) {
				return ResponseEntity.status(HttpStatus.OK).build();
			} else {
				throw new IllegalArgumentException("Greška pri deblokiranju korisnika!");
			}
		} else {
			throw new IllegalArgumentException("Potrebna prijava korisnika za deblokiranje zahtjeva!");
		}
		
	}
	
}