package ProgChad.PomoziMi.Controller.RestController;

import ProgChad.PomoziMi.Controller.ControllerUtils;
import ProgChad.PomoziMi.DTO.JSON.RecenzijaDTOJson;
import ProgChad.PomoziMi.DTO.RecenzijaDTO;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Recenzija;
import ProgChad.PomoziMi.service.KorisnikService;
import ProgChad.PomoziMi.service.RecenzijeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
public class RestRecenzijaController {

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private RecenzijeService recenzijeService;

    @PostMapping("/dodajRecenziju")
    public ResponseEntity<RecenzijaDTOJson> dodavanjeRecenzije(@RequestBody RecenzijaDTO recenzijaDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

            Korisnik recenziraniKorisnik = korisnikService.findByUsername(recenzijaDTO.getRecenziraniKorisnikUsername());
            Recenzija recenzija = new Recenzija();
            recenzija.setKomentar(recenzijaDTO.getKomentar());
            recenzija.setOcjena(recenzijaDTO.getOcjena());
            recenzija.setRecenziraKorisnik(loggedInUser);
            recenzija.setRecenziraniKorisnik(recenziraniKorisnik);

            Recenzija returnedRecenzija = recenzijeService.createRecenzija(recenzija);

            if(returnedRecenzija != null) {
                return ResponseEntity.status(HttpStatus.OK).body(ControllerUtils.transformSingleRecenzijaToDTOJson(recenzija));
            }else {
                throw new IllegalArgumentException("Neispravna recenzija!");
            }
        }else {
            throw new IllegalArgumentException("Potrebna prijava korisnika za davanje recenzija!");
        }
    }

    @DeleteMapping("izbrisiRecenziju/{recenziraniId}")
    public ResponseEntity<?> obrisiRecenziju(@PathVariable("recenziraniId") Integer recenziraniId) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

            Korisnik recenziraniKorisnik = korisnikService.findByKorisnikID(recenziraniId);
            if(recenziraniKorisnik == null)
                throw new IllegalArgumentException("Korisnik ne postoji!");

            Recenzija recenzija = recenzijeService.findByOcijenjenIOcijenjujeId(recenziraniId, loggedInUser.getKorisnikId());
            if(recenzija == null)
                throw new IllegalArgumentException("Ta recenzija ne postoji!");

            if(!recenzijeService.deleteRecenzija(recenzija))
                throw new IllegalArgumentException("Pogreška brisanje recenzije!");

            return ResponseEntity.status(HttpStatus.OK).build();
        }else {
            throw new IllegalArgumentException("Potrebna prijava korisnika za brisanje recenzija!");
        }

    }

    @GetMapping("/recenzijeKorisnika")
    public ResponseEntity<Set<RecenzijaDTOJson>> getKorisnikoveRecenzije(@RequestParam(value = "userId", required = false) Integer userId){
        Set<RecenzijaDTOJson> recenzije = ControllerUtils.transformToRecenzijeDTOJson(recenzijeService.findByRecenziraniKorisnikId(userId));
        return ResponseEntity.ok().body(recenzije);
    }

}
