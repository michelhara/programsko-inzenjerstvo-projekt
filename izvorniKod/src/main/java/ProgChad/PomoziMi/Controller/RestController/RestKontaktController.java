package ProgChad.PomoziMi.Controller.RestController;

import ProgChad.PomoziMi.domain.Kontakti;
import ProgChad.PomoziMi.service.KontaktiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestKontaktController {

    @Autowired
    private KontaktiService kontaktiService;

    @DeleteMapping("deleteKontakt/{id}")
    public ResponseEntity<?> deleteKontakt(@PathVariable("id") Integer kontaktId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken)
            throw new IllegalArgumentException("Potrebna prijava korisnika za dobivanje liste zahtjeva!");

        Kontakti k = kontaktiService.findByKontaktId(kontaktId);
        kontaktiService.removeKontakt(k);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("kontakt/{id}")
    public ResponseEntity<?> dohvatiKontakt(@PathVariable("id") Integer kontaktId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken)
            throw new IllegalArgumentException("Potrebna prijava korisnika za dobivanje liste zahtjeva!");

        Kontakti k = kontaktiService.findByKontaktId(kontaktId);

        return ResponseEntity.status(HttpStatus.OK).body(k);
    }

}
