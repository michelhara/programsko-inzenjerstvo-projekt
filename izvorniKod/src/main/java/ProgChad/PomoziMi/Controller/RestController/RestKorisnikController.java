package ProgChad.PomoziMi.Controller.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ProgChad.PomoziMi.Controller.ControllerUtils;
import ProgChad.PomoziMi.DTO.KorisnikDTO;
import ProgChad.PomoziMi.DTO.RecenzijaDTO;
import ProgChad.PomoziMi.DTO.JSON.KorisnikDTOJson;
import ProgChad.PomoziMi.DTO.JSON.RecenzijaDTOJson;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Recenzija;
import ProgChad.PomoziMi.domain.RegistrationToken;
import ProgChad.PomoziMi.mailer.EmailSenderService;
import ProgChad.PomoziMi.mapper.KorisnikMapper;
import ProgChad.PomoziMi.service.KorisnikService;
import ProgChad.PomoziMi.service.RecenzijeService;
import ProgChad.PomoziMi.service.RegistrationTokenService;
import ProgChad.PomoziMi.service.validation.Validacija;
import ProgChad.PomoziMi.service.validation.impl.KorisnikValidation;

@RestController
public class RestKorisnikController {

	@Autowired
	private KorisnikService korisnikService;

	@Autowired
	private RegistrationTokenService registrationTokenService;

	@Autowired
	private EmailSenderService emailSenderService;

	@Autowired
	private RecenzijeService recenzijeService;

	@GetMapping("/users")
	public List<KorisnikDTOJson> listaKorisnika() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String currentUserName = authentication.getName();
			Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

			if (!loggedInUser.getUloga().equals("ROLE_ADMIN")) {
				throw new IllegalArgumentException("Nemate dovoljne razine ovlasti za ovu akciju!");
			}

			List<Korisnik> korisnici = korisnikService.listAll();
			List<KorisnikDTOJson> returnKorisnici = new ArrayList<>();
			for (Korisnik k : korisnici) {
				returnKorisnici.add(ControllerUtils.transformToKorisnikDTOJson(k));
			}

			return returnKorisnici;
		} else {
			throw new IllegalArgumentException("Potrebna prijava u sustav!");
		}
	}

	@GetMapping("/blockedUsers")
	public List<KorisnikDTOJson> listaBlokiranihKorisnika() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String currentUserName = authentication.getName();
			Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);

			if (!loggedInUser.getUloga().equals("ROLE_ADMIN")) {
				throw new IllegalArgumentException("Nemate dovoljne razine ovlasti za ovu akciju!");
			}

			List<Korisnik> korisnici = korisnikService.listAllBlocked();
			List<KorisnikDTOJson> returnKorisnici = new ArrayList<>();
			for (Korisnik k : korisnici) {
				returnKorisnici.add(ControllerUtils.transformToKorisnikDTOJson(k));
			}

			return returnKorisnici;
		} else {
			throw new IllegalArgumentException("Potrebna prijava u sustav!");
		}
	}

	@GetMapping("/user")
	public ResponseEntity<KorisnikDTOJson> korisnik(@RequestParam(value = "userId", required = false) Integer id) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if ((authentication instanceof AnonymousAuthenticationToken))
			throw new IllegalArgumentException("Trebate biti prijavljeni za ovu akciju!");

		String currentUserName = authentication.getName();
		Korisnik loggedInUser = korisnikService.findByUsername(currentUserName);
		if (id != null && !loggedInUser.getUloga().equals("ROLE_ADMIN"))
			throw new IllegalArgumentException("Nemate dovoljne razine ovlasti za ovu akciju!");

		Korisnik korisnik;
		if (id == null) {
			korisnik = loggedInUser;
		} else
			korisnik = korisnikService.findByKorisnikID(id);

		return ResponseEntity.ok().body(ControllerUtils.transformToKorisnikDTOJson(korisnik));
	}

	@PostMapping("/register")
	public ResponseEntity<?> registerAccount(@RequestBody KorisnikDTO korisnikDTO) {
		Validacija validacija = new KorisnikValidation();
		if (validacija.validiraj(korisnikDTO)) {
			Korisnik korisnik = korisnikService.createKorisnik(KorisnikMapper.modelToDTO(korisnikDTO));
			RegistrationToken regTok = registrationTokenService.createToken(new RegistrationToken(korisnik));
			try {
				emailSenderService.sendRegistrationVerificationEmail(regTok,
						ServletUriComponentsBuilder.fromCurrentRequestUri().build().toUriString());
			} catch (MessagingException | IOException me) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
			return ResponseEntity.status(HttpStatus.CREATED).build();
		} else
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Korisnik već postoji!");
	}

	@GetMapping("baseUrl")
	public String getBaseUrl() {
		return ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
	}

}
