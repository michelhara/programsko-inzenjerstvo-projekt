package ProgChad.PomoziMi.Controller;

import ProgChad.PomoziMi.DTO.JSON.*;
import ProgChad.PomoziMi.domain.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ControllerUtils {

    /**
     * Metoda se koristi kod mapiranja zahtjeva u njihove DATA TRANSFER OBJEKTE zbog manjih podataka koji se u konacnici prenose unutar JSON-a.
     * @param zahtjevi
     * @param trenutniKorisnik
     * @return
     */
    public static List<ZahtjevDTOJson> transformToZahtjevDTOJson (Collection<Zahtjev> zahtjevi, Korisnik trenutniKorisnik) {
        return zahtjevi.stream().map(z -> transformSingleZahtjevToDTOJson(trenutniKorisnik, z)).collect(Collectors.toList());
    }

    public static ZahtjevDTOJson transformSingleZahtjevToDTOJson(Korisnik trenutniKorisnik, Zahtjev z) {
        return new ZahtjevDTOJson(
                z.getZahtjevId(),
                z.getOpisZahtjeva(),
                z.getRok(),
                z.getIzvrsen(),
                z.getLatitude(),
                z.getLongitude(),
                z.getBrojTelefona(),
                z.getKategorija(),
                z.getSakriven(),
                z.getPrikaziPodatke(),
                z.getPrihvacaKorisnikId() == null ? null : z.getPrihvacaKorisnikId().getKorisnikId(),
                z.getPrihvacaKorisnikId() == null ? null : z.getPrihvacaKorisnikId().getUsername(),
                z.getZeliPomoci() != null && z.getZeliPomoci().contains(trenutniKorisnik),
                z.getKorisnikId().getKorisnikId(),
                z.getKorisnikId().getUsername()
        );
    }

    public static Set<NotifikacijaDTOJson> transformToNotifikacijaDTOJson (Collection<Notifikacije> notifikacije) {
        return notifikacije.stream().map(n -> transformToSingleNotifikacijaDTOJson(n)).collect(Collectors.toUnmodifiableSet());
    }

    public static NotifikacijaDTOJson transformToSingleNotifikacijaDTOJson (Notifikacije notifikacija) {
        return new NotifikacijaDTOJson(notifikacija.getNotifikacijaId(),
                notifikacija.getKorisnikId().getKorisnikId(),
                notifikacija.getKorisnikId().getUsername(),
                notifikacija.getSadrzaj(),
                notifikacija.getKorisnikKojiJeNapravioNesto().getKorisnikId(),
                notifikacija.getKorisnikKojiJeNapravioNesto().getUsername(),
                notifikacija.getZahtjevZaPomoc().getZahtjevId(),
                notifikacija.getObavijestOPomoci());
    }

    public static KorisnikDTOJson transformToKorisnikDTOJson (Korisnik trenutniKorisnik) {
        return new KorisnikDTOJson(
                trenutniKorisnik.getKorisnikId(),
                trenutniKorisnik.getIme(),
                trenutniKorisnik.getPrezime(),
                trenutniKorisnik.getEmail(),
                trenutniKorisnik.getUsername(),
                trenutniKorisnik.getUloga(),
                transformToRecenzijeDTOJson(trenutniKorisnik.getRecenzije()),
                transformToKontaktiDTOJson(trenutniKorisnik.getKontakti()),
                transformToNotifikacijaDTOJson(trenutniKorisnik.getNotifikacije()));
    }

    public static Set<KontaktiDTOJson> transformToKontaktiDTOJson (Collection<Kontakti> kontakti) {
        return kontakti.stream().map(k -> transformSingleKontaktToDTOJson(k)).collect(Collectors.toUnmodifiableSet());
    }

    public static KontaktiDTOJson transformSingleKontaktToDTOJson(Kontakti kontakt) {
        return new KontaktiDTOJson(
                kontakt.getKontaktId(),
                kontakt.getBrojTelefona());
    }
    
    public static RecenzijaDTOJson transformSingleRecenzijaToDTOJson(Recenzija recenzija) {
        return new RecenzijaDTOJson(
        		recenzija.getRecenziraniKorisnik().getKorisnikId(),
        		recenzija.getRecenziraniKorisnik().getUsername(),
        		recenzija.getRecenziraKorisnik().getKorisnikId(),
        		recenzija.getRecenziraKorisnik().getUsername(),
        		recenzija.getOcjena(),
        		recenzija.getKomentar());
    }
    
    public static Set<RecenzijaDTOJson> transformToRecenzijeDTOJson (Collection<Recenzija> recenzije) {
        return recenzije.stream().map(r -> transformSingleRecenzijaToDTOJson(r)).collect(Collectors.toUnmodifiableSet());
    }

    public static List<KorisnikDTOJson> transformToMnogiKorisniciDTOJson(List<Korisnik> korisnici) {
        return korisnici.stream().map(k -> transformToKorisnikDTOJson(k)).collect(Collectors.toList());
    }
}
