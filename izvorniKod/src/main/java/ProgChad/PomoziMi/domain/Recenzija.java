package ProgChad.PomoziMi.domain;

import java.util.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(RecenzijaId.class)
public class Recenzija {

	@Id
	@ManyToOne
	@JoinColumn(name = "korisnikId")
	private Korisnik recenziraniKorisnik;
	
	@Id
	@ManyToOne
	private Korisnik recenziraKorisnik;
	
	//mozda ce trebati dodati ogranicenja za iznos ocjene
	@NotNull
	private Integer ocjena;
	
	@NotNull
	private String komentar;

	public Recenzija() {
		//prazni konstruktor
	}
	
	public Recenzija(Korisnik recenziraniKorisnik, Korisnik recenziraKorisnik, @NotNull Integer ocjena,
			@NotNull String komentar) {
		this.recenziraniKorisnik = recenziraniKorisnik;
		this.recenziraKorisnik = recenziraKorisnik;
		this.ocjena = ocjena;
		this.komentar = komentar;
	}

	public Integer getOcjena() {
		return ocjena;
	}

	public void setOcjena(Integer ocjena) {
		this.ocjena = ocjena;
	}

	public String getKomentar() {
		return komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public Korisnik getRecenziraniKorisnik() {
		return recenziraniKorisnik;
	}
	
	public void setRecenziraniKorisnik(Korisnik recenziraniKorisnik) {
		this.recenziraniKorisnik = recenziraniKorisnik;
	}

	public Korisnik getRecenziraKorisnik() {
		return recenziraKorisnik;
	}
	
	public void setRecenziraKorisnik(Korisnik recenziraKorisnik) {
		this.recenziraKorisnik = recenziraKorisnik;
	}
	
	@Override
	public String toString() {
		return "Korisnik " + this.recenziraKorisnik.getUsername() + " daje ocjenu " + this.ocjena + " korisniku " 
				+ this.recenziraniKorisnik.getUsername() + " uz komentar: " + this.komentar;
	}

	@Override
	public int hashCode() {
		return Objects.hash(recenziraniKorisnik, recenziraKorisnik);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Recenzija recenzija = (Recenzija) obj;
		return Objects.equals(recenziraniKorisnik, recenzija.recenziraniKorisnik) &&
				Objects.equals(recenziraKorisnik, recenzija.recenziraKorisnik);
	}
	
	
	
}
