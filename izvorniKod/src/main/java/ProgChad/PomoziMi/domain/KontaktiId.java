package ProgChad.PomoziMi.domain;

import java.io.Serializable;
import java.util.Objects;

public class KontaktiId implements Serializable {

	private static final long serialVersionUID = -1896338996477024134L;
	
	private Integer kontaktId;
	
	private Integer korisnikId;
	
	public KontaktiId() {
		//defaultni konstrutkor
	}
	
	public KontaktiId(Integer kontaktId, Integer korisnikId) {
		this.kontaktId = kontaktId;
		this.korisnikId = korisnikId;
	}
	
	@Override
	public boolean equals(Object obj) {
		//ako objekt usporedujemo sa samim sobom vraca true
		if (obj == this) {
			return true;
		}
		
		if (!(obj instanceof KontaktiId)) {
			return false;
		}
		
		KontaktiId kid = (KontaktiId)obj;
		
		return (this.kontaktId == kid.kontaktId) &&
				this.korisnikId == kid.korisnikId;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.kontaktId, this.korisnikId);
	}

}
