package ProgChad.PomoziMi.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.util.Objects;
import java.util.Set;

@Entity
public class Korisnik {

	@Id
	@GeneratedValue
	private Integer korisnikId;
	
	@NotNull
	private String ime;

	@NotNull
	private String prezime;
	
	@NotNull
	private String uloga;
	
	@Column(unique=true)
	@NotNull
	private String email;
	
	@Column(unique=true)
	@NotNull
	private String username;
	
	@Column(unique=true)
	@NotNull
	private String password;
	
	@NotNull
	private Integer enabled;

	/**
	 * Predstavlja skup svih recenzija.
	 */
	@OneToMany(mappedBy = "recenziraniKorisnik")
	private Set<Recenzija> recenzije;

	/**
	 * Predstavlja skup svih kontakata.
	 */
	@OneToMany(mappedBy = "korisnikId")
	private Set<Kontakti> kontakti;

	/**
	 * Predstavlja skup svih notifikacija.
	 */
	@OneToMany(mappedBy = "korisnikId")
	private Set<Notifikacije> notifikacije;

	/**
	 * Predstavlja skup svih zahtjeva.
	 */
	@OneToMany(mappedBy = "korisnikId")
	private Set<Zahtjev> zahtjevi;
	
	@OneToMany(mappedBy = "korisnikId")
	private Set<Zahtjev> sakriveniZahtjevi;
	
	public Korisnik() {
		//defaultni konstruktor
	}
	
	public Korisnik(@NotNull String ime, @NotNull String prezime, @NotNull String uloga, @NotNull String email,
			@NotNull String username, @NotNull String password, @NotNull Integer enabled) {
		this.ime = ime;
		this.prezime = prezime;
		this.uloga = uloga;
		this.email = email;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}

	public Integer getKorisnikId() {
		return korisnikId;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getUloga() {
		return uloga;
	}

	public void setUloga(String uloga) {
		this.uloga = uloga;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}
	
	public void addSakriveniZahtjevi(Zahtjev zahtjev) {
		this.sakriveniZahtjevi.add(zahtjev);
	}

	public void addRecenzije(Recenzija recenzija) {
		this.recenzije.add(recenzija);
	}
	
	public void setSakriveniZahtjevi(Set<Zahtjev> sakriveniZahtjevi) {
		this.sakriveniZahtjevi = sakriveniZahtjevi;
	}
	
	public Set<Zahtjev> getSakriveniZahtjevi(){
		return this.sakriveniZahtjevi;
	}

	public Set<Recenzija> getRecenzije() {
		return recenzije;
	}

	public Set<Kontakti> getKontakti() {
		return kontakti;
	}

	public Set<Notifikacije> getNotifikacije() {
		return notifikacije;
	}

	public Set<Zahtjev> getZahtjevi() {
		return zahtjevi;
	}

	public void setZahtjevi(Set<Zahtjev> zahtjevi) {
		this.zahtjevi = zahtjevi;
	}

	@Override
	public String toString() {
		return "Korisnik: " + this.ime + " " + this.prezime + ", Username: " + this.username;	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Korisnik korisnik = (Korisnik) o;
		return Objects.equals(korisnikId, korisnik.korisnikId) &&
				Objects.equals(username, korisnik.username);
	}

	@Override
	public int hashCode() {
		return Objects.hash(korisnikId, username);
	}
}
