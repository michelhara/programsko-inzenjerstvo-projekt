package ProgChad.PomoziMi.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class BlokiraniKorisnik implements Serializable {

	private static final long serialVersionUID = 4358568961043080154L;

	@Id
	private Integer korisnikId;
	
	@OneToOne
    @JoinColumn(name = "korisnikId")
    @MapsId
    private Korisnik korisnik;
	
	@NotNull
	private Date datumDo;
	
	private Integer blokiraoAdmin;
	
	public BlokiraniKorisnik() {
		
	}

	public BlokiraniKorisnik(Korisnik korisnik, @NotNull Date datumDo, Integer blokiraoAdmin) {
		super();
		this.korisnik = korisnik;
		this.datumDo = datumDo;
		this.blokiraoAdmin = blokiraoAdmin;
	}

	public Date getDatumDo() {
		return datumDo;
	}

	public void setDatumDo(Date datumDo) {
		this.datumDo = datumDo;
	}

	public Integer getBlokiraoAdmin() {
		return blokiraoAdmin;
	}

	public void setBlokiraoAdmin(Integer blokiraoAdmin) {
		this.blokiraoAdmin = blokiraoAdmin;
	}
	
	public Integer getBlokiraniKorisnikId() {
		return korisnikId;
	}
	
	public void setBlokiraniKorisnikId(Integer korisnikId) {
		this.korisnikId = korisnikId;
	}

	public Korisnik getBlokiraniKorisnik() {
		return korisnik;
	}

	public void setBlokiraniKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	
}
