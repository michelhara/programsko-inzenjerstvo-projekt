package ProgChad.PomoziMi.domain;

import java.io.Serializable;
import java.util.Objects;

public class RecenzijaId implements Serializable {

	private static final long serialVersionUID = 6921777396956076864L;

	private Integer recenziraniKorisnik;
	
	private Integer recenziraKorisnik;
	
	public RecenzijaId() {
		//defautlni konstruktor
	}
	
	public RecenzijaId(Integer recenziraniKorisnik, Integer recenziraKorisnik) {
		this.recenziraniKorisnik = recenziraniKorisnik;
		this.recenziraKorisnik = recenziraKorisnik;
	}
	
	@Override
	public boolean equals(Object obj) {
		//ako objekt usporedujemo sa samim sobom vraca true
		if (obj == this) {
			return true;
		}
		
		if (!(obj instanceof RecenzijaId)) {
			return false;
		}
		
		RecenzijaId rid = (RecenzijaId)obj;
		
		return (this.recenziraniKorisnik == rid.recenziraniKorisnik) &&
				(this.recenziraKorisnik == rid.recenziraKorisnik);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.recenziraniKorisnik, this.recenziraKorisnik);
	}

}
