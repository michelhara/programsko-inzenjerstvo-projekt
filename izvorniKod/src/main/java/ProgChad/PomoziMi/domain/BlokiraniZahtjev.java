package ProgChad.PomoziMi.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(BlokiraniZahtjevId.class)
public class BlokiraniZahtjev {
	
	@Id
	@OneToOne
	private Zahtjev zahtjevId;	
		
	@NotNull
	private Date datumDo; 
	
	@NotNull
	private Integer blokiraoAdmin;
	 
	public BlokiraniZahtjev() {
		
	}

	public BlokiraniZahtjev(Zahtjev zahtjevId, @NotNull Date datumDo, @NotNull Integer blokiraoAdmin) {
		super();
		this.zahtjevId = zahtjevId;
		this.datumDo = datumDo;
		this.blokiraoAdmin = blokiraoAdmin;
	}

	public Zahtjev getZahtjevId() {
		return zahtjevId;
	}

	public void setZahtjevId(Zahtjev zahtjevId) {
		this.zahtjevId = zahtjevId;
	}

	public Date getDatumDo() {
		return datumDo;
	}

	public void setDatumDo(Date datumDo) {
		this.datumDo = datumDo;
	}

	public Integer getBlokiraoAdmin() {
		return blokiraoAdmin;
	}

	public void setBlokiraoAdmin(Integer blokiraoAdmin) {
		this.blokiraoAdmin = blokiraoAdmin;
	}
	
}
