package ProgChad.PomoziMi.domain;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Entitet koji služi za registraciju korisnika.
 */
@Entity
public class RegistrationToken {

    /**
     * Signalizira tokenov unikantni id.
     */
    @Id
    @GeneratedValue
    private long tokenId;

    /**
     * Token povezan uz potvrdu registracije.
     */
    private String token;

    /**
     * Vrijeme nastanka tokena.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    /**
     * Vrijeme validonsti tokena.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date untilDate;

    /**
     * Korisnik koji se registrira.
     */
    @OneToOne(targetEntity = Korisnik.class)
    @JoinColumn(nullable = false, name = "korisnikId")
    private Korisnik korisnik;

    public RegistrationToken() {
    }

    public RegistrationToken(Korisnik korisnik) {
        this.korisnik = korisnik;
        createdDate = new Date();
        untilDate= addDay(createdDate.toString());
        token = UUID.randomUUID().toString();
    }

    private Date addDay(String startingDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(simpleDateFormat.parse(startingDate));
        } catch (ParseException e) {
            c.setTime(new Date());
        }
        c.add(Calendar.DATE, 2);
        return c.getTime();
    }

    public void setTokenId(long tokenId) {
        this.tokenId = tokenId;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setUntilDate(Date untilDate) {
        this.untilDate = untilDate;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public long getTokenId() {
        return tokenId;
    }

    public String getToken() {
        return token;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public Date getUntilDate() {
        return untilDate;
    }
}
