package ProgChad.PomoziMi.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(NotifikacijeId.class)
public class Notifikacije {

	@Id
	@GeneratedValue
	private Integer notifikacijaId;

	@Id
	@ManyToOne
	private Korisnik korisnikId; // korisnik ciji je zahtjev

	@NotNull
	private String sadrzaj;

	@OneToOne
	private Korisnik korisnikKojiJeNapravioNesto; // korisnik koji ce prihvatit zahtjev, tj korisnik kojem ce biti odobreno prihvaćanje

	@OneToOne
	private Zahtjev zahtjevZaPomoc;

	private boolean obavijestOPomoci;

	public Notifikacije() {
		//prazni konstruktor za JPA
	}

	public Notifikacije(Korisnik korisnikId) {
		this.korisnikId = korisnikId;
	}

	public Integer getNotifikacijaId() {
		return notifikacijaId;
	}

	public void setNotifikacijaId(Integer notifikacijaId) {
		this.notifikacijaId = notifikacijaId;
	}

	public String getSadrzaj() {
		return sadrzaj;
	}

	public void setSadrzaj(String sadrzaj) {
		this.sadrzaj = sadrzaj;
	}

	public Korisnik getKorisnikId() {
		return korisnikId;
	}

	@Override
	public String toString() {
		return "Notifikacija za korisnika " + this.korisnikId.getUsername()
				+ ": " + this.sadrzaj;
	}

	public Korisnik getKorisnikKojiJeNapravioNesto() {
		return korisnikKojiJeNapravioNesto;
	}

	public void setKorisnikKojiJeNapravioNesto(Korisnik korisnikKojiJeNapravioNesto) {
		this.korisnikKojiJeNapravioNesto = korisnikKojiJeNapravioNesto;
	}

	public void setKorisnikId(Korisnik korisnikId) {
		this.korisnikId = korisnikId;
	}

	public void setZahtjevZaPomoc(Zahtjev zahtjevZaPomoc) {
		this.zahtjevZaPomoc = zahtjevZaPomoc;
	}

	public Zahtjev getZahtjevZaPomoc() {
		return zahtjevZaPomoc;
	}

	public boolean getObavijestOPomoci() {
		return obavijestOPomoci;
	}

	public void setObavijestOPomoci(boolean obavijestOPomoci) {
		this.obavijestOPomoci = obavijestOPomoci;
	}


}
