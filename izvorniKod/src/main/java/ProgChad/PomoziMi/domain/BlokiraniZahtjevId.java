package ProgChad.PomoziMi.domain;

import java.io.Serializable;
import java.util.Objects;

public class BlokiraniZahtjevId implements Serializable {

	private static final long serialVersionUID = 510352492890594996L;
	
	private Zahtjev zahtjevId;
	
	public BlokiraniZahtjevId() {
		
	}
	
	public BlokiraniZahtjevId(Zahtjev zahtjevId) {
		this.zahtjevId = zahtjevId;
	}
	
	@Override
	public boolean equals(Object obj) {
		//ako objekt usporedujemo sa samim sobom vraca true
		if (obj == this) {
			return true;
		}
		
		if (!(obj instanceof BlokiraniZahtjevId)) {
			return false;
		}
		
		BlokiraniZahtjevId bzid = (BlokiraniZahtjevId)obj;
		
		return (this.zahtjevId == bzid.zahtjevId);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.zahtjevId);
	}
	
}
