package ProgChad.PomoziMi.domain;

import java.io.Serializable;
import java.util.Objects;

public class ZahtjevId implements Serializable {

	private static final long serialVersionUID = -9133632703504829594L;
	
	private Integer korisnikId;
		
	private Integer zahtjevId;
	
	public ZahtjevId() {
		//defaultni konstrutkor
	}
	
	public ZahtjevId(Integer zahtjevId, Integer korisnikId) {
		this.zahtjevId = zahtjevId;
		this.korisnikId = korisnikId;
	}
	
	@Override
	public boolean equals(Object obj) {
		//ako objekt usporedujemo sa samim sobom vraca true
		if (obj == this) {
			return true;
		}
		
		if (!(obj instanceof ZahtjevId)) {
			return false;
		}
		
		ZahtjevId zid = (ZahtjevId)obj;
		
		return (this.zahtjevId == zid.zahtjevId) &&
				this.korisnikId == zid.korisnikId;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.zahtjevId, this.korisnikId);
	}
	

}
