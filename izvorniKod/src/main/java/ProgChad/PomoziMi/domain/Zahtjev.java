package ProgChad.PomoziMi.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

import javax.persistence.*;
import javax.validation.Constraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@IdClass(ZahtjevId.class)
public class Zahtjev implements Serializable {
	
	private static final long serialVersionUID = -2689442796499508106L;

	@Id
	@ManyToOne
	@JoinColumn(name = "korisnikId")
	private Korisnik korisnikId;
	
	@Id
	@GeneratedValue
	private Integer zahtjevId;
	
	@NotNull
	private String opisZahtjeva;
	
	@JsonFormat(pattern="yyyy-MM-dd HH-mm-ss")
	private Timestamp rok;

	@JsonFormat(pattern="yyyy-MM-dd HH-mm-ss")
	private Timestamp datumNastanka;
	
	private Boolean izvrsen;
	
	private Double latitude;

	private Double longitude;
	
	@Pattern(regexp="[0-9]+")
	private String brojTelefona;
	
	@NotNull
	private String kategorija;
	
	@NotNull
	private Boolean sakriven;

	private Boolean prikaziPodatke;
	
	@ManyToOne
	private Korisnik prihvacaKorisnikId;

	@ManyToMany
	private Set<Korisnik> zeli_pomoci;

	@ManyToMany
	private Set<Korisnik> sakriveni_zahtjev;
	
	public Zahtjev() {
		//defaultni konstruktor
	}
 	
	public Zahtjev(Korisnik korisnikId, @NotNull String opisZahtjeva, Timestamp rok, Boolean izvrsen, Double latitude, Double longitude,
				   String brojTelefona, @NotNull String kategorija, @NotNull Boolean sakriven, @NotNull Boolean prikaziPodatke,
				   Korisnik prihvacaKorisnikId) {
		this.korisnikId = korisnikId;
		this.opisZahtjeva = opisZahtjeva;
		this.rok = rok;
		this.izvrsen = izvrsen;
		this.latitude = latitude;
		this.longitude = longitude;
		this.brojTelefona = brojTelefona;
		this.kategorija = kategorija;
		this.sakriven = sakriven;
		this.prikaziPodatke = prikaziPodatke;
		this.prihvacaKorisnikId = prihvacaKorisnikId;
		this.datumNastanka = new Timestamp(System.currentTimeMillis());
	}

	public Korisnik getKorisnikId() {
		return korisnikId;
	}

	public void setKorisnikId(Korisnik korisnikId) {
		this.korisnikId = korisnikId;
	}

	public String getOpisZahtjeva() {
		return opisZahtjeva;
	}

	public void setOpisZahtjeva(String opisZahtjeva) {
		this.opisZahtjeva = opisZahtjeva;
	}

	public Timestamp getRok() {
		return rok;
	}

	public void setRok(Timestamp rok) {
		this.rok = rok;
	}

	public Boolean getIzvrsen() {
		return izvrsen;
	}

	public void setIzvrsen(Boolean izvrsen) {
		this.izvrsen = izvrsen;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
	
	public String getKategorija() {
		return kategorija;
	}

	public void setKategorija(String kategorija) {
		this.kategorija = kategorija;
	}

	public Boolean getSakriven() {
		return sakriven;
	}

	public void setSakriven(Boolean sakriven) {
		this.sakriven = sakriven;
	}

	public Boolean getPrikaziPodatke() {
		return prikaziPodatke;
	}

	public void setPrikaziPodatke(Boolean prikaziPodatke) {
		this.prikaziPodatke = prikaziPodatke;
	}

	public Korisnik getPrihvacaKorisnikId() {
		return prihvacaKorisnikId;
	}

	public void setPrihvacaKorisnikId(Korisnik prihvacaKorisnikId) {
		this.prihvacaKorisnikId = prihvacaKorisnikId;
	}

	public Integer getZahtjevId() {
		return zahtjevId;
	}
	
	public void setZahtjevId(Integer zahtjevId) {
		this.zahtjevId = zahtjevId;
	}
	
	public Set<Korisnik> getZeliPomoci(){
		return this.zeli_pomoci;
	}
	
	public void setZeliPomoci(Set<Korisnik> zeli_pomoci) {
		this.zeli_pomoci = zeli_pomoci;
	}
	
	public void addZeliPomoci(Korisnik korisnik) {
		this.zeli_pomoci.add(korisnik);
	}
	
	public void removeZeliPomoci(Korisnik korisnik) {
		this.zeli_pomoci.remove(korisnik);
	}
	
	public void addSakriveniZahtjev(Korisnik korisnik) {
		this.sakriveni_zahtjev.add(korisnik);
	}

	public void removeSakriveniZahtjev(Korisnik korisnik) {
		this.sakriveni_zahtjev.remove(korisnik);
	}
	
	public Set<Korisnik> getSakriveniZahtjev() {
		return this.sakriveni_zahtjev;
	}
	
	public void setSakriveniZahtjev(Set<Korisnik> sakriveni_zahtjev) {
		this.sakriveni_zahtjev = sakriveni_zahtjev;
	}

	public Timestamp getDatumNastanka() {
		return datumNastanka;
	}

	public void setDatumNastanka(Timestamp datumNastanka) {
		this.datumNastanka = datumNastanka;
	}
	
	@Override
	public String toString() {
		return "Zahtjev [korisnikId=" + korisnikId + ", zahtjevId=" + zahtjevId + ", sakriven=" + sakriven + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(zahtjevId, korisnikId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Zahtjev zahtjev = (Zahtjev) obj;
		return Objects.equals(korisnikId, zahtjev.korisnikId) &&
				Objects.equals(zahtjevId, zahtjev.zahtjevId);
	}
}
