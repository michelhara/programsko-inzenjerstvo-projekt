package ProgChad.PomoziMi.domain;

import java.util.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@IdClass(KontaktiId.class)
public class Kontakti {

	@Id
	@GeneratedValue
	private Integer kontaktId;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "korisnikId")
	private Korisnik korisnikId;
	
	@NotNull
	@Pattern(regexp="[0-9]+")
	@Column(unique = true)
	private String brojTelefona;

	public Kontakti() {
		//prazni konstruktor za JPA
	}

	public Kontakti(Korisnik korisnikId, @NotNull @Pattern(regexp = "[0-9]+") String brojTelefona) {
		this.korisnikId = korisnikId;
		this.brojTelefona = brojTelefona;
	}

	public Integer getKontaktId() {
		return kontaktId;
	}

	public void setKontaktId(Integer kontaktId) {
		this.kontaktId = kontaktId;
	}

	public Korisnik getKorisnikID() {
		return korisnikId;
	}

	public void setKorisnikID(Korisnik korisnikId) {
		this.korisnikId = korisnikId;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
	
	@Override
	public String toString() {
		return "Korisnik " + this.korisnikId.getIme() + " " + this.korisnikId.getPrezime()
				+ ", broj telefona: " + this.brojTelefona;
	}

	@Override
	public int hashCode() {
		return Objects.hash(kontaktId, korisnikId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Kontakti kontakt = (Kontakti) obj;
		return Objects.equals(korisnikId, kontakt.korisnikId) &&
				Objects.equals(kontaktId, kontakt.kontaktId);
	}
	
	
	
}
