package ProgChad.PomoziMi.domain;

import java.io.Serializable;
import java.util.Objects;

public class NotifikacijeId implements Serializable {

	private static final long serialVersionUID = -1441607789868839389L;
	
	private Integer notifikacijaId;

	private Integer korisnikId;
	
	public NotifikacijeId() {
		//defaultni konstrutkor
	}
	
	public NotifikacijeId(Integer notifikacijaId, Integer korisnikId) {
		this.notifikacijaId = notifikacijaId;
		this.korisnikId = korisnikId;
	}
	
	@Override
	public boolean equals(Object obj) {
		//ako objekt usporedujemo sa samim sobom vraca true
		if (obj == this) {
			return true;
		}
		
		if (!(obj instanceof NotifikacijeId)) {
			return false;
		}
		
		NotifikacijeId nid = (NotifikacijeId)obj;
		
		return (this.notifikacijaId == nid.notifikacijaId) &&
				(this.korisnikId == nid.korisnikId);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.notifikacijaId, this.korisnikId);
	}

}
