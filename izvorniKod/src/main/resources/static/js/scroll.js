function collapseMe() {
    if (window.screen.width < 900) {
        var registrationForm = document.getElementById("registrationForm");
        var collapsible = document.getElementById("collapsible");
        if (checkRegistrationDisplay(registrationForm) && checkRegistrationDisplay(collapsible))
            $("#registrationForm").collapse("hide");
    }
}

function checkRegistrationDisplay(e) {
    return window.getComputedStyle(e).display !== "none"
}

function scrollToTop() {
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

async function validirajRegistriraj() {

    console.log("TU SAM!");

    //debugger;

    let formEl = document.getElementById("registracija");
    let form = {};

    for (var i = 0; i < formEl.elements.length - 1; ++i) {
        form[formEl[i].name] = formEl[i].value;
    }

    let link = document.getElementById("registracija").action + "/";
    let toast = document.querySelector(".toast");
    toast.setAttribute("data-autohide", "false");
    $(".toast").toast('show');
    toast = document.querySelector(".toast-body");
    let response;

    try {
        response = await axios.post(link, form);
        toast.innerHTML  = "Račun napravljen! Molimo Vas, provjerite svoj e-mail račun da ga potvrdite!"
        $("#registrationForm").collapse("hide");
    } catch (reject) {
        toast.innerHTML  = reject.response.data.message;
        return;
    }
}

$(document).ready(function () {
    if(window.location.search) {
        let toast = document.querySelector(".toast");
        toast.setAttribute("data-autohide", "false");
        $(".toast").toast('show');
    }
});