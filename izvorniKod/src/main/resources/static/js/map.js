let mymap;
let marker;
let lat;
let long;

$(document).ready(function () {
    $("#mapOptions").addClass("mapOptions-disabled");
    postaviOdaberivostMape();
    definirajMapu();
});

definirajMapu = function () {
    mymap = L.map('mapid').setView([45.815399, 15.966568], 13);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoicG9tb3ppbWkiLCJhIjoiY2tqY3g5OWQxODJjbjJ5cWpobWp4a3BqOSJ9.ApU872jbAFvYrUK9oBsISQ'
    }).addTo(mymap);

    mymap.on("click", onMapClick);
}

function onMapClick(e) {
    lat = e.latlng.lat;
    long = e.latlng.lng;
    clearAndAddMarker();
}

function setLatLong(position) {
    lat = position.coords.latitude;
    long = position.coords.longitude;
    clearAndAddMarker();
}

function autoLocation() {
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(setLatLong);
    } else {
        alert("Geolokacija nije podržana!");
    }
}

function clearAndAddMarker() {
    if(marker)
        mymap.removeLayer(marker);
    marker = L.marker([lat, long]).addTo(mymap);
    mymap.setView([lat, long], 13);
    postaviVrijednostiForme();
}

function postaviVrijednostiForme() {
    $('input[name=latitude]').val(lat);
    $('input[name=longitude]').val(long);
}

postaviOdaberivostMape = function () {
    $('input[type=radio][name=adresa]').change(function () {
        if (this.value == "false") {
            $("#mapOptions").addClass("mapOptions-disabled");
            lat = null;
            long = null;
            if(marker)
                mymap.removeLayer(marker);
            postaviVrijednostiForme();
        }
        else {
            $("#mapOptions").removeClass("mapOptions-disabled");
        }
    });
}

