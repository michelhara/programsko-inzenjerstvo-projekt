$(document).ready(function () {    
    sessionStorage.setItem("prikaz", "opceniti")
    
    $('.blokirani-korisnici-button').on('click', { prikaz: "blokirani" }, showBlockedUsers);
    $('.block-Korisnik').on('click', blokirajKorisnika);
    $('.unblock-Korisnik').on('click', odblokirajKorisnika);
});

async function showBlockedUsers() {
	let serverUrl = sessionStorage.getItem("baseUrl");

	try {
        let result = await axios({ url: `/blockedUsers/`, baseURL: serverUrl, method: 'get' });
        if (result.status != 200) {
            showToastMessage("Korisnici neuspješno dohvaćeni...");
        }
        
        var korisnici = result.data;

    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
        
    let spremnikKorisnika = $("#spremnik-Korisnika");
    spremnikKorisnika.empty();
    
    korisnici.forEach(function(k, index, array){
    
	    let template = document.querySelector('#korisnikTemplate').content.cloneNode(true);
	    
	    let korisnickiLink = template.querySelector('.blokirani-ime-link');
    	korisnickiLink.href = `/user/${k.username}`;
		    
	    let userContainer = template.querySelector(".korisnik");
	    userContainer.id = `korisnik-${k.korisnikId}`;
	    let userSpan = template.querySelector('.korisnikSpan');
	    userSpan.innerHTML = k.username;
	    let button = template.querySelector('.unblock-Korisnik');
	    $(button).data(`user-id`, k.korisnikId);
	    $(button).on('click', odblokirajKorisnika);
	    
	    $("#spremnik-Korisnika").append(template);
	    
        spremnikKorisnika.prepend(template);
    });
	
}

async function blokirajKorisnika() {
	let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Blokirati ovog korisnika?")) return;
    var korisnikId = $(this).data("user-id"); //trebam dobiti id iz gumba
    let result = await axios({ url: `/blockUser/${korisnikId}/`, baseURL: serverUrl, method: 'put' });
    if (result.status == 200) {
        $(`#korisnik-${korisnikId}`).remove();
        showToastMessage("Korisnik blokiran!")
    } else {
        showToastMessage("Korisnik neuspješno blokiran!");
    }
    var segment_str = window.location.pathname;
	var segment_array = segment_str.split( '/' );
	var last_segment = segment_array.pop();	
	console.log(last_segment);
	if (last_segment != "home") {
		location.reload(true);
	}
}

async function odblokirajKorisnika() {
	let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Odlokirati ovog korisnika?")) return;
    var korisnikId = $(this).data("user-id");
    let result = await axios({ url: `/unblockUser/${korisnikId}/`, baseURL: serverUrl, method: 'put' });
    if (result.status == 200) {
        $(`#korisnik-${korisnikId}`).remove();
        showToastMessage("Korisnik odblokiran!")
    } else {
        showToastMessage("Korisnik neuspješno odblokiran!");
    }
    var segment_str = window.location.pathname;
	var segment_array = segment_str.split( '/' );
	var last_segment = segment_array.pop();	
	console.log(last_segment);
	if (last_segment != "home") {
		location.reload(true);
	}
}