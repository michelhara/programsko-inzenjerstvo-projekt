$(document).ready(function () {
    sessionStorage.setItem("prikaz", "opceniti")
    closeForm();

    $('.delete-Kontakt').on('click', obrisiKontakt);
    $('.delete-Zahtjev').on('click', obrisiZahtjev);
    $('.block-Zahtjev').on('click', blokirajZahtjev);
    $('.unblock-Zahtjev').on('click', odblokirajZahtjev);
    $('.moji-zahtjevi-button').on('click', { url: "/zahtjeviKorisnika/?userId=", prikaz: "korisnicki" }, dohvatiZahtjeve);
    $('.pretrazi-button').on('click', { url: "/zahtjevi", prikaz: "opceniti", url_parametri: pripremiFilterUrl }, dohvatiZahtjeve);
    $('.ispunjeni-zahtjevi-button').on('click', { url: "/zahtjeviIzvrseni", prikaz: "izvrseni" }, dohvatiZahtjeve);
    $('.blokirani-zahtjevi-button').on('click', { url: "/zahtjeviBlokirani", prikaz: "blokirani" }, dohvatiZahtjeve);
    $('.wanna-Help').on('click', pomogniZahtjevu);
    $('.dont-wanna-Help').on('click', odustaniOdZahtjeva);

    $('.accept-Help').on('click', prihvatiKorisnika);
    $('.delete-Notification').on('click', izbrisiNotifikaciju);

    provjeriNotifikacijskiGumb();

    $('.hide-Request').on('click', sakrijZahtjev);
    $('.edit-Request').on('click', otvoriFormu);
})

//PROVJERA JE LI BROJ NOTIFIKACIJA JEDNAK 0
function provjeriNotifikacijskiGumb() {
    let notifikacijskiGumb = $(".notifikacije-gumb");
    let brojNotifikacija = $(".broj-notifikacija").text();
    if (brojNotifikacija == "0") {
        notifikacijskiGumb.prop("disabled", true)
    }
}

var varijabla;
var uredivaniZahtjev; //bitno kod uređivanja zahtjeva, dohvaca zahtjev kakav je prije uređivanja da se prikazu podaci unutar forme
var stariZahtjev; //objekt koji je stari zahtjev

//FUNCKIONALNOSTI POVEZANE UZ FORMU
function openForm() {
    document.getElementById("requestForm").style.display = "block";
}

function closeForm() {
    document.getElementById("requestForm").style.display = "none";
    document.getElementById("zahtjeviForma").reset();
    $("#mapOptions").addClass("mapOptions-disabled");
}

async function napraviNoviZahtjev() {

    console.log("TU SAM!");

    //debugger;

    let formData = $("#zahtjeviForma").serializeArray();

    let form = {};

    let datum;
    let vrijeme;
    for (var i = 0; i < formData.length; ++i) {
        if (formData[i].name == "datum") {
            datum = formData[i].value;
            continue;
        }
        if (formData[i].name == "vrijeme") {
            vrijeme = formData[i].value;
            continue;
        }
        form[formData[i].name] = formData[i].value;
    }

    if (datum && vrijeme)
        form["rok"] = new Date(datum + "T" + vrijeme).toISOString();
    else if (datum) {
        form["rok"] = new Date(datum + "T" + "00:00:00.000").toISOString();
    } else if (vrijeme) {
        showToastMessage("Ne možete dati vrijeme bez datuma!");
        return;
    } else {
        form["rok"] = null;
    }

    let link = document.getElementById("zahtjeviForma").action + "/";
    let response;
    try {
        response = await axios.post(link, form);
        let noviZahtjev = response.data;
        showToastMessage("Zahtjev napravljen!");
        if (form["spremiKontakt"] == "true") {

            let template = document.querySelector('#kontaktTemplate').content.cloneNode(true);
            let numberContainer = template.querySelector(".kontakt");
            numberContainer.id = `kontakt-${noviZahtjev.kontaktId}`;
            let numberSpan = template.querySelector('.kontaktSpan');
            numberSpan.innerHTML = noviZahtjev.brojTelefona;
            let button = template.querySelector('.delete-Kontakt');
            $(button).data(`number-id`, noviZahtjev.kontaktId);
            $(button).on('click', obrisiKontakt);
            $("#spremnik-Kontakata").append(template);

        }

        closeForm();

        let korisnik = JSON.parse(sessionStorage.getItem("korisnik"));
        let spremnikZahtjeva = $("#spremnik-Zahtjeva");
        provjeriDodajuPaDodaj(response.data, spremnikZahtjeva, korisnik)

        if (response.data.sakriven && sessionStorage.getItem("prikaz") != "korisnicki")
            showToastMessage("Skriveni zahtjevi nisu vidljivi na javnom prikazu zahtjeva.");

    } catch (reject) {
        showToastMessage(reject.response.data.message);
        return;
    }
}

//REST FUNKCIONALNOSTI 

//REST FUNKCIONALNOSTI KONTAKTA
async function obrisiKontakt() {
    if (!confirm("Obriši ovaj kontakt?")) return;
    var kontaktId = $(this).data("number-id");

    try {
        let result = await axios.delete(`deleteKontakt/${kontaktId}/`);
        if (result.status == 200) {
            $(`#kontakt-${kontaktId}`).remove();
            showToastMessage("Kontakt obrisan!")
        } else {
            showToastMessage("Brisanje kontakta neuspješno!");
        }
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
}

function showHideRange() {
    var chkYes = document.getElementById("sLokacijom");
    var dvtext = document.getElementById("range");
    var dvlocation = document.getElementById("distance");
    dvtext.style.display = chkYes.checked ? "block" : "none";
    dvlocation.style.display = chkYes.checked ? "block" : "none";
    if (!chkYes.checked) {
        $("input[name=lat]").val("");
        $("input[name=long]").val("");
        $("input[name=distance]").val("");
    }
}

async function otvoriFormu() {
    if (uredivaniZahtjev) {
        showToastMessage("Dovršite uređivanje prošlog zahtjeva!");
        return;
    }

    let zahtjevId = $(this).data("zahtjev-id");
    stariZahtjev = $(`#zahtjev-${zahtjevId}`);
    let serverUrl = sessionStorage.getItem("baseUrl");

    stariZahtjev.toggle();

    try {
        result = await axios({ url: `zahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'get' })

        if (result.status != 200)
            showToastMessage("Neuspješno dohvaćanje zahtjeva!");

        uredivaniZahtjev = result.data;

        prikaziFormu();
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }

}

//REST FUNKCIONALNOSTI ZAHTJEVA!
async function sakrijZahtjev() {
    let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Želite li sakriti/blokirati ovaj zahtjev?")) return;
    var zahtjevId = $(this).data("zahtjev-id");

    try {
        let result = await axios({ url: `hideZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });

        if (result.status != 200) {
            showToastMessage("Zahtjev nije uspješno sakriven!");
        }
        else {
            showToastMessage("Zahtjev uspješno sakriven!");
            if (sessionStorage.getItem("prikaz") == "korisnicki") {
                $(this).unbind();
                $(this).on('click', otkrijZahtjev);
                $(this).text("Otkrij zahtjev");
            } else {
                $(`#zahtjev-${zahtjevId}`).remove();

            }
        }
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }

}

async function otkrijZahtjev() {
    let serverUrl = sessionStorage.getItem("baseUrl");
    var zahtjevId = $(this).data("zahtjev-id");
    try {
        let result = await axios({ url: `unhideZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });

        if (result.status != 200) {
            showToastMessage("Zahtjev nije uspješno otkriven...");
        }
        else {
            showToastMessage("Zahtjev je uspješno otrkiven!");
            if (sessionStorage.getItem("prikaz") == "korisnicki") {
                $(this).unbind();
                $(this).on('click', sakrijZahtjev);
                $(this).text("Sakrij");
            }
        }
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }

}

//ODUSTAJANJE I POMAGANJE ZAHTJEVU
async function odustaniOdZahtjeva() {
    let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Želite li odustati od ovog zahtjeva?")) return;
    var zahtjevId = $(this).data("zahtjev-id");

    try {
        let result = await axios({ url: `noHelpZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });

        if (result.status != 200) {
            showToastMessage("Pomoć neuspješno povućena!");
        }
        else {
            $(this).unbind();
            $(this).text("Želim pomoći");
            $(this).on('click', pomogniZahtjevu);
            showToastMessage("Pomoć uspješno povućena!");
        }
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
}

async function pomogniZahtjevu() {
    let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Želite li pomoći ovom zahtjevu?")) return;
    var zahtjevId = $(this).data("zahtjev-id");

    try {
        let result = await axios({ url: `helpZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });

        if (result.status != 200) {
            showToastMessage("Pomoć neuspješno poslana!");
        }
        else {
            $(this).unbind();
            $(this).text("Ne želim pomoći");
            $(this).on('click', odustaniOdZahtjeva);
            showToastMessage("Pomoć uspješno poslana!");
        }
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
}

async function spremiZahtjev() {
    let formData = $(".zahtjev-edit").serializeArray();

    let form = {};

    let datum;
    let vrijeme;
    for (var i = 0; i < formData.length; ++i) {
        if (formData[i].name == "datum-edit") {
            datum = formData[i].value;
            continue;
        }
        if (formData[i].name == "vrijeme-edit") {
            vrijeme = formData[i].value;
            continue;
        }
        let index = formData[i].name.indexOf("-");
        imePolja = formData[i].name.slice(0, index);
        form[imePolja] = formData[i].value;
    }

    if (datum && vrijeme)
        form["rok"] = new Date(datum + "T" + vrijeme).toISOString();
    else if (datum) {
        form["rok"] = new Date(datum + "T" + "00:00:00.000").toISOString();
    } else if (vrijeme) {
        showToastMessage("Ne možete dati vrijeme bez datuma!");
        return;
    } else {
        form["rok"] = null;
    }

    let serverUrl = sessionStorage.getItem("baseUrl");

    let zahtjevId = $(this).data("zahtjev-id");

    try {
        let result = await axios({ url: `updateZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put', data: form })

        if (result.status != 200) {
            showToastMessage("Neuspješno ažuriranje zahtjeva!");
        }

        let noviZahtjev = result.data;

        $("#spremnik-zahtjev-edit").remove();

        if (provjera(noviZahtjev))
            stariZahtjev.replaceWith(napraviZahtjev(noviZahtjev, JSON.parse(sessionStorage.getItem("korisnik"))));
        else {
            stariZahtjev.remove()
            showToastMessage("Zahtjev kojem je promijenjena vidljivost na skriven ili je izvršen nije vidljiv na svim zahtjevima.");
        }

        stariZahtjev = undefined;
        uredivaniZahtjev = undefined;
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }

}

async function obrisiZahtjev() {
    if (!confirm("Obriši ovaj zahtjev?")) return;
    var zahtjevId = $(this).data("zahtjev-id");
    let result = await axios.delete(`/deleteZahtjev/${zahtjevId}/`);
    if (result.status == 200) {
        $(`#zahtjev-${zahtjevId}`).remove();
        showToastMessage("Zahtjev obrisan!")
    } else {
        showToastMessage("Brisanje zahtjeva neuspješno!");
    }
}

//BLOKIRANJE ZAHTJEVA
async function blokirajZahtjev() {
    let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Blokirati ovaj zahtjev?")) return;
    var zahtjevId = $(this).data("zahtjev-id");
    let result = await axios({ url: `/blockZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });
    if (result.status == 200) {
        $(`#zahtjev-${zahtjevId}`).remove();
        showToastMessage("Zahtjev blokiran!")
    } else {
        showToastMessage("Blokiranje zahtjeva neuspješno!");
    }
}

async function odblokirajZahtjev() {
    let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Odlokirati ovaj zahtjev?")) return;
    var zahtjevId = $(this).data("zahtjev-id");
    let result = await axios({ url: `/unblockZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });
    if (result.status == 200) {
        $(`#zahtjev-${zahtjevId}`).remove();
        showToastMessage("Zahtjev odblokiran!")
    } else {
        showToastMessage("Zahtjev neuspješno odblokiran!");
    }
}

//EDIT ZAHTJEVA
function odustaniUredivanje() {

    stariZahtjev.toggle();
    $("#spremnik-zahtjev-edit").remove();

    stariZahtjev = undefined;
    uredivaniZahtjev = undefined;
}

function prikaziFormu() {
    let template = document.querySelector('#zahtjevEditTemplate').content.cloneNode(true);

    let zahtjevEditUsername = template.querySelector(".zahtjev-edit-username");
    zahtjevEditUsername.innerHTML = uredivaniZahtjev.vlasnikZahtjevaUsername;

    let zahtjevEditBroj = template.querySelector('input[name=brojTelefona-edit]');
    zahtjevEditBroj.value = uredivaniZahtjev.brojTelefona;

    if (uredivaniZahtjev.sakriven) {
        template.querySelector('#sakrivenDa-edit').checked = true;
    } else {
        template.querySelector('#sakrivenNe-edit').checked = true;
    }

    let datumEdit = template.querySelector('input[name=datum-edit]');
    let vrijemeEdit = template.querySelector('input[name=vrijeme-edit]');
    if (uredivaniZahtjev.rok) {
        date = new Date(uredivaniZahtjev.rok);
        datumEdit.value = date.toISOString().slice(0, 10);
        vrijemeEdit.value = date.toLocaleTimeString();
    }

    let kategorijaEdit = template.querySelector('select[name=kategorija-edit]');
    kategorijaEdit.value = uredivaniZahtjev.kategorija;


    if (uredivaniZahtjev.izvrsen) {
        template.querySelector('#izvrsenDa-edit').checked = true;
    } else {
        template.querySelector('#izvrsenNe-edit').checked = true;
    }

    //Provjera je li odabran vec neki korisnik koji treba pomoci zahtjevu. Ako je, na gumb odustani korisnik se prikvaci funkcija 
    //da se od toga korisnika odustane, isto tako, ispise se njegovo ime i zapamti se njegov id unutar forme
    let odustaniOdKorisnikaButton = template.querySelector('.odustani-Korisnik');
    if (uredivaniZahtjev.prihvacaKorisnikUsername) {
        $(odustaniOdKorisnikaButton).on('click', odbijKorisnika);
        $(odustaniOdKorisnikaButton).data('zahtjev-id', uredivaniZahtjev.zahtjevId);
        let pomagacEdit = template.querySelector('.zahtjev-edit-pomaze');
        pomagacEdit.innerHTML = uredivaniZahtjev.prihvacaKorisnikUsername;
        let pomagacId = template.querySelector('input[name=prihvacaKorisnikId-edit]');
        pomagacId.value = uredivaniZahtjev.prihvacaKorisnikId;
    } else {
        $(odustaniOdKorisnikaButton).remove();
    }

    let opisZahtjeva = template.querySelector('textarea[name=opisZahtjeva-edit]');
    opisZahtjeva.value = uredivaniZahtjev.opisZahtjeva;

    let urediButton = template.querySelector('.finish-edit');
    $(urediButton).data(`zahtjev-id`, uredivaniZahtjev.zahtjevId);
    $(urediButton).on('click', spremiZahtjev);

    let odustaniButton = template.querySelector('.cancel-edit');
    $(odustaniButton).on('click', odustaniUredivanje);

    $(template).insertAfter(stariZahtjev);
}

//DOHVACANJE SVIH ZAHTJEVA
async function dohvatiZahtjeve(event) {

    if (event.data.url == "/zahtjeviIzvrseni") {
        $("#napravi-novi-zahtjev").prop("disabled", true)
        closeForm();
    } else {
        $("#napravi-novi-zahtjev").prop("disabled", false)
    }

    let zahtjeviUrl = event.data.url;

    if (event.data.url_parametri) {
        zahtjeviUrl = event.data.url + event.data.url_parametri();
    }

    //Tako da se može uređivati ako čovjek ne zatvori edit formu, ali promijeni prikaz.
    stariZahtjev = undefined;
    uredivaniZahtjev = undefined;

    let serverUrl = sessionStorage.getItem("baseUrl");
    sessionStorage.setItem("prikaz", event.data.prikaz);

    try {
        let result = await axios({ url: zahtjeviUrl, baseURL: serverUrl, method: 'get' });
        if (result.status != 200) {
            showToastMessage("Zahtjevi neuspješno dohvaćeni...");
        }
        let zahtjevi = result.data;

        prikaziZahtjeve(zahtjevi);
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }

}

function prikaziZahtjeve(zahtjevi) {
    let spremnikZahtjeva = $("#spremnik-Zahtjeva");
    let korisnik = JSON.parse(sessionStorage.getItem("korisnik"));
    spremnikZahtjeva.empty();
    zahtjevi.forEach(zahtjev => {
        provjeriDodajuPaDodaj(zahtjev, spremnikZahtjeva, korisnik);
    })
}

function provjeriDodajuPaDodaj(zahtjev, spremnikZahtjeva, korisnik) {
    if (provjera(zahtjev))
        spremnikZahtjeva.prepend(napraviZahtjev(zahtjev, korisnik));
}

function provjera(zahtjev) {
    //u slucaju da korisnik radi novi zahtjev, ali je skriven i nije na korisnickom prikazu, tamo se ne smije dinamicki dodati
    if (zahtjev.sakriven && sessionStorage.getItem("prikaz") != "korisnicki") return false;
    //u slucaju da korisnik uređuje zahtjev i stavi da je izvrsen, ali nije na korisnickom prikazu
    if (zahtjev.izvrsen && sessionStorage.getItem("prikaz") != "korisnicki" && sessionStorage.getItem("prikaz") != "izvrseni") return false;
    return true;
}

function napraviZahtjev(zahtjev, korisnik) {
    let template = document.querySelector('#zahtjevTemplate').content.cloneNode(true);

    let zahtjevContainer = template.querySelector(".zahtjev");
    zahtjevContainer.id = `zahtjev-${zahtjev.zahtjevId}`;

    let zahtjevNaslov = template.querySelector('.zahtjev-naslov');
    zahtjevNaslov.innerHTML = zahtjev.opisZahtjeva;

    let korisnickiLink = template.querySelector('.korisnicko-ime-link');
    korisnickiLink.href = `/user/${zahtjev.vlasnikZahtjevaUsername}`;

    let korisnickoIme = template.querySelector('.korisnicko-ime');
    korisnickoIme.innerHTML = zahtjev.vlasnikZahtjevaUsername;

    let zahtjevRok = template.querySelector('.zahtjev-rok');
    if (zahtjev.rok)
        zahtjevRok.innerHTML = " " + formatDate(new Date(zahtjev.rok));
    else
        zahtjevRok.innerHTML = " Neodređeni rok";

    let kategorija = template.querySelector('.kategorija');
    kategorija.innerHTML = zahtjev.kategorija;

    let izvrsenSpan = template.querySelector('.izvrsen-zahtjev');
    if(zahtjev.izvrsen && korisnik.korisnikId == zahtjev.vlasnikZahtjevaId) {
        izvrsenSpan.innerHTML = "Zahtjev je izvršen";
    } else {
        $(izvrsenSpan).remove();
    }

    let buttonHide = template.querySelector('.hide-Request');
    $(buttonHide).data(`zahtjev-id`, zahtjev.zahtjevId);

    let helpButton = template.querySelector('.wanna-Help');
    let editButton = template.querySelector('.edit-Request');

    let blockButton = template.querySelector('.block-Zahtjev');
    let unblockButton = template.querySelector('.unblock-Zahtjev');
    let deleteButton = template.querySelector('.delete-Zahtjev');

    if (!zahtjev.izvrsen) {
        if (zahtjev.vlasnikZahtjevaId != korisnik.korisnikId) {
            $(helpButton).data(`zahtjev-id`, zahtjev.zahtjevId);
            if (zahtjev.trenutniKorisnikZainteresinan) {
                $(helpButton).text("Ne želim pomoći");
                $(helpButton).on('click', odustaniOdZahtjeva);
            } else {
                $(helpButton).on('click', pomogniZahtjevu);
            }
            $(buttonHide).on('click', sakrijZahtjev);
            editButton.remove();
        }
        else {
            if (zahtjev.sakriven) {
                $(buttonHide).text("Otkrij zahtjev");
                $(buttonHide).on('click', otkrijZahtjev);
            } else {
                $(buttonHide).on('click', sakrijZahtjev);
            }
            $(editButton).data(`zahtjev-id`, zahtjev.zahtjevId);
            $(editButton).on('click', otvoriFormu)
            helpButton.remove();
        }
    } else {
        if (zahtjev.vlasnikZahtjevaId == korisnik.korisnikId) {
            $(editButton).data(`zahtjev-id`, zahtjev.zahtjevId);
            $(editButton).on('click', otvoriFormu)
        } else {
            editButton.remove();
        }
        helpButton.remove();
        buttonHide.remove();
    }

    if (korisnik.uloga == 'ROLE_ADMIN' && !zahtjev.izvrsen) {
        $(deleteButton).data(`zahtjev-id`, zahtjev.zahtjevId);
        $(deleteButton).on('click', obrisiZahtjev);
        if (sessionStorage.getItem("prikaz") == "opceniti" && korisnik.korisnikId != zahtjev.vlasnikZahtjevaId) {
            $(blockButton).data(`zahtjev-id`, zahtjev.zahtjevId);
            $(blockButton).on('click', blokirajZahtjev);
            unblockButton.remove();
        } else if (sessionStorage.getItem("prikaz") == "blokirani" && korisnik.korisnikId != zahtjev.vlasnikZahtjevaId) {
            $(unblockButton).data(`zahtjev-id`, zahtjev.zahtjevId);
            $(unblockButton).on('click', odblokirajZahtjev);
            blockButton.remove();
        } else {
            blockButton.remove();
            unblockButton.remove();
        }
    } else if (korisnik.korisnikId == zahtjev.vlasnikZahtjevaId && !zahtjev.izvrsen) {
        $(deleteButton).data(`zahtjev-id`, zahtjev.zahtjevId);
        $(deleteButton).on('click', obrisiZahtjev);
        blockButton.remove();
        unblockButton.remove();
    } else {
        blockButton.remove();
        unblockButton.remove();
        deleteButton.remove();
    }

    return template;
}

function appendLeadingZeroes(n) {
    if (n <= 9) {
        return "0" + n;
    }
    return n;
}

function formatDate(date) {
    let formatted_date = appendLeadingZeroes(date.getDate()) + "." + appendLeadingZeroes(date.getMonth() + 1) + "." + date.getFullYear() + " " +
        appendLeadingZeroes(date.getHours()) + ":" + appendLeadingZeroes(date.getMinutes());
    return formatted_date;
}

//SLUŽI ZA POKAZIVANJE SVIH TOASTEVA
function showToastMessage(message) {
    let toast = document.querySelector(".toast");
    toast.setAttribute("data-autohide", "false");
    $(".toast").toast('show');
    toast = document.querySelector(".toast-body");
    toast.innerHTML = message;
}

let reqlat;
let reqlong;

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(reqSetLatLong);
    } else {
        alert("Geolokacija nije podržana!");
    }
}

function reqSetLatLong(position) {
    reqlat = position.coords.latitude;
    reqlong = position.coords.longitude;
    $('input[name=lat]').val(reqlat);
    $('input[name=long]').val(reqlong);
}

function pripremiFilterUrl() {

    var filterLokacije;

    var chkYes = document.getElementById("sLokacijom");

    var filterLokacije = chkYes.checked ? document.getElementById("sLokacijom") : document.getElementById("bezLokacije");

    var selectedCategory = $('#category').find(":selected").val();

    return "/?filter=" + filterLokacije.value + "&category=" +
        selectedCategory + "&distance=" + document.getElementById("distance").value +
        "&lat=" + document.getElementById("lat").value + "&long=" + document.getElementById("long").value
}

//PRIHVACANJE I ODBACIVANJE NOTIFIKACIJA

async function prihvatiKorisnika() {

    let prihvaceniKorisnikId = $(this).data("korisnik-id");
    let zahtjevId = $(this).data("zahtjev-id");

    let serverUrl = sessionStorage.getItem("baseUrl");

    try {
        let result = await axios({ url: `/acceptHelpZahtjev/${zahtjevId}/${prihvaceniKorisnikId}`, baseURL: serverUrl, method: 'put' });

        if (result.status != 200) {
            showToastMessage("Došlo je do pogreške kod prihvaćanja korisnika.");
            return
        }

        let notifikacijaId = $(this).data("notifikacija-id");
        await brisanjeNotifikacije(notifikacijaId);

    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
}

async function izbrisiNotifikaciju() {

    try {
        let notifikacijaId = $(this).data("notifikacija-id");
        await brisanjeNotifikacije(notifikacijaId)
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
}

async function brisanjeNotifikacije(notifikacijaId) {
    try {
        let serverUrl = sessionStorage.getItem("baseUrl");
        let result = await axios({ url: `/notification/delete/${notifikacijaId}`, baseURL: serverUrl, method: 'delete' });

        if (result.status != 200) {
            showToastMessage("Došlo je do pogreške kod micanja notifikacije.");
            return
        }

        $(`#notifikacija-${notifikacijaId}`).remove();
        let brojNotifikacija = $(".broj-notifikacija").text();
        $(".broj-notifikacija").text(brojNotifikacija - 1);

        provjeriNotifikacijskiGumb();

    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
}

async function odbijKorisnika() {
    if (!confirm("Stvarno želite odustati od korisnikove pomoći?")) return;

    let zahtjevId = $(this).data("zahtjev-id");

    try {
        let serverUrl = sessionStorage.getItem("baseUrl");
        let result = await axios({ url: `/removeHelpZahtjev/${zahtjevId}`, baseURL: serverUrl, method: 'put' });

        if (result.status != 200) {
            showToastMessage("Došlo je do pogreške kod odustajanja od korisnikove pomoći.");
            return
        }

        let pomagacEdit = document.querySelector('.zahtjev-edit-pomaze');
        pomagacEdit.innerHTML = "Neodređeni korisnik.";
        let pomagacId = document.querySelector('input[name=prihvacaKorisnikId-edit]');
        pomagacId.value = "";

        $('.odustani-Korisnik').remove();

    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }

}