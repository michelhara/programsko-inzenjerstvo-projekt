$(document).ready(function () {
    $('.dodaj-Ocjena').on('click', dodajOcjenu);
    $('.delete-Ocjena').on('click', obrisiOcjenu);
    $('.modal').on('hidden.bs.modal', function(){
        document.getElementById("forma-ocjena").reset();
    });
})

async function dodajOcjenu() {

    let serverUrl = sessionStorage.getItem("baseUrl");

    let formData = $("#forma-ocjena").serializeArray();

    let form = {};

    for (var i = 0; i < formData.length; ++i) {
        form[formData[i].name] = formData[i].value;
    }

    try {
        let result = await axios({ url: `dodajRecenziju/`, baseURL: serverUrl, method: 'post', data: form });

        if (result.status != 200) {
            showToastMessage("Ocjena neuspješno dodana!");
        } else {
        	showToastMessage("Ocjena dodana!")

	        let novaRecenzija = result.data;
	
	        dodajNovuRecenziju(novaRecenzija);
	
	        $(".ocijeni-button").prop("disabled", true);
	
	        $('.modal').modal('hide');
        }
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
}

async function obrisiOcjenu() {
    if (!confirm("Obrisati ovu recenziju?")) return;
    var recenziraniId = $(this).data("ocijenjeni-id");
    var recenziraId = $(this).data("ocijenio-id");
    try {
        let result = await axios.delete(`/izbrisiRecenziju/${recenziraniId}/`);
    if (result.status == 200) {
        $(`#recenzija-${recenziraId}-${recenziraniId}`).remove();
        showToastMessage("Ocjena obrisana!")
        $(".ocijeni-button").prop("disabled", false);
    } else {
        showToastMessage("Ocjena obrisana neuspješno!");
    }
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
}

function dodajNovuRecenziju(novaRecenzija) {
    let template = document.querySelector('#ocjeneTemplate').content.cloneNode(true);

    let ocjenaSpremnik = template.querySelector(".ocjena");
    ocjenaSpremnik.id = "recenzija-" + novaRecenzija.recenziraKorisnikId + '-' + novaRecenzija.recenziraniKorisnikId;

    let zvijezdaSpremnik = template.querySelector(".zvijezde");
    for(let i = 0; i < novaRecenzija.ocjena - 1; i++) {
        zvijezdaSpremnik.appendChild(template.querySelector(".fa-star").cloneNode(true));
    }

    let ocjenaKomentar = template.querySelector(".ocjena-komentar")
    ocjenaKomentar.innerHTML = novaRecenzija.komentar;

    let ocjenaUsernameLink = template.querySelector('.ocjena-username-link');
    ocjenaUsernameLink.innerHTML = novaRecenzija.recenziraKorisnikUsername;
    ocjenaUsernameLink.href = ocjenaUsernameLink.href + novaRecenzija.recenziraKorisnikUsername;

    let korisnik = JSON.parse(sessionStorage.getItem("korisnik"));

    let deleteButton = template.querySelector('.delete-Ocjena');

    if (novaRecenzija.recenziraKorisnikId = korisnik.korisnikId) {
        $(deleteButton).data("ocijenjeni-id", novaRecenzija.recenziraniKorisnikId);
        $(deleteButton).data("ocijenio-id", novaRecenzija.recenziraKorisnikId);
        $(deleteButton).on('click', obrisiOcjenu);
    } else {
        $(deleteButton).remove();
    }

    $("#spremnik-Ocjena").prepend(template);
}