$(document).ready(function () {
    $('.hide-Request').on('click', sakrijZahtjev);
    $('.delete-Zahtjev').on('click', obrisiZahtjev);
    $('.block-Zahtjev').on('click', blokirajZahtjev);
    $('.wanna-Help').on('click', pomogniZahtjevu);
    $('.dont-wanna-Help').on('click', odustaniOdZahtjeva);
})

//REST FUNKCIONALNOSTI 

async function obrisiZahtjev() {
    if (!confirm("Obrisati ovaj zahtjev?")) return;
    var zahtjevId = $(this).data("zahtjev-id");
    let result = await axios.delete(`/deleteZahtjev/${zahtjevId}/`);
    if (result.status == 200) {
        $(`#zahtjev-${zahtjevId}`).remove();
        showToastMessage("Zahtjev obrisan!")
    } else {
        showToastMessage("Brisanje zahtjeva neuspješno!");
    }
}

async function blokirajZahtjev() {
	let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Blokirati ovaj zahtjev?")) return;
    var zahtjevId = $(this).data("zahtjev-id");
    let result = await axios({ url: `/blockZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });
    if (result.status == 200) {
        $(`#zahtjev-${zahtjevId}`).remove();
        showToastMessage("Zahtjev blokiran!")
    } else {
        showToastMessage("Blokiranje zahtjeva neuspješno!");
    }
}

async function sakrijZahtjev() {
    let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Želite li sakriti/blokirati ovaj zahtjev?")) return;
    var zahtjevId = $(this).data("zahtjev-id");
    let result = await axios({ url: `hideZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });

    if (result.status != 200) {
        showToastMessage("Zahtjev nije uspješno sakriven!");
    }
    else {
        showToastMessage("Zahtjev uspješno sakriven!");
        if (sessionStorage.getItem("prikaz") == "korisnicki") {
            $(this).unbind();
            $(this).on('click', otkrijZahtjev);
            $(this).text("Otkrij zahtjev");
        } else {
            $(`#zahtjev-${zahtjevId}`).remove();

        }
    }
}

async function otkrijZahtjev() {
    let serverUrl = sessionStorage.getItem("baseUrl");
    var zahtjevId = $(this).data("zahtjev-id");
    let result = await axios({ url: `unhideZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });

    if (result.status != 200) {
        showToastMessage("Zahtjev nije uspješno otkriven...");
    }
    else {
        showToastMessage("Zahtjev je uspješno otrkiven!");
        if (sessionStorage.getItem("prikaz") == "korisnicki") {
            $(this).unbind();
            $(this).on('click', sakrijZahtjev);
            $(this).text("Sakrij");
        }
    }
}

//ODUSTAJANJE I POMAGANJE ZAHTJEVU
async function odustaniOdZahtjeva() {
    let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Želite li odustati od ovog zahtjeva?")) return;
    var zahtjevId = $(this).data("zahtjev-id");

    try {
        let result = await axios({ url: `noHelpZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });

        if (result.status != 200) {
            showToastMessage("Pomoć neuspješno povućena!");
        }
        else {
            $(this).unbind();
            $(this).text("Želim pomoći");
            $(this).on('click', pomogniZahtjevu);
            showToastMessage("Pomoć uspješno povućena!");
        }
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
}

async function pomogniZahtjevu() {
    let serverUrl = sessionStorage.getItem("baseUrl");
    if (!confirm("Želite li pomoći ovom zahtjevu?")) return;
    var zahtjevId = $(this).data("zahtjev-id");

    try {
        let result = await axios({ url: `helpZahtjev/${zahtjevId}/`, baseURL: serverUrl, method: 'put' });

        if (result.status != 200) {
            showToastMessage("Pomoć neuspješno poslana!");
        }
        else {
            $(this).unbind();
            $(this).text("Ne želim pomoći");
            $(this).on('click', odustaniOdZahtjeva);
            showToastMessage("Pomoć uspješno poslana!");
        }
    } catch (reject) {
        showToastMessage(reject.response.data.message);
    }
}

function showToastMessage(message) {
    let toast = document.querySelector(".toast");
    toast.setAttribute("data-autohide", "false");
    $(".toast").toast('show');
    toast = document.querySelector(".toast-body");
    toast.innerHTML = message;
}
