$(document).ready(function () {
    setUpLoggedInKorisnik();
});

async function setUpLoggedInKorisnik() {
    let urlResult = await axios.get("/baseUrl");
    if (urlResult.status == 200) {
        sessionStorage.setItem("baseUrl", urlResult.data);
    } else {
        alert("Problem sa dohvaćanjem BASE URL-a!");
    }
    let resultKorisnik = await axios.get("/user");
    if (urlResult.status == 200) {
        sessionStorage.setItem("korisnik", JSON.stringify(resultKorisnik.data));
    } else {
        alert("Problem sa dohvaćanjem korisnika!");
    }
}