package ProgChad.PomoziMi.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.TransactionSystemException;

import ProgChad.PomoziMi.domain.Kontakti;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.service.KontaktiService;
import ProgChad.PomoziMi.service.KorisnikService;

@SpringBootTest
public class UnitTestKontakti {

	@Autowired
	private KontaktiService kontaktiService;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Test
	public void successfullySaveKontakt() {
		
		Korisnik korisnik = korisnikService.findByUsername("pjero");
		
		Kontakti expectedKontakt = new Kontakti(
				korisnik,
				"01555666");
		
		Kontakti actualKontakt = kontaktiService.saveKontakt(expectedKontakt);
		expectedKontakt = kontaktiService.findByKontaktId(actualKontakt.getKontaktId());
		
		assertEquals(expectedKontakt, actualKontakt);
	}
	
	@Test
	public void saveKontaktWithWrongNumberFormat() {
		
		Korisnik korisnik = korisnikService.findByUsername("pjero");
		
		Kontakti expectedKontakt = new Kontakti(
				korisnik,
				"01555AAA");
		
		assertThrows(IllegalArgumentException.class, () -> kontaktiService.saveKontakt(expectedKontakt));
	}
}
