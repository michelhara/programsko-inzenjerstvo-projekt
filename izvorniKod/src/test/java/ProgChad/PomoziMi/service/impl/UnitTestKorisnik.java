package ProgChad.PomoziMi.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.service.KorisnikService;

@SpringBootTest
public class UnitTestKorisnik {
	
	@Autowired
	KorisnikService korisnikService;
	
	@Test
	public void successfullyCreateKorisnik() {
		Korisnik expectedKorisnik = new Korisnik(
				"Ivan",
				"Horvat",
				"ROLE_USER",
				"ivan.horvat@mail.com",
				"IHorvat",
				"IHorvat",
				1);
		
		Korisnik actualKorisnik = korisnikService.createKorisnik(expectedKorisnik);
		
		assertEquals(expectedKorisnik, actualKorisnik);
	}
	
	@Test
	public void createKorisnikWithSameEmail() {
		Korisnik expectedKorisnik = new Korisnik(
				"Ivan",
				"Horvat",
				"ROLE_USER",
				"pjer@email.com",
				"IHorvat",
				"IHorvat",
				1);
		
		assertThrows(IllegalArgumentException.class, () -> korisnikService.createKorisnik(expectedKorisnik));
	}
	
	@Test
	public void createKorisnikWithSameUsername() {
		Korisnik expectedKorisnik = new Korisnik(
				"Ivan",
				"Horvat",
				"ROLE_USER",
				"ivan.horvat@mail.com",
				"pjero",
				"pjero",
				1);
		
		assertThrows(IllegalArgumentException.class, () -> korisnikService.createKorisnik(expectedKorisnik));
	}
}
