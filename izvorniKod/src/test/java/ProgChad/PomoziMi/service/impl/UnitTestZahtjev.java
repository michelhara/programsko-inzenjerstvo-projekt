package ProgChad.PomoziMi.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Timestamp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Zahtjev;
import ProgChad.PomoziMi.service.KorisnikService;
import ProgChad.PomoziMi.service.ZahtjevService;

@SpringBootTest
public class UnitTestZahtjev {
	
	@Autowired
	ZahtjevService zahtjevService;
	
	@Autowired
	KorisnikService korisnikService;
	
	@Test
	public void twoPlacesCloseEnough() {
		
		Zahtjev zahtjev = zahtjevService.findByZahtjevId(8);
		ZahtjevServiceJpa zahtjevServiceJpa = new ZahtjevServiceJpa();
		
		//Crkva sv.Donata u Zadru
		boolean closeEnough = zahtjevServiceJpa.closeEnough(zahtjev, 50, 44.11638, 15.22431);
	
		assertEquals(true, closeEnough);
	}
	
	@Test
	public void twoPlacesNotCloseEnough() {
		
		Zahtjev zahtjev = zahtjevService.findByZahtjevId(8);
		ZahtjevServiceJpa zahtjevServiceJpa = new ZahtjevServiceJpa();
		
		//Koncertna dvorana "Vatrolav Lisinski" u Zagrebu
		boolean closeEnough = zahtjevServiceJpa.closeEnough(zahtjev, 100, 45.80116, 15.97981);
	
		assertEquals(false, closeEnough);
	}
	
	@Test
	public void tooSmallLengthGiven() {
		
		Zahtjev zahtjev = zahtjevService.findByZahtjevId(8);
		ZahtjevServiceJpa zahtjevServiceJpa = new ZahtjevServiceJpa();
	
		//Dioklecijanova palača u Splitu
		assertThrows(IllegalArgumentException.class, () -> zahtjevServiceJpa.closeEnough(zahtjev, -500, 43.50877, 16.44030));
	}
	
	@Test
	public void successfullyCreateZahtjev() {
		
		Korisnik zahtjevAutor = korisnikService.findByUsername("pjero");
		Korisnik prihvacaKorisnik = korisnikService.findByUsername("NikolaMoni");
		
		Zahtjev expectedZahtjev = new Zahtjev(
				zahtjevAutor,
				"Koji je najbolji kineski restoran u Zagrebu?",
				Timestamp.valueOf("2021-05-15 10:00:00"),
				false,
				null,
				null,
				"01555666",
				"Sve",
				false,
				true,
				prihvacaKorisnik);
		
		Zahtjev actualZahtjev = zahtjevService.createZahtjev(zahtjevAutor.getKorisnikId(), expectedZahtjev);
		expectedZahtjev = zahtjevService.findByZahtjevId(actualZahtjev.getZahtjevId());
		
		assertEquals(expectedZahtjev, actualZahtjev);
	}
	
	@Test
	public void createZahtjevWithoutOpisZahtjeva() {
		
		Korisnik zahtjevAutor = korisnikService.findByUsername("pjero");
		Korisnik prihvacaKorisnik = korisnikService.findByUsername("NikolaMoni");
		
		Zahtjev expectedZahtjev = new Zahtjev(
				zahtjevAutor,
				null,
				Timestamp.valueOf("2021-05-15 10:00:00"),
				false,
				null,
				null,
				"01555666",
				"Sve",
				false,
				true,
				prihvacaKorisnik);
		
		assertThrows(IllegalArgumentException.class, () -> zahtjevService.createZahtjev(zahtjevAutor.getKorisnikId(), expectedZahtjev));
	}
}
