package ProgChad.PomoziMi.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ProgChad.PomoziMi.domain.Korisnik;
import ProgChad.PomoziMi.domain.Recenzija;
import ProgChad.PomoziMi.service.KorisnikService;
import ProgChad.PomoziMi.service.RecenzijeService;

@SpringBootTest
public class UnitTestRecenzija {

	@Autowired
	RecenzijeService recenzijeService;
	
	@Autowired
	KorisnikService korisnikService;
	
	@Test
	public void successfullyCreateRecenzija() {
	
		Korisnik recenziraniKorisnik = korisnikService.findByUsername("pjero");
		Korisnik recenziraKorisnik = korisnikService.findByUsername("NikolaMoni");
		
		Recenzija expectedRecenzija = new Recenzija(
				recenziraniKorisnik,
				recenziraKorisnik,
				5,
				"Jako dobro obavljen posao!");
		
		Recenzija actualRecenzija = recenzijeService.createRecenzija(expectedRecenzija);
		List<Recenzija> recenzije = recenzijeService.findByRecenziraniKorisnikId(recenziraniKorisnik.getKorisnikId());
		for(Recenzija r : recenzije) {
			if(r.equals(actualRecenzija)) {
				expectedRecenzija = r;
				break;
			}
		}
		
		assertEquals(expectedRecenzija, actualRecenzija);
	}
	
	@Test
	public void createRecenzijaWithoutKomentar() {
		
		Korisnik recenziraniKorisnik = korisnikService.findByUsername("pjero");
		Korisnik recenziraKorisnik = korisnikService.findByUsername("NikolaMoni");
		
		Recenzija expectedRecenzija = new Recenzija(
				recenziraniKorisnik,
				recenziraKorisnik,
				5,
				null);
		
		assertThrows(IllegalArgumentException.class, () -> recenzijeService.createRecenzija(expectedRecenzija));
	}
	
	@Test
	public void korisnikRecenziraoDrugogKorisnik() {
		
		Korisnik recenziraniKorisnik = korisnikService.findByUsername("pjero");
		Korisnik recenziraKorisnik = korisnikService.findByUsername("NikolaMoni");
		
		Recenzija expectedRecenzija = new Recenzija(
				recenziraniKorisnik,
				recenziraKorisnik,
				5,
				"Jako dobro obavljen posao!");
		
		Recenzija actualRecenzija = recenzijeService.createRecenzija(expectedRecenzija);
		boolean ocijenio = recenzijeService.ocijenioKorisnik(recenziraKorisnik, recenziraniKorisnik);
		
		assertEquals(true, ocijenio);
	}
}
