package ProgChad.PomoziMi;

import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SystemTest {

	@Test
	public void testLoginGoodCreds() {
		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("localhost:8080");
		
		WebElement element = driver.findElement(By.name("username"));
		
		element.sendKeys("pjero");
		
		element = driver.findElement(By.name("password"));
		
		element.sendKeys("pjero");
		
		driver.findElement(By.cssSelector("button[type='submit']")).click();
		
		String redirURL = driver.getCurrentUrl();
		
		boolean compRes = redirURL.contains("home");
		
		assertEquals(true, compRes);
		
		driver.quit();
	}
	
	@Test
	public void testLoginBadCreds() {
		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("localhost:8080");
		
		WebElement element = driver.findElement(By.name("username"));
		
		element.sendKeys("badTest");
		
		element = driver.findElement(By.name("password"));
		
		element.sendKeys("badTest");
		
		driver.findElement(By.cssSelector("button[type='submit']")).click();
		
		String redirURL = driver.getCurrentUrl();
		
		boolean compRes = redirURL.contains("home");
		
		assertEquals(false, compRes);
		
		driver.quit();
	}
	
	
	@Test
	public void testLogout() {
		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("localhost:8080");
		
		WebElement element = driver.findElement(By.name("username"));
		
		element.sendKeys("pjero");
		
		element = driver.findElement(By.name("password"));
		
		element.sendKeys("pjero");
		
		driver.findElement(By.cssSelector("button[type='submit']")).click();
		
		driver.findElement(By.cssSelector("button[type='submit']")).click();
		
		String redirURL = driver.getCurrentUrl();
		
		boolean compRes = redirURL.contains("logout=true");
		
		assertEquals(true, compRes);
		
		driver.quit();
	}
	
	@Test
	public void testIzradaZahtjeva() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("localhost:8080");
		
		WebElement element = driver.findElement(By.name("username"));
		element.sendKeys("pjero");
		
		element = driver.findElement(By.name("password"));
		element.sendKeys("pjero");
		
		driver.findElement(By.cssSelector("button[type='submit']")).click();
		driver.findElement(By.cssSelector("button[id='napravi-novi-zahtjev']")).click();
		
		element = driver.findElement(By.cssSelector("input[list='brojeviTelefona']"));
		element.sendKeys("01555666");
		
		driver.findElement(By.cssSelector("input[id='spremiKontaktDa']")).click();
		
		driver.findElement(By.cssSelector("label[for='kategorija']")).click();
		
		driver.findElement(By.cssSelector("option[value='hrana']")).click();
		
		driver.findElement(By.cssSelector("input[id='adresaDa']")).click();
		
		driver.findElement(By.cssSelector("button[onclick='autoLocation()']")).click();
		
		element = driver.findElement(By.name("opisZahtjeva"));
		element.sendKeys("Koji je najbolji kineski restoran u Zagrebu?");
		
		driver.findElement(By.cssSelector("button[onclick='napraviNoviZahtjev()']")).click();
		
		element = driver.findElement(By.cssSelector("div[id='spremnik-Zahtjeva']"));
		
		boolean napravljenZahtjev = element.getText().contains("Koji je najbolji kineski restoran u Zagrebu?");
		assertEquals(true, napravljenZahtjev);
		
		driver.quit();
	}
}
