# Programsko Inženjerstvo - Projekt: "Pomozi mi"<br/>
Kratki opis: Aplikacija koja služi za brzo i jednostavno traženje pomoći.

## Članovi grupe ProgChad:<br/>
Nikola Kešćec<br/>
Mario Gjenero<br/>
Lovro Srebačić<br/>
Leonarda Gjenero<br/>
Ante Juričić<br/>
Michel Haralović

## Link upogonjene aplikacije:<br/>
https://pomozi-mi.herokuapp.com
